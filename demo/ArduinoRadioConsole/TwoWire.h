#ifndef TWOWIRE_H_
#define TWOWIRE_H_

#include "Arduino.h"

extern "C" void TWI_vect();

class ArduinoTwi
{
public:
  void begin();

  int8_t beginWrite(uint8_t addr) { return start(addr << 1); }
  int8_t beginRead(uint8_t addr)  { return start((addr << 1) | 0x01); }
  void   endTransfer();

  int8_t transmit(const uint8_t *data, int8_t data_len);
  int8_t receive(uint8_t        *data,  int8_t data_len);

  static constexpr uint32_t timeout_micros = 25000;
  static constexpr uint32_t default_freq   = 100000UL;

  static ArduinoTwi inst;
private:
  int8_t start(uint8_t val);
  bool   waitProcessing();

  uint8_t          slarw;
  volatile int8_t  remaining_bytes;

  union {
    const uint8_t *transmit_buffer;
    uint8_t       *receive_buffer;
  };

  friend void TWI_vect();
};

static auto & TwoWire = ArduinoTwi::inst;


#endif
