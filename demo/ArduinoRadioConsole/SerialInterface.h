#ifndef SERIALINTERFACE_H_
#define SERIALINTERFACE_H_

#include "Arduino.h"

#define FPSTR(pstr_pointer) (reinterpret_cast<const __FlashStringHelper *>(pstr_pointer))

struct SerialCommandDefinition
{
  bool (*fnActivate)();
  void (*fnInput)(uint8_t c);
  void (*fnPoll)();
  const char cmd[];
};

class SerialInterface
{
public:
  void begin();
  void input(uint8_t c);
  void poll();
  
  bool command_finished();

  void   println_progmem_string_table(int8_t index, int8_t first_index, const __FlashStringHelper * (*progmem_string_table));
  void   print_hex(const uint8_t* buf, uint8_t len);
  int8_t printlnTwiError(int8_t status);
  
  uint8_t   buffer[32 + 1];
  uint8_t   index;
  
  static SerialInterface inst;

private:
  static bool input_activate();
  static void input_input(uint8_t c);
  static bool help_activate();
  
  void        load_command(uint8_t idx);
  void        activate_command(uint8_t idx);
  
  unsigned long             last_input_millis;
  SerialCommandDefinition   current_command = { NULL, NULL, NULL, {} };

  static const SerialCommandDefinition input_def;
  static const SerialCommandDefinition help_def;
  
  static const SerialCommandDefinition* const command_table[] PROGMEM;
  
  friend class CommandInput;
  friend class CommandHelp;
};

class CommandInput
{
public:
  static const SerialCommandDefinition def;
  
protected:
  static bool activate();
  static void input(uint8_t c);
  static void poll();
};

class CommandHelp : public CommandInput
{
public:
  static const SerialCommandDefinition def;

private:
  static bool activate();
};



static SerialInterface & SerialIf = SerialInterface::inst ;

#endif
