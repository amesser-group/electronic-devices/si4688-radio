#ifndef MAX98089DEFS_H_
#define MAX98089DEFS_H_

#include <stdint.h>

class Max98089Defs
{
public:
  enum : uint8_t
  {
    REG_BATTERY_VOLTAGE                          = 0x03,
    REG_MASTER_CLOCK                             = 0x10,
    REG_DAI1_CLOCK_MODE                          = 0x11,
    REG_DAI1_ANY_CLOCK_CONTROL1                  = 0x12,
    REG_DAI1_ANY_CLOCK_CONTROL2                  = 0x13,
    REG_DAI1_FORMAT                              = 0x14,
    REG_DAI1_CLOCK                               = 0x15,
    REG_DAI1_IO_CONFIGURATION                    = 0x16,
    REG_DAI1_TIME_DIVISION_MULTIPLEX             = 0x17,
    REG_DAI1_FILTERS                             = 0x18,
    REG_DAC_MIXER                                = 0x22,
    REG_LEFT_RECEIVER_AMPLIFIER_MIXER            = 0x28,
    REG_RIGHT_RECEIVER_AMPLIFIER_MIXER           = 0x29,
    REG_RECEIVER_AMPLIFIER_MIXER_CONTROL         = 0x2A,
    REG_LEFT_SPEAKER_OUTPUT_MIXER_REGISTER       = 0x2B,
    REG_RIGHT_SPEAKER_OUTPUT_MIXER_REGISTER      = 0x2C,
    REG_DAI1_PLAYBACK_LEVEL_CONTROL              = 0x2F,

    REG_HPVOL_LEFT                               = 0x39,
    REG_HPVOL_RIGHT                              = 0x3A,
    REG_LEFT_RECEIVER_AMPLIFIER_VOLUME_CONTROL   = 0x3B,
    REG_RIGHT_RECEIVER_AMPLIFIER_VOLUME_CONTROL  = 0x3C,
    REG_LEFT_SPEAKER_OUTPUT_LEVEL_REGISTER       = 0x3D,
    REG_RIGHT_SPEAKER_OUTPUT_LEVEL_REGISTER      = 0x3E,

    REG_LEVEL_CONTROL                            = 0x49,
    REG_OUTPUT_ENABLE                            = 0x4D,
    REG_POWER_MANAGEMENT_TOPLEVEL_BIAS_CONTROL   = 0x4E,
    REG_DAC_LOW_POWER_MODE2                      = 0x50,
    REG_SYSTEM_SHUTDOWN                          = 0x51,

    REG_EQ1_BAND1                                = 0x52,
  };
};

#endif
