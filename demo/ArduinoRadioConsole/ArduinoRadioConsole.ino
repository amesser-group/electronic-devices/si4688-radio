/*
  Fade

  This example shows how to fade an LED on pin 9 using the analogWrite()
  function.

  The analogWrite() function uses PWM, so if you want to change the pin you're
  using, be sure to use another PWM capable pin. On most Arduino, the PWM pins
  are identified with a "~" sign, like ~3, ~5, ~6, ~9, ~10 and ~11.

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Fade
*/
#include "RadioShield.h"
#include "SerialInterface.h"
#include "TwoWire.h"
#include <avr/pgmspace.h>


static const uint8_t pin_int = 9; 


#if 0
bool check_eeprom() 
{
  uint8_t buf[2] = {0x80, 0x00};
  
  if (i2c_write_read(0x50, buf, 2, 2) != 2)
    return false;
    
  if ((buf[0] & 0xF0) != 0)
    return false;
    
  if ((buf[1] & 0xF0) != 0)
    return false;
    
  return true;
}
#endif


enum class AppState : uint8_t 
{
  POWERUP = 0,
  IDLE,
  NOT_READY,
};

static AppState state = AppState::POWERUP;

// the setup routine runs once when you press reset:
void setup() 
{
  // declare pin 9 to be an output:
  pinMode(pin_int, INPUT);
  
  Serial.begin(115200);
  TwoWire.begin();
  
  SerialIf.begin();
  RadioShield.begin();
}

// the loop routine runs over and over again forever:
void loop() 
{
  if(Serial.available())
  {
    SerialIf.input(Serial.read());
  }
  else
  {
    SerialIf.poll();
  }
  
  #if 0
  switch(state)
  {
  case AppState::POWERUP:
    if(!RadioShield.isReady())
      state = AppState::NOT_READY;
    else
      state = AppState::IDLE;
    break;
  case AppState::NOT_READY:
    Serial.println(F("RadioShield not ready"));
    delay(5000);
    RadioShield.begin();
    break;
  }
  #endif

#if 0
  if (check_eeprom())
    Serial.write("EEPROM OK\n");
  else
    Serial.write("EEPROM Failed\n");

  if (check_max98089())
    Serial.write("MAX98089 OK\n");
  else
    Serial.write("MAX98089 Failed\n");

  if (check_pcal6408())
    Serial.write("PCAL6408 OK\n");
  else
    Serial.write("PCAL6408 Failed\n");

  if (check_si4688())
    Serial.write("SI4688 OK\n");
  else
    Serial.write("SI4688 Failed\n");
#endif

  /* delay(5000); */
}
