#ifndef COMMANDSTATUS_H_
#define COMMANDSTATUS_H_

#include "SerialInterface.h"

class CommandStatus : public CommandHelp
{
public:
  static bool activate();
  static const SerialCommandDefinition def;
private:
  static void println_stringtable(int8_t i, int8_t i_start, const __FlashStringHelper** table, int8_t table_count);
};

#endif
