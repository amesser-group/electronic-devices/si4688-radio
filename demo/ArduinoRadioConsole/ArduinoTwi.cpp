#include "TwoWire.h"
#include "SerialInterface.h"

ArduinoTwi ArduinoTwi::inst;

void
ArduinoTwi::begin()
{
  // activate internal pullups for twi.
  digitalWrite(SDA, 1);
  digitalWrite(SCL, 1);

  // initialize twi prescaler and bit rate
  TWSR = 0;
  TWBR = ((F_CPU / default_freq) - 16) / 2;

  /* twi bit rate formula from atmega128 manual pg 204
  SCL Frequency = CPU Clock Frequency / (16 + (2 * TWBR))
  note: TWBR should be 10 or higher for master mode
  It is 72 for a 16mhz Wiring board with 100kHz TWI */

  // enable twi module, acks, and twi interrupt
  TWCR = _BV(TWEN) | _BV(TWIE);
}

bool
ArduinoTwi::waitProcessing()
{
  int8_t   tmp = ~remaining_bytes;
  uint32_t ts_timeout;
  
  while((TWCR & _BV(TWIE)))
  {
    if (tmp != remaining_bytes)
      ts_timeout = micros() + timeout_micros;
    else if ( (int32_t)(ts_timeout - micros()) > 0)
      return false;
  }  

  return true;
}


int8_t ArduinoTwi::start(uint8_t val)
{
  slarw           = val;
  remaining_bytes = 0;

  //Serial.print(F("tw pre-start: ")); Serial.print(val); Serial.print(" "); Serial.println(TWSR);

  TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN) | _BV(TWIE);

  if(!waitProcessing())
    return -3;

  //Serial.print(F("tw start: ")); Serial.print(val); Serial.print(" "); Serial.println(TWSR);

  switch(TWSR & 0xF8)
  {
  case 0x18:
  case 0x40:
    return 0;
  case 0x20:
    TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN) | _BV(TWIE);
    return -1;
  case 0x38:
    TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWIE);
    return -2;
  case 0x48:
    TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN) | _BV(TWIE);
    return -2;
  default:
    return -4;
  }
}

void
ArduinoTwi::endTransfer()
{
  //Serial.print(F("tw stop: ")); Serial.println(TWSR);

  if( (TWSR & 0xF8) == 0x50)
  {
    TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWIE);
    waitProcessing();
  }
  
  TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN) | _BV(TWIE);
  while((TWCR & _BV(TWSTO)));
}

int8_t
ArduinoTwi::transmit(const uint8_t *data, int8_t data_len)
{
  transmit_buffer  = data;
  remaining_bytes  = data_len;
  
  TWCR = _BV(TWEN) | _BV(TWIE);

  if(!waitProcessing())
    return -3;
  
  switch(TWSR & 0xF8)
  {
  case 0x28:
    return data_len - remaining_bytes;
  case 0x30:
    TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN) | _BV(TWIE);
    return -2;
  case 0x38:
    TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWIE);
    return -4;
  default:
    return -4;
  }
}

int8_t
ArduinoTwi::receive(uint8_t *data, int8_t data_len)
{
  receive_buffer   = data;
  remaining_bytes  = data_len;

  if((TWSR & 0xF8) == 0x50)
    TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWEA) | _BV(TWIE);
  else      
    TWCR = _BV(TWEN) | _BV(TWIE);

  if(!waitProcessing())
    return -3;
  
  switch(TWSR & 0xF8)
  {
  case 0x40:
  case 0x50:
  case 0x58:
    return data_len - remaining_bytes;
  case 0x38:
    TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWIE);
    return -4;
  default:
    return -4;
  }
}


ISR(TWI_vect)
{
  auto & inst = ArduinoTwi::inst;
  
  switch(TWSR & 0xF8)
  {
  case 0x08:
  case 0x10:
    TWDR = inst.slarw;
    TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWIE);
    break;
  case 0x18:
  case 0x28:
    if(inst.remaining_bytes > 0)
    {
      TWDR = *(inst.transmit_buffer++);
      inst.remaining_bytes--;
      
      TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWIE);
    }
    else
    {
      TWCR = _BV(TWEN);
    }
    break;
  case 0x40:
    if(inst.remaining_bytes > 0)
      TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWEA) | _BV(TWIE);
    else
      TWCR = _BV(TWEN);
    break;    
  case 0x50:
    if(inst.remaining_bytes > 0)
    {
      *(inst.receive_buffer++) = TWDR;
      inst.remaining_bytes--;
    }
    
    if(inst.remaining_bytes > 0)
      TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWEA) | _BV(TWIE);
    else
      TWCR = _BV(TWEN);
    break;    
  case 0x58:
    TWCR = _BV(TWEN);
    break;
  default:
    TWCR = _BV(TWEN);
    break;
  }
}
