#include "RadioShield.h"
#include "SerialInterface.h"
#include "TwoWire.h"

#include <avr/pgmspace.h>


extern const uint8_t max98089_voltageconfig[2] PROGMEM;
extern const uint8_t max98089_clockconfig[18] PROGMEM;
extern const uint8_t max98089_mixer[13] PROGMEM;
extern const uint8_t max98089_powermgmt[5] PROGMEM;


bool
RadioShieldImplementation::begin_max98089() 
{
  uint8_t buf[2];
  
  if(!enable_max98089_clock())
    return false;
  
  buf[0] = 0xFF;
  if (i2c_write_read(i2caddr_max98089, buf, 1, 1) != 1)
  {
    max98089_state = Max98089State::DeviceError;
    return false;
  }
    
  if (buf[0] != 0x40)
  {
    max98089_state = Max98089State::DeviceError;
    return false;
  }
    
  if (!shutdownMax98089())
    return false;

  if(i2c_write_P(i2caddr_max98089, max98089_voltageconfig, sizeof(max98089_voltageconfig)) != 0)
  {
    max98089_state = Max98089State::DeviceError;
    return false;
  }

  if(i2c_write_P(i2caddr_max98089, max98089_clockconfig, sizeof(max98089_clockconfig)) != 0)
  {
    max98089_state = Max98089State::DeviceError;
    return false;
  }
    
  if(i2c_write_P(i2caddr_max98089, max98089_mixer, sizeof(max98089_mixer)) != 0)
  {
    max98089_state = Max98089State::DeviceError;
    return false;
  }

  if(i2c_write_P(i2caddr_max98089, max98089_powermgmt, sizeof(max98089_powermgmt)) != 0)
  {
    max98089_state = Max98089State::DeviceError;
    return false;
  }

  if(!setSpeakerVolume(0))
    return false;

  return setHeadphoneVolume(0);
}

bool
RadioShieldImplementation::shutdownMax98089()
{
  uint8_t buf[2];
  
  buf[0] = Max98089Defs::REG_SYSTEM_SHUTDOWN; buf[1] = 0x00;
  
  if (i2c_write_read(i2caddr_max98089, buf, 2, 0) != 0)
  {
    max98089_state = Max98089State::DeviceError;
    return false;
  }

  max98089_state = Max98089State::Shutdown;
  return true;
}

bool
RadioShieldImplementation::activateMax98089()
{
  uint8_t buf[2];
  
  buf[0] = Max98089Defs::REG_SYSTEM_SHUTDOWN; buf[1] = 0xC0;
  
  if (i2c_write_read(i2caddr_max98089, buf, 2, 0) != 0)
  {
    max98089_state = Max98089State::DeviceError;
    return false;
  }

  max98089_state = Max98089State::Active;
  return true;
}


bool
RadioShieldImplementation::setSpeakerVolume(uint8_t vol)
{
  uint8_t buf[1+2];
  
  buf[0] = Max98089Defs::REG_LEFT_SPEAKER_OUTPUT_LEVEL_REGISTER; 
  buf[1] = vol & 0x1F;
  buf[2] = vol & 0x1F;
  
  if (i2c_write_read(i2caddr_max98089, buf, 3, 0) != 0)
  {
    max98089_state = Max98089State::DeviceError;
    return false;
  }

  return true;  
}


bool
RadioShieldImplementation::setHeadphoneVolume(uint8_t vol)
{
  uint8_t buf[1+2];
  
  buf[0] = Max98089Defs::REG_HPVOL_LEFT; 
  buf[1] = vol & 0x1F;
  buf[2] = vol & 0x1F;
  
  if (i2c_write_read(i2caddr_max98089, buf, 3, 0) != 0)
  {
    max98089_state = Max98089State::DeviceError;
    return false;
  }

  return true;  
}

const uint8_t max98089_voltageconfig[2] PROGMEM =
{ 
  Max98089Defs::REG_BATTERY_VOLTAGE, 
  (uint8_t)((5.0-2.55) * 10.) /* 5.0 V Speaker amplifier supply voltage */
};

/* Reference Clock & Audio interface setup */
const uint8_t max98089_clockconfig[18] PROGMEM =
{
  Max98089Defs::REG_MASTER_CLOCK,
  0x10, /* PCLK = MCLK = 12.288 MHz */

  /* Start Config for Si4688 IF */
  /*DAI1 Clock Control */
  0x80,                                  /* 48 kHz Samplerate for ALC */
  (0x6000 >> 8) & 0x7F, 0x6000  & 0xFE,  /* Setup for 48 khz samplerate from 12.288 MHz PCLK and DHF=0 */ 
  /*DAI1 Configuration */
  0x90, /* I2S: Master, Second rising edge */
  0x01, /* I2S: BCLK = 64*LRCLK = 3.072 MHz-> suitable for 24bit output of si468x */// todo
  0x81, /* DAI1: Use Port S2, Playback enable */
  0x00,
  0x80, /* Set mode 1 for music mode */

  /* Start Config for Microcontroller IF */

  /* DAI2 Clock Control */
  0x70,                                 /* 44.1 kHz Samplerate for ALC */
  (0x5833 >> 8) & 0x7F, 0x5833  & 0xFE,  /* Setup for 44.1 khz samplerate from 12.288 MHz PCLK and DHF=0 */
  /*DAI2 Configuration */
  0x10, /* I2S: Slave, Second rising edge */
  0x02, /* I2S: BCLK, not relevant in slave mode */
  0x41, /* DAI2: Use Port S1, Playback enable */
  0x00,
  0x00,
};


/* Mixers configuration */
const uint8_t max98089_mixer[13] PROGMEM =
{
  Max98089Defs::REG_DAC_MIXER, 
  0xA5, /* Connect DAC with DAI1 & DAI2*/
  0x00, /* Left ADC  unconnected */
  0x00, /* Right ADC unconnected */
  0x01, /* Connect Headphone Left  with Left  DAC */
  0x01, /* Connect Headphone Right with Right DAC */
  0x00, /* Bypass Headphone Output Mixer, Connect with DAC directly, Gain = 0db */
  0x00, /* Disconnect Left Lineout */
  0x00, /* Diconnect Right Lineout */
  0x80, /* Stereo Linout, Gain 0dB */
  0x01, /* Reg 0x2b Connect Left Speaker to Left DAC */
  0x01, /* Reg 0x2c Connect Right Speaker to Right DAC */
  0x00, /* Speaker Gain 0dB */  
};


/* Power management */
const uint8_t max98089_powermgmt[5] PROGMEM =
{
  Max98089Defs::REG_OUTPUT_ENABLE,
  0xF3, /* Enable Headphones, Speaker, DAC */
  0xF0, /* Enable Bandgap, Regulator, Common Mode & Bias */
  0x00, /* Disable low power mode */
  0x0F, /* DAI1 + DAI2 Input Dither, DAI1 + DAI2 Clock Gen Enable */
};
