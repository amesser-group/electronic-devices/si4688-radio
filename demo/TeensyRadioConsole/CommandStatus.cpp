#include "CommandStatus.h"
#include "RadioShield.h"

const SerialCommandDefinition CommandStatus::def PROGMEM = 
{
  &(CommandStatus::activate),
  &(CommandStatus::input),
  &(CommandStatus::poll),
  "status",
};

static const char status_unkown[]      PROGMEM = "Unkown: ";


static const char status_deviceerror[] PROGMEM = "DeviceError";
static const char status_poweron[]     PROGMEM = "PowerOn";
static const char status_shutdown[]    PROGMEM = "Shutdown";
static const char status_active[]      PROGMEM = "Active";

static const __FlashStringHelper* const max98089_statusnames[] PROGMEM =
{
  FPSTR(status_deviceerror),
  FPSTR(status_poweron),
  FPSTR(status_shutdown),
  FPSTR(status_active),
};

static const char status_standby[]     PROGMEM = "Standby";
static const char status_bootloader[]  PROGMEM = "Bootloader";
static const char status_firmware[]    PROGMEM = "Firmware";

static const __FlashStringHelper* const si4688_statusnames[] PROGMEM =
{
  FPSTR(status_deviceerror),
  FPSTR(status_standby),
  FPSTR(status_poweron),
  FPSTR(status_bootloader),
  FPSTR(status_firmware),
};


void
CommandStatus::println_stringtable(int8_t i, int8_t i_start, const __FlashStringHelper** table, int8_t table_count)
{
  if(i < (table_count + i_start))
  {
    SerialIf.println_progmem_string_table(i, i_start, table);
  }
  else
  {
    Serial.print(FPSTR(status_unkown)); Serial.println(i);
  }
}


bool
CommandStatus::activate()
{
  Serial.print(F("!=== Max98089: ")); 
  println_stringtable((int8_t)RadioShield.getMax98089State(), -1, max98089_statusnames, sizeof(max98089_statusnames) / sizeof(max98089_statusnames[0]));

  Serial.print(F("!=== Si4688: "));

  auto s = RadioShield.getSi4688State();
  if(s != Si4688State::Standby)
  {
    RadioShield.waitSi4688Ready();
    SerialIf.print_hex(RadioShield.getSi4688StatusWord(), 4);
    Serial.print(' ');  
  }
  println_stringtable((int8_t)s, -1, si4688_statusnames, sizeof(si4688_statusnames) / sizeof(si4688_statusnames[0]));
  

  return CommandInput::activate();
}
