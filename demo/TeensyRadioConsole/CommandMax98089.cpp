#include "CommandMax98089.h"
#include "RadioShield.h"

const SerialCommandDefinition CommandMax98089Shutdown::def PROGMEM = 
{
  &(CommandMax98089Shutdown::activate),
  &(CommandMax98089Shutdown::input),
  &(CommandMax98089Shutdown::poll),
  "max_shutdown",  
};

bool
CommandMax98089Shutdown::activate()
{
  RadioShield.shutdownMax98089();
  return SerialIf.command_finished();
}

const SerialCommandDefinition CommandMax98089Activate::def PROGMEM = 
{
  &(CommandMax98089Activate::activate),
  &(CommandMax98089Activate::input),
  &(CommandMax98089Activate::poll),
  "max_activate",  
};

bool
CommandMax98089Activate::activate()
{
  RadioShield.activateMax98089();
  return SerialIf.command_finished();
}

const SerialCommandDefinition CommandMax98089SpeakerVolume::def PROGMEM = 
{
  &(CommandMax98089SpeakerVolume::activate),
  &(CommandMax98089SpeakerVolume::input),
  &(CommandMax98089SpeakerVolume::poll),
  "vol_spk",  
};

bool
CommandMax98089SpeakerVolume::activate()
{
  char *s,*q;
  unsigned long val;
  
  /* ensure null terminatation */
  s = SerialIf.buffer;
  s[SerialIf.index] = '\0';
  
  s +=8;
  val = strtoul(s, &q, 0);
  
  if(q == s)
    return false;
  else if (val > 0x1f)
    return false;
  
  RadioShield.setSpeakerVolume((uint8_t)val);
  return SerialIf.command_finished();
}

const SerialCommandDefinition CommandMax98089HeadphoneVolume::def PROGMEM = 
{
  &(CommandMax98089HeadphoneVolume::activate),
  &(CommandMax98089HeadphoneVolume::input),
  &(CommandMax98089HeadphoneVolume::poll),
  "vol_hp",  
};

bool
CommandMax98089HeadphoneVolume::activate()
{
  char *s,*q;
  unsigned long val;
  
  /* ensure null terminatation */
  s = SerialIf.buffer;
  s[SerialIf.index] = '\0';
  
  s +=7;
  val = strtoul(s, &q, 0);
  
  if(q == s)
    return false;
  else if (val > 0x1f)
    return false;
  
  RadioShield.setHeadphoneVolume((uint8_t)val);
  return SerialIf.command_finished();
}
