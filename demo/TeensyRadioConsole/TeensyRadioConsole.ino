/*
  Fade

  This example shows how to fade an LED on pin 9 using the analogWrite()
  function.

  The analogWrite() function uses PWM, so if you want to change the pin you're
  using, be sure to use another PWM capable pin. On most Arduino, the PWM pins
  are identified with a "~" sign, like ~3, ~5, ~6, ~9, ~10 and ~11.

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Fade
*/
#include "RadioShield.h"
#include "SerialInterface.h"
#include "TwoWire.h"
#include "Audio.h"
#include "RadioShieldI2S.h"

#include <avr/pgmspace.h>


static const uint8_t pin_int = 9; 

#if 0
bool check_eeprom() 
{
  uint8_t buf[2] = {0x80, 0x00};
  
  if (i2c_write_read(0x50, buf, 2, 2) != 2)
    return false;
    
  if ((buf[0] & 0xF0) != 0)
    return false;
    
  if ((buf[1] & 0xF0) != 0)
    return false;
    
  return true;
}
#endif


AudioInputUSB          usb1;
RadioShieldI2SOutput   i2s1;
AudioConnection  patchCord1(usb1, 0, i2s1, 0);
AudioConnection  patchCord2(usb1, 1, i2s1, 1);


// the setup routine runs once when you press reset:
void setup() 
{  
  Serial.begin(9600);
  
  Serial.println("Starting TwoWire");
  TwoWire.begin();

  Serial.println("Starting serial If");
  SerialIf.begin();
  Serial.println("Starting Radioshield");
  RadioShield.begin();


  AudioMemory(16);
  Serial.println("Ready");
}


// the loop routine runs over and over again forever:
void loop() 
{
  if(Serial.available())
  {
    SerialIf.input(Serial.read());
  }
  else
  {
    SerialIf.poll();
  }
  
#if 0
  if (check_eeprom())
    Serial.write("EEPROM OK\n");
  else
    Serial.write("EEPROM Failed\n");

  if (check_max98089())
    Serial.write("MAX98089 OK\n");
  else
    Serial.write("MAX98089 Failed\n");

  if (check_pcal6408())
    Serial.write("PCAL6408 OK\n");
  else
    Serial.write("PCAL6408 Failed\n");

  if (check_si4688())
    Serial.write("SI4688 OK\n");
  else
    Serial.write("SI4688 Failed\n");
#endif

  /* delay(5000); */
}
