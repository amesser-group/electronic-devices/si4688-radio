EESchema Schematic File Version 4
LIBS:dabradio-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title "Si4688 based DAB/FM Radio"
Date "2019-03-26"
Rev "2"
Comp "Copyright (c) 2019 Andreas Messer"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L dabradio:SI4688-A10-GM U201
U 1 1 558C6090
P 5450 3700
F 0 "U201" H 6000 2400 60  0000 C CNN
F 1 "SI4688-A10-GM" H 5250 2550 60  0000 C CNN
F 2 "Housings_DFN_QFN:QFN-48-1EP_7x7mm_Pitch0.5mm" H 5450 3350 60  0001 C CNN
F 3 "" H 5450 3350 60  0000 C CNN
	1    5450 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C205
U 1 1 558C60D8
P 6700 5050
AR Path="/558C60D8" Ref="C205"  Part="1" 
AR Path="/559EC5B3/558C60D8" Ref="C205"  Part="1" 
F 0 "C205" H 6700 5150 40  0000 L CNN
F 1 "8p2" H 6706 4965 40  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 6738 4900 30  0001 C CNN
F 3 "~" H 6700 5050 60  0000 C CNN
	1    6700 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C207
U 1 1 558C60EF
P 6900 5050
AR Path="/558C60EF" Ref="C207"  Part="1" 
AR Path="/559EC5B3/558C60EF" Ref="C207"  Part="1" 
F 0 "C207" H 6900 5150 40  0000 L CNN
F 1 "2n2" H 6906 4965 40  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 6938 4900 30  0001 C CNN
F 3 "~" H 6900 5050 60  0000 C CNN
	1    6900 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C209
U 1 1 558C60F5
P 7100 5050
AR Path="/558C60F5" Ref="C209"  Part="1" 
AR Path="/559EC5B3/558C60F5" Ref="C209"  Part="1" 
F 0 "C209" H 7100 5150 40  0000 L CNN
F 1 "1u" H 7106 4965 40  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 7138 4900 30  0001 C CNN
F 3 "~" H 7100 5050 60  0000 C CNN
	1    7100 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C211
U 1 1 558C610F
P 7400 5050
AR Path="/558C610F" Ref="C211"  Part="1" 
AR Path="/559EC5B3/558C610F" Ref="C211"  Part="1" 
F 0 "C211" H 7400 5150 40  0000 L CNN
F 1 "8p2" H 7406 4965 40  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 7438 4900 30  0001 C CNN
F 3 "~" H 7400 5050 60  0000 C CNN
	1    7400 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C213
U 1 1 558C6115
P 7600 5050
AR Path="/558C6115" Ref="C213"  Part="1" 
AR Path="/559EC5B3/558C6115" Ref="C213"  Part="1" 
F 0 "C213" H 7600 5150 40  0000 L CNN
F 1 "2n2" H 7606 4965 40  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 7638 4900 30  0001 C CNN
F 3 "~" H 7600 5050 60  0000 C CNN
	1    7600 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C215
U 1 1 558C611B
P 7800 5050
AR Path="/558C611B" Ref="C215"  Part="1" 
AR Path="/559EC5B3/558C611B" Ref="C215"  Part="1" 
F 0 "C215" H 7800 5150 40  0000 L CNN
F 1 "1u" H 7806 4965 40  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 7838 4900 30  0001 C CNN
F 3 "~" H 7800 5050 60  0000 C CNN
	1    7800 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C216
U 1 1 558C6121
P 8100 5050
AR Path="/558C6121" Ref="C216"  Part="1" 
AR Path="/559EC5B3/558C6121" Ref="C216"  Part="1" 
F 0 "C216" H 8100 5150 40  0000 L CNN
F 1 "8p2" H 8106 4965 40  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 8138 4900 30  0001 C CNN
F 3 "~" H 8100 5050 60  0000 C CNN
	1    8100 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C217
U 1 1 558C6127
P 8300 5050
AR Path="/558C6127" Ref="C217"  Part="1" 
AR Path="/559EC5B3/558C6127" Ref="C217"  Part="1" 
F 0 "C217" H 8300 5150 40  0000 L CNN
F 1 "2n2" H 8306 4965 40  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 8338 4900 30  0001 C CNN
F 3 "~" H 8300 5050 60  0000 C CNN
	1    8300 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C218
U 1 1 558C612D
P 8500 5050
AR Path="/558C612D" Ref="C218"  Part="1" 
AR Path="/559EC5B3/558C612D" Ref="C218"  Part="1" 
F 0 "C218" H 8500 5150 40  0000 L CNN
F 1 "1u" H 8506 4965 40  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 8538 4900 30  0001 C CNN
F 3 "~" H 8500 5050 60  0000 C CNN
	1    8500 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C206
U 1 1 558C6884
P 6750 3650
AR Path="/558C6884" Ref="C206"  Part="1" 
AR Path="/559EC5B3/558C6884" Ref="C206"  Part="1" 
F 0 "C206" H 6750 3750 40  0000 L CNN
F 1 "8p2" H 6756 3565 40  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 6788 3500 30  0001 C CNN
F 3 "~" H 6750 3650 60  0000 C CNN
	1    6750 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C208
U 1 1 558C688A
P 6950 3650
AR Path="/558C688A" Ref="C208"  Part="1" 
AR Path="/559EC5B3/558C688A" Ref="C208"  Part="1" 
F 0 "C208" H 6950 3750 40  0000 L CNN
F 1 "2n2" H 6956 3565 40  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 6988 3500 30  0001 C CNN
F 3 "~" H 6950 3650 60  0000 C CNN
	1    6950 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C210
U 1 1 558C6890
P 7150 3650
AR Path="/558C6890" Ref="C210"  Part="1" 
AR Path="/559EC5B3/558C6890" Ref="C210"  Part="1" 
F 0 "C210" H 7150 3750 40  0000 L CNN
F 1 "1u" H 7156 3565 40  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 7188 3500 30  0001 C CNN
F 3 "~" H 7150 3650 60  0000 C CNN
	1    7150 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:Ferrite_Bead FB201
U 1 1 558C71BD
P 8950 3450
F 0 "FB201" V 9150 3450 60  0000 C CNN
F 1 "BLM15HG102SN1D" V 8750 3450 60  0000 C CNN
F 2 "Resistors_SMD:R_0402" H 8950 3450 60  0001 C CNN
F 3 "~" H 8950 3450 60  0000 C CNN
	1    8950 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:L L203
U 1 1 558C8089
P 4400 2750
F 0 "L203" H 4400 2850 50  0000 C CNN
F 1 "22n" H 4400 2700 50  0000 C CNN
F 2 "Resistors_SMD:R_0402_NoSilk" H 4400 2750 60  0001 C CNN
F 3 "~" H 4400 2750 60  0000 C CNN
	1    4400 2750
	0    -1   -1   0   
$EndComp
$Comp
L Device:L L202
U 1 1 558C816A
P 4200 3050
F 0 "L202" H 4200 3150 50  0000 C CNN
F 1 "120n" H 4200 3000 50  0000 C CNN
F 2 "Resistors_SMD:R_0402_NoSilk" H 4200 3050 60  0001 C CNN
F 3 "~" H 4200 3050 60  0000 C CNN
	1    4200 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C202
U 1 1 558C83F6
P 4000 3050
AR Path="/558C83F6" Ref="C202"  Part="1" 
AR Path="/559EC5B3/558C83F6" Ref="C202"  Part="1" 
F 0 "C202" H 4000 3150 40  0000 L CNN
F 1 "2p7" H 4006 2965 40  0000 L CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 4038 2900 30  0001 C CNN
F 3 "~" H 4000 3050 60  0000 C CNN
	1    4000 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:L L201
U 1 1 558C8401
P 3800 2750
F 0 "L201" H 3800 2850 50  0000 C CNN
F 1 "18n" H 3800 2700 50  0000 C CNN
F 2 "Resistors_SMD:R_0402_NoSilk" H 3800 2750 60  0001 C CNN
F 3 "~" H 3800 2750 60  0000 C CNN
	1    3800 2750
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C201
U 1 1 558C856A
P 3450 2750
AR Path="/558C856A" Ref="C201"  Part="1" 
AR Path="/559EC5B3/558C856A" Ref="C201"  Part="1" 
F 0 "C201" H 3450 2850 40  0000 L CNN
F 1 "33p" H 3456 2665 40  0000 L CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 3488 2600 30  0001 C CNN
F 3 "~" H 3450 2750 60  0000 C CNN
	1    3450 2750
	0    -1   -1   0   
$EndComp
Text HLabel 2750 2750 0    60   Input ~ 0
ANT
Text HLabel 2750 3700 0    60   Input ~ 0
GND
Text HLabel 9600 4500 2    60   Input ~ 0
+3.3V
Text HLabel 9600 4700 2    60   Input ~ 0
+1.8V
Text HLabel 9750 3850 2    60   Input ~ 0
GND
Text HLabel 4500 3850 0    60   Input ~ 0
#SPI/I2C
Text HLabel 6800 3050 2    60   Input ~ 0
I2S_CLK
Text HLabel 6800 3150 2    60   Input ~ 0
I2S_WS
Text HLabel 6800 3250 2    60   Output ~ 0
I2S_OUT
Text HLabel 4500 4050 0    60   Input ~ 0
SCLK/SCL
Text HLabel 4500 4150 0    60   Input ~ 0
SI/SDA
Text HLabel 4500 4250 0    60   Output ~ 0
SO/A0
Text HLabel 4500 3650 0    60   Input ~ 0
RESET
Text HLabel 4500 3950 0    60   Input ~ 0
CS/A1
$Comp
L Device:Crystal X201
U 1 1 559EDDEE
P 4350 4650
F 0 "X201" H 4350 4800 60  0000 C CNN
F 1 "19.2 Mhz" H 4350 4500 60  0000 C CNN
F 2 "" H 4350 4650 60  0001 C CNN
F 3 "~" H 4350 4650 60  0000 C CNN
	1    4350 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C203
U 1 1 559EDE11
P 4150 4850
AR Path="/559EDE11" Ref="C203"  Part="1" 
AR Path="/559EC5B3/559EDE11" Ref="C203"  Part="1" 
F 0 "C203" H 4150 4950 40  0000 L CNN
F 1 "16p" H 4156 4765 40  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4188 4700 30  0001 C CNN
F 3 "~" H 4150 4850 60  0000 C CNN
	1    4150 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C204
U 1 1 559EDE1C
P 4550 4850
AR Path="/559EDE1C" Ref="C204"  Part="1" 
AR Path="/559EC5B3/559EDE1C" Ref="C204"  Part="1" 
F 0 "C204" H 4550 4950 40  0000 L CNN
F 1 "16p" H 4556 4765 40  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4588 4700 30  0001 C CNN
F 3 "~" H 4550 4850 60  0000 C CNN
	1    4550 4850
	1    0    0    -1  
$EndComp
Text HLabel 4100 5050 0    60   Input ~ 0
GND
$Comp
L dabradio:SST25VF016B U202
U 1 1 55AE898A
P 8050 2700
F 0 "U202" H 7800 3000 60  0000 L CNN
F 1 "SST25VF016B" H 7800 2400 60  0000 L CNN
F 2 "Housings_SOIC:SOIJ-8_5.3x5.3mm_Pitch1.27mm" H 8050 2700 60  0001 C CNN
F 3 "" H 8050 2700 60  0000 C CNN
	1    8050 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C220
U 1 1 55AE8D64
P 8800 2800
AR Path="/55AE8D64" Ref="C220"  Part="1" 
AR Path="/559EC5B3/55AE8D64" Ref="C220"  Part="1" 
F 0 "C220" H 8800 2900 40  0000 L CNN
F 1 "100n" H 8806 2715 40  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 8838 2650 30  0001 C CNN
F 3 "~" H 8800 2800 60  0000 C CNN
	1    8800 2800
	1    0    0    -1  
$EndComp
Text HLabel 8850 2550 2    60   Input ~ 0
+3.3V
Text HLabel 8850 3050 2    60   Input ~ 0
GND
$Comp
L dabradio:CM1213A-01 D201
U 1 1 55AE991D
P 3250 3250
F 0 "D201" H 2950 3550 60  0000 L CNN
F 1 "CM1213A-01" H 3600 3550 60  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 3350 3450 60  0001 C CNN
F 3 "" H 3350 3450 60  0000 C CNN
	1    3250 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C214
U 1 1 55AEA86C
P 7650 3650
AR Path="/55AEA86C" Ref="C214"  Part="1" 
AR Path="/559EC5B3/55AEA86C" Ref="C214"  Part="1" 
F 0 "C214" H 7650 3750 40  0000 L CNN
F 1 "100n" H 7656 3565 40  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 7688 3500 30  0001 C CNN
F 3 "~" H 7650 3650 60  0000 C CNN
	1    7650 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C212
U 1 1 55AEA872
P 7450 3650
AR Path="/55AEA872" Ref="C212"  Part="1" 
AR Path="/559EC5B3/55AEA872" Ref="C212"  Part="1" 
F 0 "C212" H 7450 3750 40  0000 L CNN
F 1 "8p2" H 7456 3565 40  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 7488 3500 30  0001 C CNN
F 3 "~" H 7450 3650 60  0000 C CNN
	1    7450 3650
	1    0    0    -1  
$EndComp
Text HLabel 9750 3450 2    60   Input ~ 0
+1.8V
Wire Wire Line
	6700 5250 6700 5200
Wire Wire Line
	6900 5250 6900 5200
Connection ~ 6700 5250
Wire Wire Line
	7100 5250 7100 5200
Connection ~ 6900 5250
Wire Wire Line
	7400 5250 7400 5200
Connection ~ 7100 5250
Wire Wire Line
	7600 5250 7600 5200
Connection ~ 7400 5250
Wire Wire Line
	7800 5250 7800 5200
Connection ~ 7600 5250
Wire Wire Line
	8100 5250 8100 5200
Connection ~ 7800 5250
Wire Wire Line
	8300 5250 8300 5200
Connection ~ 8100 5250
Wire Wire Line
	8500 5250 8500 5200
Connection ~ 8300 5250
Wire Wire Line
	6300 4450 6350 4450
Connection ~ 6350 4850
Wire Wire Line
	6300 4550 6350 4550
Connection ~ 6350 4550
Wire Wire Line
	6350 4650 6300 4650
Connection ~ 6350 4650
Wire Wire Line
	6350 4750 6300 4750
Connection ~ 6350 4750
Wire Wire Line
	6350 4850 6300 4850
Wire Wire Line
	6300 4250 6400 4250
Wire Wire Line
	8300 4500 8300 4900
Wire Wire Line
	6300 4050 6550 4050
Wire Wire Line
	7150 3850 7150 3800
Wire Wire Line
	6300 4150 6500 4150
Wire Wire Line
	6900 4600 6900 4900
Wire Wire Line
	7600 4550 7600 4900
Wire Wire Line
	9100 4700 9600 4700
Wire Wire Line
	6750 3800 6750 3850
Wire Wire Line
	6950 3850 6950 3800
Connection ~ 6950 3850
Connection ~ 6750 3850
Wire Wire Line
	6300 3450 6750 3450
Wire Wire Line
	6700 4600 6700 4900
Wire Wire Line
	6300 4350 6450 4350
Wire Wire Line
	8500 4500 8500 4900
Wire Wire Line
	4600 2750 4550 2750
Wire Wire Line
	3950 2750 4000 2750
Wire Wire Line
	4200 2550 4200 2750
Wire Wire Line
	4200 2550 4600 2550
Connection ~ 4200 2750
Wire Wire Line
	4200 3350 4200 3200
Wire Wire Line
	4600 3050 4550 3050
Wire Wire Line
	4550 3050 4550 3150
Connection ~ 4550 3350
Wire Wire Line
	4600 3150 4550 3150
Connection ~ 4550 3150
Wire Wire Line
	4600 3250 4550 3250
Connection ~ 4550 3250
Wire Wire Line
	4000 2750 4000 2900
Connection ~ 4000 2750
Wire Wire Line
	4000 3700 4000 3350
Connection ~ 4200 3350
Wire Wire Line
	3600 2750 3650 2750
Connection ~ 4000 3350
Wire Wire Line
	2750 2750 3250 2750
Wire Wire Line
	3250 2750 3250 2800
Connection ~ 3250 2750
Wire Wire Line
	4500 3850 4600 3850
Wire Wire Line
	6800 3050 6700 3050
Wire Wire Line
	6800 3150 6700 3150
Wire Wire Line
	6800 3250 6700 3250
Wire Wire Line
	4500 4050 4600 4050
Wire Wire Line
	4600 4150 4500 4150
Wire Wire Line
	4500 4250 4600 4250
Wire Wire Line
	4500 3650 4600 3650
Wire Wire Line
	4600 3950 4500 3950
Wire Wire Line
	4500 4650 4550 4650
Wire Wire Line
	4550 4650 4550 4700
Connection ~ 4550 4650
Wire Wire Line
	4600 4450 4150 4450
Wire Wire Line
	4150 4450 4150 4650
Wire Wire Line
	4150 4650 4200 4650
Connection ~ 4150 4650
Wire Wire Line
	4100 5050 4150 5050
Wire Wire Line
	4550 5050 4550 5000
Wire Wire Line
	4150 5000 4150 5050
Connection ~ 4150 5050
Wire Wire Line
	6300 2550 7600 2550
Wire Wire Line
	8550 2650 8500 2650
Wire Wire Line
	8550 2350 8550 2550
Wire Wire Line
	8500 2550 8550 2550
Wire Wire Line
	8600 2750 8500 2750
Wire Wire Line
	8600 2300 8600 2750
Wire Wire Line
	6300 2650 7050 2650
Wire Wire Line
	7050 2650 7050 2300
Wire Wire Line
	8650 2850 8500 2850
Wire Wire Line
	8650 2250 8650 2850
Wire Wire Line
	7000 2750 7000 2250
Wire Wire Line
	6300 2750 7000 2750
Wire Wire Line
	6300 2850 7150 2850
Wire Wire Line
	7150 2850 7150 2650
Wire Wire Line
	7150 2650 7600 2650
Connection ~ 8550 2550
Wire Wire Line
	8800 2550 8800 2650
Connection ~ 8800 2550
Wire Wire Line
	8800 2950 8800 3050
Wire Wire Line
	7550 3050 8800 3050
Wire Wire Line
	7550 3050 7550 2850
Wire Wire Line
	7550 2850 7600 2850
Connection ~ 8800 3050
Wire Wire Line
	8550 2350 7550 2350
Wire Wire Line
	7550 2350 7550 2750
Wire Wire Line
	7550 2750 7600 2750
Wire Wire Line
	7050 2300 8600 2300
Wire Wire Line
	7000 2250 8650 2250
Wire Wire Line
	2750 3700 3050 3700
Wire Wire Line
	3050 3700 3050 3650
Wire Wire Line
	3450 3700 3450 3650
Connection ~ 3050 3700
Connection ~ 3450 3700
Wire Wire Line
	4000 3350 4200 3350
Wire Wire Line
	6750 3450 6750 3500
Wire Wire Line
	6950 3450 6950 3500
Wire Wire Line
	7150 3450 7150 3500
Connection ~ 6750 3450
Connection ~ 6950 3450
Connection ~ 7150 3450
Wire Wire Line
	6300 3850 6750 3850
Wire Wire Line
	7450 3500 7450 3450
Connection ~ 7450 3450
Wire Wire Line
	7650 3500 7650 3450
Connection ~ 7650 3450
Wire Wire Line
	9100 3450 9750 3450
Wire Wire Line
	7450 3800 7450 3850
Wire Wire Line
	7450 3850 7650 3850
Wire Wire Line
	7650 3850 7650 3800
Connection ~ 7650 3850
Wire Wire Line
	6400 5250 6700 5250
Wire Wire Line
	6350 4450 6350 4550
Wire Wire Line
	6550 4050 6550 4500
Wire Wire Line
	6550 4500 8100 4500
Connection ~ 8500 4500
Connection ~ 8300 4500
Wire Wire Line
	8100 4900 8100 4500
Connection ~ 8100 4500
Wire Wire Line
	6500 4150 6500 4550
Wire Wire Line
	6500 4550 7400 4550
Wire Wire Line
	7800 4550 7800 4700
Connection ~ 7600 4550
Wire Wire Line
	7400 4900 7400 4550
Connection ~ 7400 4550
Wire Wire Line
	6450 4350 6450 4600
Wire Wire Line
	7100 4600 7100 4700
Connection ~ 6900 4600
Connection ~ 6700 4600
Wire Wire Line
	6450 4600 6700 4600
Wire Wire Line
	7100 4700 7800 4700
Connection ~ 7100 4700
Connection ~ 7800 4700
Text HLabel 9600 5450 2    60   Input ~ 0
GND
Wire Wire Line
	6350 5350 8750 5350
$Comp
L Device:C C219
U 1 1 55AED1C5
P 8750 5050
AR Path="/55AED1C5" Ref="C219"  Part="1" 
AR Path="/559EC5B3/55AED1C5" Ref="C219"  Part="1" 
F 0 "C219" H 8750 5150 40  0000 L CNN
F 1 "1u" H 8756 4965 40  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 8788 4900 30  0001 C CNN
F 3 "~" H 8750 5050 60  0000 C CNN
	1    8750 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 4700 8750 4900
Connection ~ 8750 4700
Wire Wire Line
	8750 5200 8750 5350
Connection ~ 8750 5350
Text Notes 2650 4050 0    60   ~ 0
I2C Address: 11001xx
Text HLabel 4500 3750 0    60   Output ~ 0
IRQ
Wire Wire Line
	4600 3750 4500 3750
Wire Wire Line
	6300 3050 6400 3050
Wire Wire Line
	6400 3150 6300 3150
Wire Wire Line
	6300 3250 6400 3250
NoConn ~ 6300 3550
NoConn ~ 6300 3650
NoConn ~ 6300 3750
$Comp
L Device:Ferrite_Bead FB202
U 1 1 55B13C6B
P 8950 4700
F 0 "FB202" V 9100 4700 60  0000 C CNN
F 1 "BLM15HG102SN1D" V 8800 4450 60  0000 C CNN
F 2 "Resistors_SMD:R_0402" H 8950 4700 60  0001 C CNN
F 3 "~" H 8950 4700 60  0000 C CNN
	1    8950 4700
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R203
U 1 1 55B13154
P 6550 3250
F 0 "R203" V 6630 3250 40  0000 C CNN
F 1 "10" V 6557 3251 40  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 6480 3250 30  0001 C CNN
F 3 "~" H 6550 3250 30  0000 C CNN
	1    6550 3250
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R202
U 1 1 55B13144
P 6550 3150
F 0 "R202" V 6630 3150 40  0000 C CNN
F 1 "10" V 6557 3151 40  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 6480 3150 30  0001 C CNN
F 3 "~" H 6550 3150 30  0000 C CNN
	1    6550 3150
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R201
U 1 1 55B1312D
P 6550 3050
F 0 "R201" V 6630 3050 40  0000 C CNN
F 1 "10" V 6557 3051 40  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 6480 3050 30  0001 C CNN
F 3 "~" H 6550 3050 30  0000 C CNN
	1    6550 3050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6400 4250 6400 5250
Wire Wire Line
	6700 5250 6900 5250
Wire Wire Line
	6900 5250 7100 5250
Wire Wire Line
	7100 5250 7400 5250
Wire Wire Line
	7400 5250 7600 5250
Wire Wire Line
	7600 5250 7800 5250
Wire Wire Line
	7800 5250 8100 5250
Wire Wire Line
	8100 5250 8300 5250
Wire Wire Line
	8300 5250 8500 5250
Wire Wire Line
	6350 4850 6350 5350
Wire Wire Line
	6350 4550 6350 4650
Wire Wire Line
	6350 4650 6350 4750
Wire Wire Line
	6350 4750 6350 4850
Wire Wire Line
	6950 3850 7150 3850
Wire Wire Line
	6750 3850 6950 3850
Wire Wire Line
	4200 2750 4250 2750
Wire Wire Line
	4200 2750 4200 2900
Wire Wire Line
	4550 3350 4600 3350
Wire Wire Line
	4550 3150 4550 3250
Wire Wire Line
	4550 3250 4550 3350
Wire Wire Line
	4000 2750 4200 2750
Wire Wire Line
	4200 3350 4550 3350
Wire Wire Line
	4000 3350 4000 3200
Wire Wire Line
	3250 2750 3300 2750
Wire Wire Line
	4550 4650 4600 4650
Wire Wire Line
	4150 4650 4150 4700
Wire Wire Line
	4150 5050 4550 5050
Wire Wire Line
	8550 2550 8550 2650
Wire Wire Line
	8550 2550 8800 2550
Wire Wire Line
	8800 2550 8850 2550
Wire Wire Line
	8800 3050 8850 3050
Wire Wire Line
	3050 3700 3450 3700
Wire Wire Line
	3450 3700 4000 3700
Wire Wire Line
	6750 3450 6950 3450
Wire Wire Line
	6950 3450 7150 3450
Wire Wire Line
	7150 3450 7450 3450
Wire Wire Line
	7450 3450 7650 3450
Wire Wire Line
	7650 3450 8800 3450
Wire Wire Line
	7650 3850 9750 3850
Wire Wire Line
	8500 4500 9600 4500
Wire Wire Line
	8300 4500 8500 4500
Wire Wire Line
	8100 4500 8300 4500
Wire Wire Line
	7600 4550 7800 4550
Wire Wire Line
	7400 4550 7600 4550
Wire Wire Line
	6900 4600 7100 4600
Wire Wire Line
	6700 4600 6900 4600
Wire Wire Line
	7100 4700 7100 4900
Wire Wire Line
	7800 4700 7800 4900
Wire Wire Line
	7800 4700 8750 4700
Wire Wire Line
	8750 4700 8800 4700
Wire Wire Line
	8750 5350 9600 5350
$EndSCHEMATC
