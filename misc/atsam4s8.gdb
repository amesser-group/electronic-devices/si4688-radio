


define hookpost-load
  # initialize stack pointer from excpetion table
  set $r13=*((unsigned long*)&(exception_table) + 0)

  # set pc according exception table
  set $pc=*((unsigned long*)&(exception_table) + 1)
end

define hookpost-target
  monitor reset init

  delete mem
  set mem inaccessible-by-default on

  mem 0x00000000 0x00400000 rw nocache
  mem 0x00400000 0x00480000 ro nocache
  mem 0x00800000 0x00C00000 ro
  mem 0x20000000 0x20020000 rw nocache

  load
end

define run
  load
end

#target remote localhost:3333

#monitor reset halt
#load