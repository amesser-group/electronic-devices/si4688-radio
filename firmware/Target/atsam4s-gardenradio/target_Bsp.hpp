/*
 *  Copyright 2018-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_TARGETGARDENRADIO_BSP_HPP_
#define MUSICBOX_TARGETGARDENRADIO_BSP_HPP_

#include "target_Psp.hpp"
#include "ecpp/Peripherals/Display/EA/EA_OLEDM204.hpp"
#include "ecpp/Ui/Text/Driver.hpp"
#include "ecpp/Units/Ticks.hpp"

#include "spi.h"
#include "pio.h"

#undef max
#undef min

namespace target
{
  using namespace ::ecpp::Peripherals::Display;
  using namespace ::ecpp::Ui::Text;
  using ::ecpp::Units::Milliseconds;

  class BspGardenRadio : public Psp
  {
  public:
    class Display : public BufferedDisplayDriver<EAOLEDM204<'A'> >
    {
    public:
      void Update();
    };

    static constexpr ioport_pin_t kIOPortPinMaskKey[] = {PIO_PA10, PIO_PA11, PIO_PA12};

    static constexpr ioport_pin_t kIOPortPinDisplayReset = IOPORT_CREATE_PIN(PIOA, 15);

    static constexpr uint32_t kIOPortPinMaskSPI_A = (PIO_PA13A_MOSI | PIO_PA14A_SPCK);
    static constexpr uint32_t kIOPortPinMaskSPI_B = ( PIO_PA9B_NPCS1);

    static constexpr uint_least8_t kSPIChipSelectDisplay = 1;

    Display display_;

    static BspGardenRadio & getInstance() { return instance_; }
    static BspGardenRadio & instance() { return instance_; }

    const Max98089RegisterSequence (&max98089_init_registers())[7] {return kMax98089InitRegisters; }

    void Init();
    void HandleUserInput();

  private:
    System::Tick key_state_last_change_ticks_;

    ButtonState key_state_to_report_[3];
    ButtonState key_state_reported_[3];
    ButtonState key_state_debounced_[3];

    static BspGardenRadio instance_;

    static const uint8_t kMax98089RegisterSequence_PowerMgmt1[3];
    static const uint8_t kMax98089RegisterSequence_PowerMgmt2[2];

    static const Max98089RegisterSequence kMax98089InitRegisters[7];

    ButtonState GetCurrentKeyState(unsigned int idx) const { return (((PIOA->PIO_PDSR) & kIOPortPinMaskKey[idx])) ? ButtonState::Idle : ButtonState::Pressed; }
    bool        CheckKeyDebounced() const { return (System::getTicks() - key_state_last_change_ticks_) > Milliseconds(20); }

    friend void ::PIOA_Handler(void);
  };

  using Bsp = BspGardenRadio;
}
#endif  /* MUSICBOX_TARGETGARDENRADIO_BSP_HPP_ */