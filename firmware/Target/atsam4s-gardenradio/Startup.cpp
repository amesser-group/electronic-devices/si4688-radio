
#include "ecpp/HAL/ATSAM4S.hpp"

void Reset_Handler()
{
  /* enable peripheral clock for pio a */
  PMC->PMC_PCER0 =  (1 << ID_PIOA);

  /* configure PA10, the middle button */
  PIOA->PIO_ODR   = PIO_PA10;
  PIOA->PIO_PUER  = PIO_PA10;
  PIOA->PIO_PPDDR = PIO_PA10;
  PIOA->PIO_IFER  = PIO_PA10;
  PIOA->PIO_PER   = PIO_PA10;

  /** When middle button was pressed during power-on
   *  Go to SAM-BA monitor mode */
  if(0 == (PIOA->PIO_PDSR & PIO_PA10))
  {
    volatile uint32_t *pulRom = (uint32_t*)0x00800000;
    int val;

    __asm volatile (
        "ldr %0, [%1] \n"
        "msr msp, %0  \n"
        "ldr pc, [%1, #4] \n"
        : "=r" (val), "+r" (pulRom)
    );
  }

  DefaultReset_Handler();
}




