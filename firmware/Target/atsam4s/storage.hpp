/*
 * storage.hpp
 *
 *  Created on: 31.01.2019
 *      Author: andi
 */

#ifndef FIRMWARE_APP_SRC_STORAGE_HPP_
#define FIRMWARE_APP_SRC_STORAGE_HPP_

#include "Storage/StorageFormat.hpp"

#include "target_System.hpp"

extern "C"
{
#include "map.h"
#include "nand.h"
}

using namespace target;

class StorageManager : protected JobAble, protected ::MusicBox::Storage::BinaryStorage
{
public:
  void init();

  bool load_database();
  void store_database();

  void                  restoreStoredState();
  /** Save stored state considering a delay */
  void                  saveStoredState();

  static StorageManager & getInstance(const struct dhara_nand* n)
  {
    return container_of(StorageManager, m_Nand, n);
  }

  void databaseChanged();
  /** disable state saves */
  void setSupressSaves(bool suppress);

private:
  bool readBlock     (int s,       ::MusicBox::Storage::BinaryStorage::Blocks & buf);
  bool updateBlock   (int s, const ::MusicBox::Storage::BinaryStorage::Blocks & buf);
  void finalizeWrite (int end_sect);

  bool updateDescriptorBlock();

  struct dhara_nand       m_Nand;
  struct dhara_map        m_Map;

  StorageInfo             StorageInfo;

  System::Job             m_UpdateStorageJob {storeDatabase_job};
  static void             storeDatabase_job(System::Job& job);

  System::Job             SaveStateJob {saveState_job};
  System::Timer           SaveStateTimer;
  static void             saveState_job(System::Job& job);
  /** Save the current state of the box to flash */
  void                    saveState();
  /** Set to true if we shall not save data e.g. during scans etc */
  uint_least8_t           SuppressSaves;
};


#endif /* FIRMWARE_APP_SRC_STORAGE_HPP_ */
