/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cstring>

#include "target_Bsp.hpp"
#include "target_Globals.hpp"

using namespace target;

Globals Globals::instance_;

void Globals::Init()
{

  Storage.init();

  /* try to load database from permanent memory, if it fails
   * init with some reasonable default values */
  Storage.load_database();
}

void Globals::Start()
{
  State.Mode = MusicBoxMode::Radio;

  SiHandler.start();

  Ui.start();

  CardDriver.start();

  Storage.restoreStoredState();
}

void Globals::statusChanged()
{
  Globals & g = Globals::getInstance();

  g.Ui.handleStatusChange();
}

void Globals::databaseChanged(const Database::ServiceIterator &it)
{
  Globals & g = Globals::getInstance();

  g.Ui.handleChange(it);
  g.Storage.databaseChanged();
}

void Globals::notifyServiceStarted()
{
  Globals & g = Globals::getInstance();

  g.MaxHandler.setShutdown(false);
  g.Storage.saveStoredState();
  g.Ui.handleStatusChange();
}

void
Globals::setMode(MusicBoxMode mode)
{
  State.Mode = mode;

  switch(mode)
  {
  case MusicBoxMode::Off:
    MaxHandler.setShutdown(true);
    SiHandler.activateMode(Si468xOperationMode::Disabled);
    break;
  case MusicBoxMode::Radio:
    SiHandler.tuneChannel(SiHandler.getCurrentServiceIterator());
    break;
  };
}

template<>
void Globals::HandleEvent(Si468xEvent::OpmodeChanged ev)
{
  Ui.handleChange(ev.mode);

  if ( (ev.mode == Si468xOperationMode::FMRadio)  ||
       (ev.mode == Si468xOperationMode::DABRadio))
  {
    MaxHandler.handlePending();
  }
}