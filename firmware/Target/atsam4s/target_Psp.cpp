/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ecpp/Target/Bsp.hpp"
#include "target_Bsp.hpp"
#include "target_Globals.hpp"

#include "delay.h"
#include "hsmci.h"
#undef min
#undef max

using namespace target;

System System::instance_;

const uint16_t
Psp::ksi4688_board_properties_[] =
{
  PROP_DIGITAL_IO_OUTPUT_SELECT,      0x0000,
  PROP_PIN_CONFIG_ENABLE,             0x0002,
  0xFFFF
};

const uint8_t Psp::kMax98089RegisterSequence_Shutdown[2] =
{ /* Make sure device is off */
  Max98089Defs::REG_SYSTEM_SHUTDOWN, 0x00
};

const uint8_t Psp::kMax98089RegisterSequence_Voltage[2] =
{  /* 3.3 V Speaker amplifier supply voltage */
  Max98089Defs::REG_BATTERY_VOLTAGE, 0x08
};

/* Reference Clock & Audio interface setup */
const uint8_t Psp::kMax98089RegisterSequence_Clock[10] =
{
  Max98089Defs::REG_MASTER_CLOCK,
  0x20, /* PCLK = MCLK/2 = 12.288 MHz */
  /*DAI1 Clock Control */
  0x80,                                  /* 48 kHz Samplerate for ALC */
  (0x6000 >> 8) & 0x7F, 0x6000  & 0xFE,  /* Setup PLL for 48 khz samplerate from 24.576 Mhz Crystal and DHF=0 */
  /*DAI1 Configuration */
  0x90, /* I2S: Master, Second rising edge */
  0x01, /* I2S: BCLK = 64*LRCLK = 3.072 MHz-> suitable for 24bit output of si468x */// todo
  0x41, /* DAI1: Use Port S1, Playback enable */
  0x00,
  0x80, /* Set mode 1 for music mode */
};

/* Mixers configuration */
const uint8_t Psp::kMax98089RegisterSequence_MixerDAC[2] =
{
  Max98089Defs::REG_DAC_MIXER, 0x84 /* Connect DAC with DAI 1 Left & Right */
};

const uint8_t Psp::kMax98089RegisterSequence_MixerOut[6] =
{
  Max98089Defs::REG_LEFT_RECEIVER_AMPLIFIER_MIXER,
  0x01, /* Connect Left Lineout to Left DAC */
  0x01, /* Connect Right Lineout to Right DAC */
  0x80, /* Enable Stereo Lineout, Gain 0dB */
  0x01, /* Connect Left Speaker to Left DAC */
  0x01, /* Connect Right Speaker to Right DAC */
};

#if 0
  /* Volume control */
  {Max98089Defs::REG_LEFT_RECEIVER_AMPLIFIER_VOLUME_CONTROL,
    0x00, 0x00, /* Lineout to 0 */
    0x00, 0x00, /* Speaker to 0 */
  },
#endif

void
Psp::Init()
{
#ifndef CONF_BOARD_KEEP_WATCHDOG_AT_INIT
  /* Disable the watchdog */
  WDT->WDT_MR = WDT_MR_WDDIS;
#endif

  SysTick_Config(sysclk_get_cpu_hz() / System::Tick::Frequency);

  /* GPIO has been deprecated, the old code just keeps it for compatibility.
    * In new designs IOPORT is used instead.
    * Here IOPORT must be initialized for others to use before setting up IO.
    */
  ioport_init();

  /* setup twi */
  ioport_set_port_mode(IOPORT_PIOA,
                       PIO_PA3A_TWD0 | PIO_PA4A_TWCK0,
                       IOPORT_MODE_MUX_A);

  ioport_disable_port(IOPORT_PIOA,
                       PIO_PA3A_TWD0 | PIO_PA4A_TWCK0);

  ioport_set_pin_level(kIOPortPinDabReset,0);
  ioport_set_pin_dir(kIOPortPinDabReset, IOPORT_DIR_OUTPUT);

  /* prepare ioport for hsmci */
  ioport_set_port_mode(IOPORT_PIOA, kIOPortPinMaskHSMCI,
                       IOPORT_DIR_INPUT | IOPORT_MODE_MUX_C);

  /* switch hsmci pins to input */
  ioport_enable_port(IOPORT_PIOA, kIOPortPinMaskHSMCI);

  hsmci_init();

  /* configure sd card control pins */
  ioport_enable_pin(kIOPortPinSDPowerEn);
  ioport_set_pin_level(kIOPortPinSDPowerEn,1);
  ioport_set_pin_dir(kIOPortPinSDPowerEn, IOPORT_DIR_OUTPUT);

  ioport_enable_pin(kIOPortPinSDDetect);
  ioport_set_pin_dir(kIOPortPinSDDetect, IOPORT_DIR_INPUT);

  NVIC_SetPriority(SysTick_IRQn, 5);
  NVIC_EnableIRQ(SysTick_IRQn);

  NVIC_SetPriority(HSMCI_IRQn, 6);
  NVIC_EnableIRQ(HSMCI_IRQn);

  /* assume si468x is still booting */
  twi_blocked_ = true;
  ClearSi4688Reset();
}

void
Psp::HandleSystemTick()
{
  System::instance().tick();
}

void
Psp::HandleTwi(TwiTransfer<1> &twi)
{
  handle_twi_job_.setHandler(HandleTwiJob);
  enqueue(handle_twi_job_);
}

void Psp::HandleTwiJob(System::Job &job)
{
  auto & psp = container_of(Psp, handle_twi_job_, job);

  if(psp.twi_blocked_)
    return;

  if(psp.twihandler_max98089_.isTransferWaiting())
    psp.twihandler_max98089_.handleTransfer();

  if(psp.twihandler_si4688_.isTransferWaiting())
    psp.twihandler_si4688_.handleTransfer();
}

void
Psp::UnblockTwiJob(System::Job &job)
{
  auto & psp = container_of(Psp, unblock_twi_job_, job);

  psp.twi_blocked_ = false;
  psp.HandleTwi(psp.twihandler_si4688_);
}

void
Psp::HandleTwiTransferDone(TwiTransfer<1> &twi, bool success)
{
  if(&twihandler_max98089_ == &twi)
    Globals::instance().MaxHandler.notifyTwiFinished(success);

  if(&twihandler_si4688_ == &twi)
    Globals::instance().SiHandler.notifyTwiFinished(success);
}


template<>
void TwiTransfer<1>::notifyTransferRequested()
{
  Bsp::instance().HandleTwi(*this);
}

template<>
void TwiTransfer<1>::notifyTransferDone(bool success)
{
  Bsp::instance().HandleTwiTransferDone(*this, success);
}


bool
Psp::SetSi4688Reset()
{
  auto & m = Globals::getInstance().MaxHandler;

  if (twihandler_max98089_.isTransferWaiting())
  {
    return false;
  }
  else if (!m.isShutdown())
  {
    m.setShutdown(true);
    return false;
  }
  else
  {
    twi_blocked_ = true;
    ioport_set_pin_level(kIOPortPinDabReset, 0);
    return true;
  }
}

void
Psp::ClearSi4688Reset()
{
  ioport_set_pin_level(kIOPortPinDabReset, 1);

  unblock_twi_job_.setHandler(UnblockTwiJob);
  unblock_twi_timer_.start(Milliseconds(100), unblock_twi_job_);
}

bool
Psp::IsCardPlugged()
{
  if(ioport_get_pin_level(kIOPortPinSDDetect))
    return false;
  else
    return true;
}

void
Psp::SetCardPowerState(bool poweron)
{
  /* detach hsmci from pins */
  ioport_enable_port(IOPORT_PIOA, kIOPortPinMaskHSMCI);

  if(poweron)
    ioport_set_pin_level(kIOPortPinSDPowerEn ,0);
  else
    ioport_set_pin_level(kIOPortPinSDPowerEn,1);
}

void
Psp::SetCardSpeed(uint32_t freq)
{
  hsmci_select_device(0, freq, 1, 0);

  /* attach hsmci to port  */
  ioport_disable_port(IOPORT_PIOA, kIOPortPinMaskHSMCI);
}

void SysTick_Handler(void)
{
  Bsp::instance().HandleSystemTick();
}

namespace ecpp::Target::Bsp
{

  template<>
  void delay(Milliseconds<> timeout)
  {
    delay_ms(timeout.Value);
  }
}