#include "target_Bsp.hpp"
#include "helpers.hpp"
#include "compiler.h"
#include "conf_clock.h"
#include "sysclk.h"
#include "ioport.h"
#include "pio.h"
#include "twi_master.h"

using namespace target;

template<int REGLEN>
void
TwiTransfer<REGLEN>::performRead()
{
  Twi *p_twi = TWI0;
  uint8_t status;

  /* Set read mode, slave address and 3 internal address byte lengths */
  p_twi->TWI_MMR = 0;
  p_twi->TWI_MMR = TWI_MMR_MREAD | TWI_MMR_DADR(m_twi_addr) |
      ((REGLEN << TWI_MMR_IADRSZ_Pos) &  TWI_MMR_IADRSZ_Msk);

  /* Set internal address for remote chip */
  p_twi->TWI_IADR = 0;
  p_twi->TWI_IADR = twi_mk_addr(m_reg, REGLEN);


  p_twi->TWI_RPR  = (uint32_t)m_RWBuf;
  p_twi->TWI_RCR  = m_BufLen;

  p_twi->TWI_PTCR = TWI_PTCR_RXTEN;

  /* Send a START condition */
  p_twi->TWI_CR = TWI_CR_START;
  m_State = StateType::STATE_W_FINISH;

  while(1)
  {
    SrReg = p_twi->TWI_SR;

    if (SrReg & TWI_SR_NACK)
    {
      status = TWI_RECEIVE_NACK;
      break;
    }
    else if (SrReg & TWI_SR_ENDRX)
    {
      status = TWI_SUCCESS;
      break;
    }
  }

  p_twi->TWI_CR   = TWI_CR_STOP;
  p_twi->TWI_PTCR = TWI_PTCR_RXTDIS;

  while (!(p_twi->TWI_SR & TWI_SR_TXCOMP)) {
  }
  p_twi->TWI_SR;
  p_twi->TWI_RHR; /* clear rx transfer reg */

  if(status != TWI_SUCCESS)
    m_BufLen = 0;

  m_State = StateType::STATE_IDLE;

  notifyTransferDone(status == TWI_SUCCESS);
}

template<int REGLEN>
void
TwiTransfer<REGLEN>::performWrite()
{
  Twi *p_twi = TWI0;
  uint8_t status;

  /* Set read mode, slave address and 3 internal address byte lengths */
  p_twi->TWI_MMR = 0;
  p_twi->TWI_MMR = TWI_MMR_DADR(m_twi_addr) |
      ((REGLEN << TWI_MMR_IADRSZ_Pos) &  TWI_MMR_IADRSZ_Msk);

  /* Set internal address for remote chip */
  p_twi->TWI_IADR = 0;
  p_twi->TWI_IADR = twi_mk_addr(m_reg, REGLEN);


  p_twi->TWI_TPR  = (uint32_t)m_ROBuf;
  p_twi->TWI_TCR  = m_BufLen;


  m_State = StateType::STATE_W_FINISH;

  /* Send a START condition */
  p_twi->TWI_CR   = TWI_CR_START;

  /* now start dma */
  p_twi->TWI_PTCR = TWI_PTCR_TXTEN;
  while(1)
  {
    SrReg = p_twi->TWI_SR;

    if (SrReg & TWI_SR_NACK)
    {
      status = TWI_SEND_NACK;
      break;
    }
    else if (SrReg & TWI_SR_ENDTX)
    {
      status = TWI_SUCCESS;
      break;
    }
  }

  p_twi->TWI_CR   = TWI_CR_STOP;
  p_twi->TWI_PTCR = TWI_PTCR_TXTDIS;

  while (!(p_twi->TWI_SR & TWI_SR_TXCOMP)) {
  }
  p_twi->TWI_SR;

  m_State = StateType::STATE_IDLE;
  notifyTransferDone(status == TWI_SUCCESS);
}

template<int REGLEN>
void TwiTransfer<REGLEN>::handleTransfer()
{
  if(m_State == StateType::STATE_W_START_READ)
    performRead();
  else if(m_State == StateType::STATE_W_START_WRITE)
    performWrite();
}

template class TwiTransfer<1>;

void TwiHandler::init()
{
  twi_master_options_t opt = {0};

  opt.speed = 200000;
  opt.chip  = 0xff;

  // Initialize the TWI master driver.

  twi_master_enable(TWI0);
  twi_master_setup(TWI0, &opt);

  /* Initialize the TWIM Module */
}

