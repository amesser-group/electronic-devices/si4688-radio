/*
 * main.cpp
 *
 *  Created on: 04.08.2016
 *      Author: andi
 */

#include "target_System.hpp"
#include "target_Bsp.hpp"
#include "target_Globals.hpp"

#include "pmc.h"
#include "spi.h"
#include "compiler.h"
#include "conf_clock.h"
#include "sysclk.h"
#include "ioport.h"
#include "pio.h"
#include "delay.h"

using namespace target;

// static uint32_t gs_ul_spi_clock = 25000;

extern "C" void _init() {};

int main()
{
  SystemInit();

  sysclk_init();

  Bsp::instance().Init();

  Globals::instance().Init();

  TwiHandler::init();

  Enable_global_interrupt();

  Globals::instance().Start();

  while(1)
  {
    auto & s = System::getInstance();

    System::Job   *job = nullptr;
    System::Timer *pTimer;

    pTimer = s.getExpiredTimer();

    if(nullptr != pTimer)
      job = pTimer->getJob();

    if (nullptr == job)
      job = s.getNextJob();

    if(nullptr != job)
      job->run();
    else
      pmc_enable_sleepmode(0);
  }
}
