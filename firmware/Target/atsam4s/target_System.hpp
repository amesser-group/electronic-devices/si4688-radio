/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TARGET_SYSTEM_HPP_
#define TARGET_SYSTEM_HPP_

#include "ecpp/Execution/JobQueue.hpp"
#include "ecpp/Execution/JobTimer.hpp"
#include "ecpp/Units/Ticks.hpp"
#include "ecpp/HAL/ARM/NVIC.hpp"

namespace target
{
  class JobAble;
  class TimerBase;

  class System
  {
  public:
    typedef ::ecpp::Units::StaticFreqTick<10>                                 Tick;
    typedef ::ecpp::Execution::Job                                            Job;
    typedef ::ecpp::Execution::JobQueue<::ecpp::HAL::ARM::InterruptCritical>  JobQueue;

    typedef ::ecpp::Execution::Timer<TimerBase>   Timer;
    typedef ::ecpp::Execution::TimerQueue<Timer>  TimerQueue;

    static System & getInstance() {return instance_;}
    static System & instance()    {return instance_;}

    static Tick getTicks()        {return instance().Ticks;}

    Timer *getExpiredTimer() { return Timers.next(); }
    Job   *getNextJob()      { return Jobs.next(); }

    void tick() { ++Ticks;}

  protected:
    void enqueue(Job &job)   { Jobs.enqueue(job);}

    volatile Tick  Ticks;
    JobQueue       Jobs;
    TimerQueue     Timers;

    friend class TimerBase;
    friend class JobAble;

  private:
    static System instance_;
  };

  class TimerBase
  {
  public:
    typedef System::Tick TicksType;
  protected:
    static  System::Tick         getTicks() {return System::getInstance().getTicks(); }
    static  System::TimerQueue&  getQueue() {return System::getInstance().Timers; }
  };

  class JobAble
  {
  protected:
    static void enqueue(System::Job &job) { System::getInstance().enqueue(job); }
  };
};

#endif
