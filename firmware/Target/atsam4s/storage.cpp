/*
 *  Copyright 2018 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "target_Bsp.hpp"
#include "target_Globals.hpp"
#include "target_System.hpp"
#include "helpers.hpp"

#include "ecpp/Datatypes.hpp"
#include "ecpp/Meta/Math.hpp"
#include "ecpp/HAL/ATSAM4S/EEFC.hpp"

using namespace std;
using namespace ecpp;

using namespace target;
using namespace ::MusicBox::Storage;
using ::ecpp::HAL::ATSAM4S::EEFC;

/** 512 Byte Flashpage */
struct FlashPage
{
  union {
    uint8_t  m_Data8[512];
    uint32_t m_Data32[512 / sizeof (uint32_t)];
  };
};

/** One erase block.= 4096 Byte
 *
 * ATMSAM4S supports 8 page block erase on all memories
 */
struct EraseBlock
{
  static constexpr unsigned int m_PageCount = 8;

  FlashPage m_Pages[m_PageCount];
};

/** Area for permanent storage 16*4kB = 64kB */
struct StorageArea
{
  static constexpr unsigned int m_BlockCount = 16;

  EraseBlock m_Blocks[m_BlockCount];
};

extern StorageArea g_Storage;
static FlashPage   s_StorageDharaPageBuf;

void StorageManager::init()
{
  dhara_error_t err;

  m_Nand.log2_page_size = MetaLog<2,    sizeof(FlashPage)>::Result;
  m_Nand.log2_ppb       = MetaLog<2,    EraseBlock::m_PageCount>::Result;
  m_Nand.num_blocks     = StorageArea::m_BlockCount;

  dhara_map_init(&m_Map, &m_Nand, s_StorageDharaPageBuf.m_Data8, 16);

  dhara_map_resume(&m_Map, &err);
}

void StorageManager::setSupressSaves(bool suppress)
{
  if (SuppressSaves == suppress)
    return;

  SuppressSaves = suppress;

  if(!suppress)
  {
    saveStoredState();
    databaseChanged();
  }
}

void StorageManager::databaseChanged()
{
  enqueue(m_UpdateStorageJob);
}

void StorageManager::storeDatabase_job(System::Job& job)
{
  StorageManager & self = container_of(StorageManager, m_UpdateStorageJob, job);

  self.store_database();
}

#define STORAGE_BINARY_VERSION   0x01
#define STORAGE_BINARY_VERSION_2 0x02

/** Read block from flash */
bool
StorageManager::readBlock(int s, BinaryStorage::Blocks & buf)
{
  dhara_error_t err;
  return 0 == dhara_map_read(&m_Map, s, buf, &err);
}

bool
StorageManager::updateBlock(int s, const BinaryStorage::Blocks & buf)
{
  Blocks        read_buf;
  dhara_error_t err;

  if( 0 != dhara_map_read(&m_Map, s, read_buf, &err))
    return false;

  if (memcmp(read_buf, buf, sizeof(buf)))
    return 0 == dhara_map_write(&m_Map, s, buf, &err);
  else
    return true;
}

void
StorageManager::finalizeWrite (int end_sect)
{
  int stop_sect = m_Map.journal.nand->num_blocks << m_Map.journal.nand->log2_ppb;
  dhara_error_t err;

  /** trim all unused sectors */
  for(; end_sect < stop_sect; ++end_sect)
  {
    if(0 != dhara_map_trim(&m_Map, end_sect, &err))
      break;
  }

  /* synchronize write access */
  dhara_map_sync(&m_Map, &err);
}

bool StorageManager::load_database()
{
  auto & db  = Globals::getInstance().Db;
  Blocks      buf;
  unsigned int sector;
  unsigned int i;

  if(!readBlock(0, buf))
    return false;

  if(!buf.Descriptor.decode(StorageInfo))
    return false;

  if(StorageInfo.TranspondersBlockOffset > 0)
  { /* load all transponders from storage into ram */
    sector = StorageInfo.TranspondersBlockOffset;
    for(i = 0; i < db.getDabChannelList().MaxTransponders; sector++)
    {
      int count;

      if(!readBlock(sector, buf))
        return false;

      count = buf.Transponder.decode(i, db.getDabChannelList());

      if(count < 0)
        return false;

      i += count;

      if(count == 0)
        break;
    }
  }

  if(StorageInfo.ServicesBlockOffset > 0)
  { /* load all services into ram */
    sector = StorageInfo.ServicesBlockOffset;

    for(i = 0; i < db._max_services; sector++)
    {
      int count;

      if(!readBlock(sector, buf))
        return false;

      count = buf.Service.decode(i, db);

      if(count < 0)
        return false;

      i += count;

      if(count == 0)
        break;
    }
  }

  return true;
}

bool
StorageManager::updateDescriptorBlock()
{
  auto const & s = Globals::getInstance().SiHandler;
  Blocks buf;

  buf.Descriptor.encode(StorageInfo, s.getCurrentServiceIterator());

  return updateBlock(0, buf);
}

void StorageManager::store_database()
{
  auto & db  = Globals::getInstance().Db;
  unsigned int sector;

  if( SuppressSaves)
    return;

  sector = 1;
  { /* store all transponders */
    Blocks buf;
    unsigned int i;

    StorageInfo.TranspondersBlockOffset = sector;

    for(i = 0; i < db.getDabChannelList().MaxTransponders; )
    {

      int    count;

      count = buf.Transponder.encode(i, db.getDabChannelList());

      if(count < 0)
        return;

      if(!updateBlock(sector, buf))
        return;
      sector++;

      i += count;

      if(count == 0)
        break;
    }
  }

  { /* store all services*/
    Blocks buf;
    unsigned int i;

    StorageInfo.ServicesBlockOffset = sector;

    for(i = 0; i < db._max_services;)
    {
      int count;

      count = buf.Service.encode(i, db);

      if(count < 0)
        return;

      if(!updateBlock(sector, buf))
        return;
      sector++;

      i += count;

      if(count == 0)
        break;
    }
  }

  if(!updateDescriptorBlock())
    return;

  finalizeWrite(sector);
}

void
StorageManager::restoreStoredState()
{
  Blocks          buf;
  ::MusicBox::Database::ServiceIterator last;

  if(!readBlock(0, buf))
    return;

  if(buf.Descriptor.decode(last))
  {
    Globals::getInstance().SiHandler.tuneChannel(last);
  }
}

void
StorageManager::saveState()
{
  dhara_error_t err;

  if(!updateDescriptorBlock())
    return;

  /* synchronize write access */
  dhara_map_sync(&m_Map, &err);
}
void
StorageManager::saveState_job(System::Job &job)
{
  StorageManager & self = container_of(StorageManager, SaveStateJob, job);
  self.saveState();
}
void
StorageManager::saveStoredState()
{
  SaveStateTimer.start(Milliseconds(10000), SaveStateJob);
}


int dhara_nand_is_bad(const struct dhara_nand *n, dhara_block_t b)
{
  return false;
}

void dhara_nand_mark_bad(const struct dhara_nand *n, dhara_block_t b)
{
}

int dhara_nand_erase(const struct dhara_nand *n, dhara_block_t b,
         dhara_error_t *err)
{
  auto &per = EEFC::getInstance(0);
  unsigned int i;

  if (b >= g_Storage.m_BlockCount)
  {
    dhara_set_error(err, DHARA_E_MAX);
    return -1;
  }

  per.eraseSectors(g_Storage.m_Blocks[b].m_PageCount, g_Storage.m_Blocks[b].m_Pages[0].m_Data32);

  for(i = 0; i < ElementCount(g_Storage.m_Blocks[b].m_Pages); ++i)
  {
    if (!dhara_nand_is_free(n, b * g_Storage.m_Blocks[b].m_PageCount + i))
    {
      dhara_set_error(err, DHARA_E_BAD_BLOCK);
      return -1;
    }
  }

  return 0;
}

int
dhara_nand_prog(const struct dhara_nand *n, dhara_page_t p,
        const uint8_t *data, dhara_error_t *err)
{
  auto &per = EEFC::getInstance(0);
  unsigned int block, page;

  if (p >= g_Storage.m_BlockCount * g_Storage.m_Blocks[0].m_PageCount)
  {
    dhara_set_error(err, DHARA_E_MAX);
    return -1;
  }

  /* translate from dhara address to flash address */
  block = p / g_Storage.m_Blocks[0].m_PageCount;
  page  = p % g_Storage.m_Blocks[0].m_PageCount;

  per.writePages(1, g_Storage.m_Blocks[block].m_Pages[page].m_Data32, data);

  if(0 != memcmp(g_Storage.m_Blocks[block].m_Pages[page].m_Data8, data,
                sizeof(g_Storage.m_Blocks[block].m_Pages[page].m_Data8)))
  {
    dhara_set_error(err, DHARA_E_BAD_BLOCK);
    return -1;
  }

  return 0;
}

int
dhara_nand_is_free(const struct dhara_nand *n, dhara_page_t p)
{
  unsigned int block, page;
  unsigned int i;

  if (p >= g_Storage.m_BlockCount * g_Storage.m_Blocks[0].m_PageCount)
    return 0;

  /* translate from dhara address to flash address */
  block = p / g_Storage.m_Blocks[0].m_PageCount;
  page  = p % g_Storage.m_Blocks[0].m_PageCount;

  for(i = 0; i < ElementCount(g_Storage.m_Blocks[block].m_Pages[page].m_Data32); ++i)
  {
    if (0xFFFFFFFF != g_Storage.m_Blocks[block].m_Pages[page].m_Data32[i])
      return 0;
  }

  return 1;
}

int dhara_nand_read(const struct dhara_nand *n, dhara_page_t p,
      size_t offset, size_t length, uint8_t *data, dhara_error_t *err)
{
  unsigned int block, page;

  if (p >= g_Storage.m_BlockCount * g_Storage.m_Blocks[0].m_PageCount)
  {
    dhara_set_error(err, DHARA_E_MAX);
    return -1;
  }

  if ((length + offset) > sizeof(g_Storage.m_Blocks[0].m_Pages[0].m_Data8))
  {
    dhara_set_error(err, DHARA_E_MAX);
    return -1;
  }

  /* translate from dhara address to flash address */
  block = p / g_Storage.m_Blocks[0].m_PageCount;
  page  = p % g_Storage.m_Blocks[0].m_PageCount;

  memcpy(data, &(g_Storage.m_Blocks[block].m_Pages[page].m_Data8[offset]), length);

  return 0;
}

int dhara_nand_copy(const struct dhara_nand *n,
    dhara_page_t src, dhara_page_t dst, dhara_error_t *err)
{
  unsigned int block, page;

  if (src >= g_Storage.m_BlockCount * g_Storage.m_Blocks[0].m_PageCount)
  {
    dhara_set_error(err, DHARA_E_MAX);
    return -1;
  }

  if (dst >= g_Storage.m_BlockCount * g_Storage.m_Blocks[0].m_PageCount)
  {
    dhara_set_error(err, DHARA_E_MAX);
    return -1;
  }


  /* translate from dhara address to flash address */
  block = src / g_Storage.m_Blocks[0].m_PageCount;
  page  = src % g_Storage.m_Blocks[0].m_PageCount;

  return dhara_nand_prog(n, dst, g_Storage.m_Blocks[block].m_Pages[page].m_Data8, err);
}
