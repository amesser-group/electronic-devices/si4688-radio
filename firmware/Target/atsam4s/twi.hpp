/*
 * twi.hpp
 *
 *  Created on: 29.06.2017
 *      Author: andi
 */

#ifndef FIRMWARE_APP_SRC_TWI_HPP_
#define FIRMWARE_APP_SRC_TWI_HPP_

#include <cstdint>

class TwiHandler
{
public:
  static void init();
};

template<int REGLEN>
class TwiTransfer
{
private:
  enum class StateType : uint8_t
  {
    STATE_IDLE,
    STATE_W_START_READ,
    STATE_W_START_WRITE,
    STATE_W_FINISH
  };

  /** true when a transfer shall be hold */
  volatile StateType m_State;

  const uint8_t*  m_reg;

  union {
    const uint8_t*  m_ROBuf;
    uint8_t*        m_RWBuf;
  };

  size_t          m_BufLen;

  uint_least8_t   m_twi_addr;
  uint_least32_t  SrReg;

  void performRead();
  void performWrite();
public:
  void set_twi_addr(uint8_t addr) { m_twi_addr = addr;}

  template<typename T>
  T* getTwiBuffer()
  {
    return static_cast<T*>(m_RWBuf);
  }

  size_t getReadLen() const
  {
    if(m_State == StateType::STATE_IDLE)
      return m_BufLen;
    else
      return 0;
  }

  void read_req(const uint8_t *reg, void* buf, size_t buflen)
  {
    m_reg       = reg;
    m_RWBuf  = static_cast<uint8_t*>(buf);
    m_BufLen    = buflen;

    m_State = StateType::STATE_W_START_READ;

    notifyTransferRequested();
  }

  void write_req(const uint8_t *reg, const void* buf, size_t buflen)
  {
    m_reg      = reg;
    m_ROBuf = static_cast<const uint8_t*>(buf);
    m_BufLen   = buflen;

    m_State = StateType::STATE_W_START_WRITE;

    notifyTransferRequested();
  }


  bool isTransferWaiting() const
  {
    return (m_State == StateType::STATE_W_START_READ) ||
           (m_State == StateType::STATE_W_START_WRITE);
  }

  bool isTransferPending() const
  {
    return m_State != StateType::STATE_IDLE;
  }

  void handleTransfer();

  void notifyTransferRequested();
  void notifyTransferDone(bool success);
};



#endif /* FIRMWARE_APP_SRC_TWI_HPP_ */
