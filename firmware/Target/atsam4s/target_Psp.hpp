/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TARGET_PSP_HPP_
#define TARGET_PSP_HPP_

#include <cstdbool>

#include "target_System.hpp"

#include "ecpp/HAL/ATSAM4S/Exceptions.hpp"
#include "ioport.h"

/* these macros are set through atmel headers */
#undef max
#undef min

#include "twi.hpp"
#include "Si4688/Driver.hpp"
#include "max98089.hpp"

namespace target
{
  enum class ButtonState : uint_least8_t
  {
    Idle    = 0,
    Down    = 1,
    Pressed = 2,
    Up      = 3,
  };

  class Psp : protected JobAble
  {
  public:
    static constexpr uint8_t kI2CAddr_Si4688   = 0x64;
    static constexpr uint8_t kI2CAddr_MAX98089 = 0x10;

    static constexpr ioport_pin_t kIOPortPinDabReset   = IOPORT_CREATE_PIN(PIOA, 2);
    static constexpr ioport_pin_t kIOPortPinSDPowerEn  = IOPORT_CREATE_PIN(PIOA, 5);
    static constexpr ioport_pin_t kIOPortPinSDDetect   = IOPORT_CREATE_PIN(PIOA, 6);

    static constexpr uint32_t kIOPortPinMaskHSMCI      = (PIO_PA28C_MCCDA | PIO_PA29C_MCCK | PIO_PA30C_MCDA0 | PIO_PA31C_MCDA1 | PIO_PA26C_MCDA2 | PIO_PA27C_MCDA3);

    void Init();

    TwiTransfer<1> & GetTwiHandler(::MusicBox::Si4688Driver&)    {return twihandler_si4688_;  }
    TwiTransfer<1> & GetTwiHandler(Max98089Manager&) {return twihandler_max98089_; }

    void HandleTwi(TwiTransfer<1> &twi);
    void HandleTwiTransferDone(TwiTransfer<1> &twi, bool success);

    bool SetSi4688Reset();
    void ClearSi4688Reset();

    const uint16_t* si4688_board_properties(::MusicBox::Si4688Driver&) const {return ksi4688_board_properties_;}

    bool IsCardPlugged();
    void SetCardPowerState(bool poweron);
    void SetCardSpeed(uint32_t freq);

  protected:
    void HandleSystemTick();

    static const uint8_t kMax98089RegisterSequence_Shutdown[2];
    static const uint8_t kMax98089RegisterSequence_Voltage[2];
    static const uint8_t kMax98089RegisterSequence_Clock[10];
    static const uint8_t kMax98089RegisterSequence_MixerDAC[2];
    static const uint8_t kMax98089RegisterSequence_MixerOut[6];

  private:
    TwiTransfer<1> twihandler_si4688_;
    TwiTransfer<1> twihandler_max98089_;

    System::Job   handle_twi_job_    {};
    System::Job   unblock_twi_job_   {};
    System::Timer unblock_twi_timer_ {};

    uint_least8_t  twi_blocked_;

    static const uint16_t ksi4688_board_properties_[];

    static void HandleTwiJob(System::Job &job);
    static void UnblockTwiJob(System::Job &job);

    friend void ::SysTick_Handler();
  };
};

#endif