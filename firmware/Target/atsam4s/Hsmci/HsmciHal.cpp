
/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the MusicBox project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */

#include "Hsmci/HsmciHal.hpp"
#include "hsmci.h"

using namespace MusicBox;

bool
HsmciHal::isReady()
{
  return 0 != (HSMCI->HSMCI_SR & HSMCI_SR_CMDRDY);
}

HsmciState
HsmciHal::getHsmciState()
{
  uint32_t sr;

  sr = HSMCI->HSMCI_SR;

  if (sr & SRStatusMask)
  {
    /* Todo: Check if reset is really needed, I dont think so */
    reset();
    return HsmciState::CommandFailed;
  }

  if ( 0 == (sr & HSMCI_SR_CMDRDY))
    return HsmciState::CommandPending;

  if ( 0 == (sr & HSMCI_SR_XFRDONE))
    return HsmciState::TransferPending;

  if( 0 == (sr & HSMCI_SR_NOTBUSY))
    return HsmciState::Busy;

  return HsmciState::Success;
}

void
HsmciHal::reset()
{
  uint32_t mr = HSMCI->HSMCI_MR;
  uint32_t dtor = HSMCI->HSMCI_DTOR;
  uint32_t sdcr = HSMCI->HSMCI_SDCR;
  uint32_t cstor = HSMCI->HSMCI_CSTOR;
  uint32_t cfg = HSMCI->HSMCI_CFG;
  HSMCI->HSMCI_CR = HSMCI_CR_SWRST;
  HSMCI->HSMCI_MR = mr;
  HSMCI->HSMCI_DTOR = dtor;
  HSMCI->HSMCI_SDCR = sdcr;
  HSMCI->HSMCI_CSTOR = cstor;
  HSMCI->HSMCI_CFG = cfg;
#ifdef HSMCI_SR_DMADONE
  HSMCI->HSMCI_DMA = 0;
#endif
#if (SAMV70 || SAMV71 || SAME70 || SAMS70)
#ifdef HSMCI_DMA_DMAEN
  HSMCI->HSMCI_DMA = 0;
#endif
#endif
  // Enable the HSMCI
  HSMCI->HSMCI_CR = HSMCI_CR_PWSEN | HSMCI_CR_MCIEN;
}

void
HsmciHal::startSendClocks()
{
  // Configure command
  HSMCI->HSMCI_MR &= ~(HSMCI_MR_WRPROOF | HSMCI_MR_RDPROOF | HSMCI_MR_FBYTE);
  // Write argument
  HSMCI->HSMCI_ARGR = 0;
  // Write and start initialization command
  HSMCI->HSMCI_CMDR = HSMCI_CMDR_RSPTYP_NORESP
                    | HSMCI_CMDR_SPCMD_INIT
                    | HSMCI_CMDR_OPDCMD_OPENDRAIN;

  HSMCI->HSMCI_IER = HSMCI_IER_CMDRDY | HSMCI_IER_XFRDONE;
}

void
HsmciHal::issueCommand(uint32_t cmdr, uint32_t cmd, uint32_t arg)
{
  cmdr |= HSMCI_CMDR_CMDNB(cmd) | HSMCI_CMDR_SPCMD_STD;

  if (cmd & SDMMC_RESP_PRESENT) {
    cmdr |= HSMCI_CMDR_MAXLAT;
    if (cmd & SDMMC_RESP_136) {
      cmdr |= HSMCI_CMDR_RSPTYP_136_BIT;
    } else if (cmd & SDMMC_RESP_BUSY) {
      cmdr |= HSMCI_CMDR_RSPTYP_R1B;
    } else {
      cmdr |= HSMCI_CMDR_RSPTYP_48_BIT;
    }
  }

  if (cmd & SDMMC_CMD_OPENDRAIN) {
    cmdr |= HSMCI_CMDR_OPDCMD_OPENDRAIN;
  }

  SRStatusMask =  (HSMCI_SR_CSTOE | HSMCI_SR_RTOE
                  | HSMCI_SR_RENDE
                  | HSMCI_SR_RDIRE | HSMCI_SR_RINDE);

  if(cmd & SDMMC_RESP_CRC)
    SRStatusMask |= HSMCI_SR_RCRCE;


  // Write argument
  HSMCI->HSMCI_ARGR = arg;
  // Write and start command
  HSMCI->HSMCI_CMDR = cmdr;

  HSMCI->HSMCI_IER = HSMCI_IER_XFRDONE;
}

void
HsmciHal::startCommand(uint32_t cmd, uint32_t arg)
{
  uint32_t cmdr;

  // Configure command
  HSMCI->HSMCI_MR &= ~(HSMCI_MR_WRPROOF | HSMCI_MR_RDPROOF | HSMCI_MR_FBYTE);
#ifdef HSMCI_SR_DMADONE
  // Disable DMA for HSMCI
  HSMCI->HSMCI_DMA = 0;
#endif
#ifdef HSMCI_MR_PDCMODE
  // Disable PDC for HSMCI
  HSMCI->HSMCI_MR &= ~HSMCI_MR_PDCMODE;
#endif
#if (SAMV70 || SAMV71 || SAME70 || SAMS70)
#ifdef HSMCI_DMA_DMAEN
  // Disable DMA for HSMCI
  HSMCI->HSMCI_DMA = 0;
#endif
#endif
  HSMCI->HSMCI_BLKR = 0;

  /* make sure pdc is off */
  HSMCI->HSMCI_PTCR = HSMCI_PTCR_RXTDIS | HSMCI_PTCR_TXTDIS;

  issueCommand(0, cmd, arg);
}

void
HsmciHal::startDMACommand(uint32_t cmd, uint32_t arg, void* buf, uint32_t num_blocks)
{
  constexpr uint32_t blocksize = 512;
  HsmciState status;
	uint32_t cmdr;
  uint32_t mr;

  mr = HSMCI->HSMCI_MR;

  // Enabling Read/Write Proof allows to stop the HSMCI Clock during
  // read/write  access if the internal FIFO is full.
	// This will guarantee data integrity, not bandwidth.
	mr |= HSMCI_MR_WRPROOF | HSMCI_MR_RDPROOF;
	// Force byte transfer if needed

  if( reinterpret_cast<uintptr_t>(buf) & 0x3)
    mr |= HSMCI_MR_FBYTE;
  else
    mr &= ~HSMCI_MR_FBYTE;


  HSMCI->HSMCI_MR = mr;

  if (0 == (HSMCI->HSMCI_SR & HSMCI_SR_FIFOEMPTY))
  { /* Fifo is not empty but should be */
    SRStatusMask = 0xFFFFFFFF;
    return;
  }

  /* make sure pdc is off */
  HSMCI->HSMCI_PTCR = HSMCI_PTCR_RXTDIS | HSMCI_PTCR_TXTDIS;

  /* dma must be started after issuing the command. Otherwise
   * write will fail */
  if (cmd & SDMMC_CMD_WRITE)
  {
    // Configure PDC transfer
    HSMCI->HSMCI_TPR  = reinterpret_cast<uintptr_t>(buf);
    HSMCI->HSMCI_TCR  = num_blocks * ((mr & HSMCI_MR_FBYTE) ? (blocksize) : (blocksize / 4));
    HSMCI->HSMCI_TNCR = 0;

    /* do not fire off dma here, it will fail */
    cmdr = HSMCI_CMDR_TRCMD_START_DATA | HSMCI_CMDR_TRDIR_WRITE;
  }
  else
  {
    mr |= HSMCI_MR_PDCMODE;

    // Configure PDC transfer
    HSMCI->HSMCI_RPR = reinterpret_cast<uintptr_t>(buf);
    HSMCI->HSMCI_RCR = num_blocks * ((mr & HSMCI_MR_FBYTE) ? (blocksize) : (blocksize / 4));
    HSMCI->HSMCI_RNCR = 0;

    cmdr = HSMCI_CMDR_TRCMD_START_DATA | HSMCI_CMDR_TRDIR_READ;
  }

	if (cmd & SDMMC_CMD_SDIO_BYTE) {
			cmdr |= HSMCI_CMDR_TRTYP_BYTE;
			// Value 0 corresponds to a 512-byte transfer
			HSMCI->HSMCI_BLKR = ((blocksize % 512) << HSMCI_BLKR_BCNT_Pos);
	} else {
		HSMCI->HSMCI_BLKR = (blocksize << HSMCI_BLKR_BLKLEN_Pos) |
				(num_blocks << HSMCI_BLKR_BCNT_Pos);
		if (cmd & SDMMC_CMD_SDIO_BLOCK) {
			cmdr |= HSMCI_CMDR_TRTYP_BLOCK;
		} else if (cmd & SDMMC_CMD_STREAM) {
			cmdr |= HSMCI_CMDR_TRTYP_STREAM;
		} else if (cmd & SDMMC_CMD_SINGLE_BLOCK) {
			cmdr |= HSMCI_CMDR_TRTYP_SINGLE;
		} else if (cmd & SDMMC_CMD_MULTI_BLOCK) {
			cmdr |= HSMCI_CMDR_TRTYP_MULTIPLE;
		} else {
			Assert(false); // Incorrect flags
		}
	}

	issueCommand(cmdr, cmd, arg);

  /* according to example code we need to wait here */
  while(HsmciState::CommandPending == getHsmciState());

  if (cmd & SDMMC_RESP_BUSY)
    while(HsmciState::Busy == getHsmciState());

  /* write dma must be started after issuing the command. Otherwise
   * write will be lost */
  if (cmd & SDMMC_CMD_WRITE)
  {
    /* HSMCI->HSMCI_PTCR = HSMCI_PTCR_TXTEN; */
    const uint32_t *data = reinterpret_cast<const uint32_t*>(buf);
    int i;

    for(i=0; i < (512/4); ++i)
    {
      while( 0 == (HSMCI->HSMCI_SR & HSMCI_SR_TXRDY));
      HSMCI->HSMCI_TDR = data[i];
    }
  }
  else
  {
    HSMCI->HSMCI_PTCR = HSMCI_PTCR_RXTEN;
  }
}

void
HsmciHal::startAppCommand(uint32_t cmd, uint32_t arg)
{
  volatile uint32_t res;

  startCommand(SDMMC_CMD55_APP_CMD, 0);

  while(HsmciState::CommandPending == getHsmciState());

  res = hsmci_get_response();

  startCommand(cmd, arg);
}
