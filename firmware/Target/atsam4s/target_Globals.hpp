#ifndef TARGET_GLOBALS_HPP_
#define TARGET_GLOBALS_HPP_

#include "MusicBox.hpp"
#include "Si4688/Driver.hpp"
#include "max98089.hpp"
#include "storage.hpp"
#include "DrawContext.hpp"
#include "Hsmci/HsmciDriver.hpp"

namespace target
{
  using namespace MusicBox;

  class Globals
  {
  public:
    static Globals & getInstance() {return instance_;}
    static Globals & instance()    {return instance_;}

    template<typename Sender, typename Event>
    static void Notify(Sender &s, Event ev) {instance().HandleEvent(ev);}

    void Init();
    void Start();
    static void statusChanged();
    static void databaseChanged(const Database::Database::HandleIteratorType &it);
    static void notifyServiceStarted();

    void        setMode(MusicBoxMode mode);

    MusicBoxGlobalState             State;
    Si4688Driver                    SiHandler;
    Max98089Manager                 MaxHandler;
    Lcd4x20DrawContext              Ui;
    Database::Database              Db;
    StorageManager                  Storage;
    HsmciDriver                     CardDriver;
    ElmChanVolumeManager            VolumeManager;

  private:
    static Globals instance_;

    template<typename Event>
    void HandleEvent(Event ev);
  };
};
#endif