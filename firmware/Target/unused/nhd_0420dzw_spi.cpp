/*
 * lcd.cpp
 *
 *  Created on: 30.03.2017
 *      Author: andi
 */


#include "app.hpp"
#include "conf_board.h"
#include "spi.h"
#include "delay.h"

#define ioport_set_pin_peripheral_mode(pin, mode) \
  do {\
    ioport_set_pin_mode(pin, mode);\
    ioport_disable_pin(pin);\
  } while (0)

#define SPI_MISO_GPIO                  (PIO_PA12_IDX)
#define SPI_MISO_FLAGS                 (PIO_PERIPH_A | PIO_DEFAULT)
#define SPI_MISO_MASK                  PIO_PA12
#define SPI_MISO_PIO                   PIOA
#define SPI_MISO_ID                    ID_PIOA
#define SPI_MISO_TYPE                  PIO_PERIPH_A
#define SPI_MISO_ATTR                  PIO_DEFAULT

/** SPI MOSI pin definition. */
#define SPI_MOSI_GPIO                  (PIO_PA13_IDX)
#define SPI_MOSI_FLAGS                 (PIO_PERIPH_A | PIO_DEFAULT)
#define SPI_MOSI_MASK                  PIO_PA13
#define SPI_MOSI_PIO                   PIOA
#define SPI_MOSI_ID                    ID_PIOA
#define SPI_MOSI_TYPE                  PIO_PERIPH_A
#define SPI_MOSI_ATTR                  PIO_DEFAULT

/** SPI SPCK pin definition. */
#define SPI_SPCK_GPIO                  (PIO_PA14_IDX)
#define SPI_SPCK_FLAGS                 (PIO_PERIPH_A | PIO_DEFAULT)
#define SPI_SPCK_MASK                  PIO_PA14
#define SPI_SPCK_PIO                   PIOA
#define SPI_SPCK_ID                    ID_PIOA
#define SPI_SPCK_TYPE                  PIO_PERIPH_A
#define SPI_SPCK_ATTR                  PIO_DEFAULT

/** SPI chip select 0 pin definition. */
#define SPI_NPCS0_GPIO                 (PIO_PA11_IDX)
#define SPI_NPCS0_FLAGS                (PIO_PERIPH_A | PIO_DEFAULT)
#define SPI_NPCS0_MASK                 PIO_PA11
#define SPI_NPCS0_PIO                  PIOA
#define SPI_NPCS0_ID                   ID_PIOA
#define SPI_NPCS0_TYPE                 PIO_PERIPH_A
#define SPI_NPCS0_ATTR                 PIO_DEFAULT

void lcd_init()
{
  ioport_set_pin_peripheral_mode(SPI_MISO_GPIO, SPI_MISO_FLAGS);
  ioport_set_pin_peripheral_mode(SPI_MOSI_GPIO, SPI_MOSI_FLAGS);
  ioport_set_pin_peripheral_mode(SPI_SPCK_GPIO, SPI_SPCK_FLAGS);
  ioport_set_pin_peripheral_mode(SPI_NPCS0_GPIO, SPI_NPCS0_FLAGS);

  spi_enable_clock(SPI_MASTER_BASE);

  spi_disable(SPI_MASTER_BASE);
  spi_reset(SPI_MASTER_BASE);
  spi_set_lastxfer(SPI_MASTER_BASE);
  spi_set_master_mode(SPI_MASTER_BASE);
  spi_disable_mode_fault_detect(SPI_MASTER_BASE);
  spi_set_fixed_peripheral_select(SPI_MASTER_BASE);
  spi_set_clock_polarity(SPI_MASTER_BASE, SPI_LCD_CHIP_SEL, SPI_LCD_CLK_POLARITY);
  spi_set_clock_phase(SPI_MASTER_BASE, SPI_LCD_CHIP_SEL, SPI_LCD_CLK_PHASE);
  spi_set_bits_per_transfer(SPI_MASTER_BASE, SPI_LCD_CHIP_SEL,
      SPI_CSR_BITS_10_BIT);
  spi_set_baudrate_div(SPI_MASTER_BASE, SPI_LCD_CHIP_SEL,
      (sysclk_get_peripheral_hz() / gs_ul_spi_clock));
  spi_set_transfer_delay(SPI_MASTER_BASE, SPI_LCD_CHIP_SEL, SPI_LCD_DLYBS,
      SPI_LCD_DLYBCT);
  spi_enable(SPI_MASTER_BASE);

  const uint16_t buffer[] = {
      0x38,
      0x08,
      0x01,
      0x06,
      0x02,
      0x0C,
  };

  unsigned int i;

  spi_set_peripheral_chip_select_value(SPI_MASTER_BASE, SPI_LCD_CHIP_PCS);

  for (i = 0; i < (sizeof(buffer) / sizeof(buffer[0])); i++) {
    uint16_t data;
    uint8_t uc_pcs;

    spi_write(SPI_MASTER_BASE, buffer[i], 0, 0);
    /* Wait transfer done. */
    while ((spi_read_status(SPI_MASTER_BASE) & SPI_SR_RDRF) == 0);
    spi_read(SPI_MASTER_BASE, &data, &uc_pcs);
  }

  spi_set_peripheral_chip_select_value(SPI_MASTER_BASE, 0xF);
}

static void
lcd_command(uint16_t cmd)
{
  delay_ms(10);

  spi_set_bits_per_transfer(
      SPI_MASTER_BASE, SPI_LCD_CHIP_SEL,
      SPI_CSR_BITS_10_BIT
  );

  spi_set_peripheral_chip_select_value(SPI_MASTER_BASE, SPI_LCD_CHIP_PCS);

  spi_write(SPI_MASTER_BASE, cmd,0,0);

  while ((spi_read_status(SPI_MASTER_BASE) & SPI_SR_RDRF) == 0);

  spi_get(SPI_MASTER_BASE);

  spi_set_peripheral_chip_select_value(SPI_MASTER_BASE, 0xF);
}

void lcd_loc(unsigned int col, unsigned int row)
{
  switch(row)
  {
  case 0: lcd_command(0x080 + 0x00 + col); break;
  case 1: lcd_command(0x080 + 0x40 + col); break;
  case 2: lcd_command(0x080 + 0x14 + col); break;
  case 3: lcd_command(0x080 + 0x54 + col); break;
  }
}


static constexpr char convert_null_char(const char c)
{
  return (c == 0) ? ' ' : c;
}

void lcd_write(const char* text)
{
  lcd_write(text, strlen(text));
}

void lcd_write(const char* text, size_t textlen)
{
  if(textlen > 0)
  {
    delay_ms(10);

    spi_set_bits_per_transfer(
        SPI_MASTER_BASE, SPI_LCD_CHIP_SEL,
        SPI_CSR_BITS_10_BIT
    );

    spi_set_peripheral_chip_select_value(SPI_MASTER_BASE, SPI_LCD_CHIP_PCS);

    spi_write(SPI_MASTER_BASE, 0x200 | convert_null_char(*text),0,0);
    text++;
    textlen--;

    while ((spi_read_status(SPI_MASTER_BASE) & SPI_SR_RDRF) == 0);

    spi_get(SPI_MASTER_BASE);

    spi_set_bits_per_transfer(
        SPI_MASTER_BASE, SPI_LCD_CHIP_SEL,
        SPI_CSR_BITS_8_BIT
    );

    for(;textlen; ++text, --textlen)
    {
      spi_write(SPI_MASTER_BASE, convert_null_char(*text),0,0);

      while ((spi_read_status(SPI_MASTER_BASE) & SPI_SR_RDRF) == 0);

      spi_get(SPI_MASTER_BASE);
    }

    spi_set_peripheral_chip_select_value(SPI_MASTER_BASE, 0xF);
  }
}
