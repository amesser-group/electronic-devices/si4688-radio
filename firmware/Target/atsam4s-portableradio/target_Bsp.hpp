/*
 * board.h
 *
 *  Created on: 11.08.2016
 *      Author: andi
 */

#ifndef FIRMWARE_APP_SRC_BSP_H_
#define FIRMWARE_APP_SRC_BSP_H_

#include <cstdbool>
#include <cstdio>

#include "target_System.hpp"
#include "target_Psp.hpp"

#include "ecpp/Peripherals/Display/NHD/NHD_0420DZW.hpp"
#include "ecpp/Peripherals/Display/CharacterMapping.hpp"
#include "ecpp/Ui/Widget/Text/DrawContext.hpp"
#include "ecpp/Ui/Text/Driver.hpp"

#include "ecpp/Units/Time.hpp"
#include "ecpp/Units/Ticks.hpp"

#include "Si4688/Driver.hpp"

#include "max98089.hpp"
#include "conf_board.h"
#include "sam4s.h"

namespace MusicBox
{
  using namespace ::target;
  using namespace ::ecpp::Peripherals::Display;
  using namespace ::ecpp::Ui::Text;

  using ::ecpp::Units::Milliseconds;



  template<unsigned FREQ, typename VALUETYPE = unsigned>
  class TickDelta
  {
  public:
    typedef VALUETYPE ValueType;

    static constexpr unsigned Frequency = FREQ;

    constexpr TickDelta() {};
    constexpr TickDelta(ValueType init) : Delta(init) {};

    template<typename T>
    constexpr bool operator > (const Milliseconds<T> & rhs) const
    {
      return Delta > (rhs.Interval + (1000 / FREQ) - 1) / (1000 / FREQ);
    }

  private:
    ValueType Delta {0};
  };


  template<unsigned int SAMPLES, typename VALUETYPE>
  class SmoothedAnalogValue
  {
  public:
    typedef VALUETYPE ValueType;

    void sample(ValueType sample)
    {
      SampleSum = SampleSum - SampleSum / SAMPLES + sample;
    }

    ValueType operator * ()
    {
      ValueType v = (SampleSum + RoundAdd) / SAMPLES;

      if (v < LastValue)
        RoundAdd = SAMPLES / 4;
      else if (v > LastValue)
        RoundAdd = SAMPLES / 4 + SAMPLES / 2;
      LastValue = v;

      return v;
    }

    /** Return value further downscaled */
    template<typename RHS>
    ValueType operator / (RHS rhs)
    {
      ValueType r = (SampleSum + RoundAdd * rhs) / (SAMPLES * rhs);

      if ((r * rhs) < LastValue)
        RoundAdd = SAMPLES / 4;
      else if ((r * rhs) > LastValue)
        RoundAdd = SAMPLES / 4 + SAMPLES / 2;

      LastValue = r*rhs;
      return r;
    }

  private:
    ValueType  SampleSum {0};
    ValueType  LastValue {0};
    ValueType  RoundAdd  {SAMPLES / 4};
  };

  class BspPortableRadio : public Psp
  {
  public:
    class Display : public BufferedDisplayDriver<::ecpp::Peripherals::Display::NHD0420DZW_4Bit>
    {
    public:
      void Update();
    };

    typedef int_least16_t                     EncoderPos;
    typedef SmoothedAnalogValue<4, int16_t>   VolumeLevelType;

    static BspPortableRadio & getInstance() { return instance_; }
    static BspPortableRadio & instance() { return instance_; }

    void Init();
    void HandleUserInput();

    ButtonState   getButtonState();

    Display     display_;

    void   HandleSystemTick();

    const Max98089RegisterSequence (&max98089_init_registers())[9] {return kMax98089InitRegisters; }
  private:
    static BspPortableRadio instance_;

    static const uint8_t kMax98089RegisterSequence_PowerMgmt1[3];
    static const uint8_t kMax98089RegisterSequence_PowerMgmt2[2];
    static const uint8_t kMax98089RegisterSequence_EQ1Enable[2];
    static const uint8_t kMax98089RegisterSequence_EQ1Config[51];

    static const Max98089RegisterSequence kMax98089InitRegisters[9];

    System::Tick   LastButtonChangeTicks;
    System::Tick   LastEncoderChangeTicks;

    ButtonState    RealButtonState;
    ButtonState    ReportedButtonState;

    uint_least8_t   LastEncoderState;
    int_least8_t    LastEncoderDelta;

    EncoderPos      encoder_pos_real_;
    EncoderPos      encoder_pos_handled_;

    VolumeLevelType VolumeLevel;

    friend void ::PIOA_Handler(void);
    friend void ::ADC_Handler(void);
  };

  using Bsp = BspPortableRadio;

}

#endif /* FIRMWARE_APP_SRC_BSP_H_ */
