#include "target_Bsp.hpp"
#include "target_Globals.hpp"
#include "ecpp/Target/Bsp.hpp"
#include "ecpp/Units/Time.hpp"
#include "pio.h"
#include "delay.h"
#include "app.hpp"
#include "compiler.h"
#include "conf_clock.h"
//#include "sysclk.h"
#include "ioport.h"
#include "adc.h"
#include "pio.h"
#include "delay.h"
#include "config.hpp"

#define LCD_PAR_PIO     PIOA

#define LCD_PAR_DB (PIO_PA15 |\
                    PIO_PA14 |\
                    PIO_PA13 |\
                    PIO_PA12)


#define LCD_PAR_PINMASK (PIO_PA15 |\
                         PIO_PA14 |\
                         PIO_PA13 |\
                         PIO_PA12 |\
                         PIO_PA11 |\
                         PIO_PA10 |\
                         PIO_PA9)

#define LCD_PAR_RW      PIO_PA9
#define LCD_PAR_E       PIO_PA10
#define LCD_PAR_RS      PIO_PA11

using namespace ecpp::Units;

namespace ecpp::Target::Bsp
{
  void
  pulseLcdEnable()
  {
    delay_us(1);
    pio_set(LCD_PAR_PIO, LCD_PAR_E);
    delay_us(1);
    pio_clear(LCD_PAR_PIO,   LCD_PAR_E);
    delay_us(1);
  }

  void
  ResetDisplay(DisplayDriver& inst)
  {
    pio_clear(LCD_PAR_PIO, LCD_PAR_PINMASK);
    pio_set_output(LCD_PAR_PIO, LCD_PAR_PINMASK, LOW, DISABLE, DISABLE);

    pio_clear (LCD_PAR_PIO, LCD_PAR_RS);
  }

  void
  writeLcdNibble(DisplayDriver& inst, uint8_t nibble)
  {
    pio_clear(LCD_PAR_PIO, LCD_PAR_RW);

    pio_set  (LCD_PAR_PIO, ( (nibble << 12)) & LCD_PAR_DB);
    pio_clear(LCD_PAR_PIO, (~(nibble << 12)) & LCD_PAR_DB);

    pulseLcdEnable();
  }

  void
  writeLcdByte(DisplayDriver& inst, uint8_t byte)
  {
    uint8_t nibble;

    nibble = (byte & 0xF0) >> 4;

    pio_clear(LCD_PAR_PIO, LCD_PAR_RW);

    pio_set  (LCD_PAR_PIO, ( (nibble << 12)) & LCD_PAR_DB);
    pio_clear(LCD_PAR_PIO, (~(nibble << 12)) & LCD_PAR_DB);

    pulseLcdEnable();

    nibble = (byte & 0x0F) >> 0;

    pio_set  (LCD_PAR_PIO, ( (nibble << 12)) & LCD_PAR_DB);
    pio_clear(LCD_PAR_PIO, (~(nibble << 12)) & LCD_PAR_DB);

    pulseLcdEnable();
  }

  void
  setLcdRS(DisplayDriver& inst)
  {
    pio_set (LCD_PAR_PIO, LCD_PAR_RS);
  }

  void
  clearLcdRS(DisplayDriver& inst)
  {
    pio_clear (LCD_PAR_PIO, LCD_PAR_RS);
  }
}

using namespace ::target;
using namespace ::MusicBox;

BspPortableRadio BspPortableRadio::instance_;

/* Power management */
const uint8_t BspPortableRadio::kMax98089RegisterSequence_PowerMgmt1[3] =
{
  Max98089Defs::REG_OUTPUT_ENABLE,
  0x3F, /* Enable Speaker + Lineout */
  0xF0, /* Enable Bandgap, Regulator, Common Mode & Bias */
};


const uint8_t BspPortableRadio::kMax98089RegisterSequence_PowerMgmt2[2] =
{
  Max98089Defs::REG_DAC_LOW_POWER_MODE2, 0x05 /* clock + DAI2 Setting */
};

  /* Enable EQ1 */
const uint8_t BspPortableRadio::kMax98089RegisterSequence_EQ1Enable[2] =
{
  Max98089Defs::REG_LEVEL_CONTROL, 0x01
};

const uint8_t BspPortableRadio::kMax98089RegisterSequence_EQ1Config[51] =
{ /* Equalizer Settings */
  /* Equalizer params for Visaton FR10-4 in 1.1l case backwards open
  * (no br). These values are only valid with respect of above MAX98089 frequency settings! */
  Max98089Defs::REG_EQ1_BAND1,
  /*            K                K1                K2                C1                C2 */
  EQ_PARAM(0x40C2), EQ_PARAM(0xC001), EQ_PARAM(0x3FBB), EQ_PARAM(0x0068), EQ_PARAM(0x05DA),
  EQ_PARAM(0x345E), EQ_PARAM(0xC002), EQ_PARAM(0x3F4F), EQ_PARAM(0x00D0), EQ_PARAM(0x095D),
  EQ_PARAM(0x1EEE), EQ_PARAM(0xC00D), EQ_PARAM(0x3F62), EQ_PARAM(0x0288), EQ_PARAM(0x08DF),
  EQ_PARAM(0x1666), EQ_PARAM(0xC2EC), EQ_PARAM(0x0DBF), EQ_PARAM(0x131B), EQ_PARAM(0x3E81),
  EQ_PARAM(0x2322), EQ_PARAM(0xFE5F), EQ_PARAM(0x0FA3), EQ_PARAM(0x3FFD), EQ_PARAM(0x3E0F),
};

const Max98089RegisterSequence BspPortableRadio::kMax98089InitRegisters[] =
{
  {BspPortableRadio::kMax98089RegisterSequence_Shutdown},
  {BspPortableRadio::kMax98089RegisterSequence_Voltage},
  {BspPortableRadio::kMax98089RegisterSequence_Clock},
  {BspPortableRadio::kMax98089RegisterSequence_MixerDAC},
  {BspPortableRadio::kMax98089RegisterSequence_MixerOut},
  {BspPortableRadio::kMax98089RegisterSequence_PowerMgmt1},
  {BspPortableRadio::kMax98089RegisterSequence_PowerMgmt2},
  {BspPortableRadio::kMax98089RegisterSequence_EQ1Enable},
  {BspPortableRadio::kMax98089RegisterSequence_EQ1Config},
};

void Bsp::HandleSystemTick()
{
  Psp::HandleSystemTick();

  adc_start(ADC);

  if((System::getTicks() - LastEncoderChangeTicks) >= Milliseconds(1000))
  { /* clear encoder pos */
    if (LastEncoderDelta < 0)
      encoder_pos_real_ = (encoder_pos_real_ + 2) & 0xFFFC;
    else
      encoder_pos_real_ = (encoder_pos_real_ + 1) & 0xFFFC;
  }
}


static constexpr int_least8_t BspEncoderDeltaMissed = -2;

static const int_least8_t BspEncoderStateMap[][4] = {
  {  0, -1,  1, BspEncoderDeltaMissed},
  {  1,  0, BspEncoderDeltaMissed, -1},
  { -1, BspEncoderDeltaMissed,  0,  1},
  { BspEncoderDeltaMissed,  1, -1,  0}
};

void PIOA_Handler(void)
{
  auto & bsp = Bsp::getInstance();

  unsigned int state = 0;
  ButtonState button;
  int_fast8_t delta;
  uint32_t reg;

  /* get & clear PIO controller sirq status */
  pio_get_interrupt_status(PIOA);

  /* read all pio pins at once */
  reg = ~(PIOA->PIO_PDSR);

  state = 0;
  if(reg & PIO_PA22)
    state += 1;

  if(reg & PIO_PA23)
    state += 2;

  if(reg & PIO_PA24)
    button = ButtonState::Pressed;
  else
    button = ButtonState::Idle;

  if(button != bsp.RealButtonState)
  {
    bsp.LastButtonChangeTicks = System::getTicks();
    bsp.RealButtonState = button;
  }

  delta = BspEncoderStateMap[bsp.LastEncoderState][state];
  bsp.LastEncoderState = state;

  if(delta == BspEncoderDeltaMissed)
  {
    delta = bsp.LastEncoderDelta;
    bsp.LastEncoderDelta = 0;
  }
  else
  {
    bsp.LastEncoderDelta = delta;
  }

  if(delta != 0)
    bsp.LastEncoderChangeTicks = System::getTicks();

  bsp.encoder_pos_real_ += (int_least16_t)delta;
}

extern "C" void ADC_Handler(void)
{
  auto & bsp = Bsp::getInstance();
  uint32_t status = adc_get_status(ADC);

  // Check the ADC conversion status
  if (ADC_ISR_EOC8 & status)
  {
    // Get latest digital data value from ADC and can be used by application
    bsp.VolumeLevel.sample(adc_get_channel_value(ADC, ADC_CHANNEL_8));
  }
}


void Bsp::Init(void)
{
  Psp::Init();

  /* set encoder to be an input */
  pio_set_input(PIOA, PIO_PA24 | PIO_PA23 | PIO_PA22,
                PIO_PULLUP | PIO_DEGLITCH);

  pio_set_debounce_filter(PIOA, PIO_PA24 | PIO_PA23 | PIO_PA22, 500);

  /* enable pin change interrupts on all encoder pins */
  pio_configure_interrupt(PIOA, PIO_PA24 | PIO_PA23 | PIO_PA22, 0);

  /* Disable all PIOA I/O line interrupt. */
  pio_disable_interrupt(PIOA, 0xFFFFFFFF);
  pio_enable_interrupt(PIOA, PIO_PA24 | PIO_PA23 | PIO_PA22);

  /* enable ADC for volume knob */
  sysclk_enable_peripheral_clock(ID_ADC);

  adc_init(ADC, sysclk_get_main_hz(), 2000000, ADC_STARTUP_TIME_8);
  adc_set_bias_current(ADC, 0);

  adc_configure_timing(ADC, 15, ADC_SETTLING_TIME_3, 2);
  adc_set_resolution(ADC, ADC_10_BITS);

  adc_enable_channel(ADC, ADC_CHANNEL_8);

  adc_enable_interrupt(ADC, ADC_IER_EOC8);

  adc_configure_trigger(ADC, ADC_TRIG_SW, 0);

  /* Configure and enable interrupt of PIO. */
  NVIC_ClearPendingIRQ(PIOA_IRQn);
  NVIC_SetPriority(PIOA_IRQn, 4);
  NVIC_EnableIRQ(PIOA_IRQn);

  NVIC_SetPriority(ADC_IRQn, 6);
  NVIC_EnableIRQ(ADC_IRQn);

  display_.initDisplay();
}

ButtonState
Bsp::getButtonState()
{
  ButtonState r = RealButtonState;

  if(r != ReportedButtonState)
  {
    if ((System::getTicks() - LastButtonChangeTicks) > Milliseconds(50))
    {
      ReportedButtonState = r;

      if (r == ButtonState::Pressed)
        return ButtonState::Down;
      else
        return ButtonState::Up;
    }
  }

  return ReportedButtonState;
}

void
Bsp::HandleUserInput()
{
  auto & ui = Globals::getInstance().Ui;

  auto buttonState = getButtonState();

  if(buttonState == ButtonState::Down)
  {
    ui.dispatchEvent(Lcd4x20Event(Lcd4x20Event::EventType::Clicked));
    ui.ResetInteractionTime();
  }
  else if(encoder_pos_real_ != encoder_pos_handled_)
  {
    auto change = encoder_pos_real_ - encoder_pos_handled_;

    if(change > 2)
    {
      while(change > 2)
      {
        encoder_pos_handled_ += 4;
        change = encoder_pos_real_ - encoder_pos_handled_;
        ui.dispatchEvent(Lcd4x20Event(Lcd4x20Event::EventType::Down));
      }
    }
    else if (change < -2)
    {
      while(change < -2)
      {
        encoder_pos_handled_ += 4;
        change = encoder_pos_real_ - encoder_pos_handled_;
        ui.dispatchEvent(Lcd4x20Event(Lcd4x20Event::EventType::Up));
      }
    }

    ui.ResetInteractionTime();
    ui.invalidate();
  }

  /* update volume from potentiometer */
  Globals::instance().MaxHandler.setVolumeLevel(VolumeLevel / 32);

}

void
Bsp::Display::Update()
{
  unsigned int r;

  for(r = 0; r < 4; ++r)
  {
    LocateCursor(0, r);
    WriteDDRAM(buffer_[r], 20);
  }
}
