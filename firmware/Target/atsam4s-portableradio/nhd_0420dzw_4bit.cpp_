/*
 * lcd.cpp
 *
 *  Created on: 30.03.2017
 *      Author: andi
 */


#include "app.hpp"
#include "conf_board.h"
#include "delay.h"
#include "pio.h"

#define LCD_PAR_PIO     PIOA

#define LCD_PAR_DB (PIO_PA15 |\
                    PIO_PA14 |\
                    PIO_PA13 |\
                    PIO_PA12)


#define LCD_PAR_PINMASK (PIO_PA15 |\
                         PIO_PA14 |\
                         PIO_PA13 |\
                         PIO_PA12 |\
                         PIO_PA11 |\
                         PIO_PA10 |\
                         PIO_PA9)

#define LCD_PAR_RW      PIO_PA9
#define LCD_PAR_E       PIO_PA10
#define LCD_PAR_RS      PIO_PA11

static void lcd_write_nibble(uint8_t nibble);
static void lcd_command(uint8_t cmd);

void lcd_init()
{

  pio_clear(LCD_PAR_PIO, LCD_PAR_PINMASK);
  pio_set_output(LCD_PAR_PIO, LCD_PAR_PINMASK, LOW, DISABLE, DISABLE);

  pio_clear (LCD_PAR_PIO, LCD_PAR_RS);

  /* switch to 8 bit to get an initial state (we have no reset) */
  //lcd_write_nibble(0x3);
  //lcd_write_nibble(0x3);

  //delay_ms(1);

  /* Now switch to four bit */
  lcd_write_nibble(0x2);
  lcd_write_nibble(0x2);
  lcd_write_nibble(0x8);

  delay_ms(1);

  /* display off */
  lcd_command(0x08);

  /* display clear */
  lcd_command(0x01);

  /* entry mode */
  lcd_command(0x06);

  /* home */
  lcd_command(0x02);

  /* display on */
  lcd_command(0x0C);
}

static void lcd_enable()
{
  delay_us(1);
  pio_set(LCD_PAR_PIO, LCD_PAR_E);
  delay_us(1);
  pio_clear(LCD_PAR_PIO,   LCD_PAR_E);
  delay_us(1);

}
static void lcd_write_nibble(uint8_t nibble)
{
  pio_clear(LCD_PAR_PIO, LCD_PAR_RW);

  pio_set  (LCD_PAR_PIO, ( (nibble << 12)) & LCD_PAR_DB);
  pio_clear(LCD_PAR_PIO, (~(nibble << 12)) & LCD_PAR_DB);

  lcd_enable();
}

static void lcd_write_byte(uint8_t byte)
{
  uint8_t nibble;

  nibble = (byte & 0xF0) >> 4;

  pio_clear(LCD_PAR_PIO, LCD_PAR_RW);

  pio_set  (LCD_PAR_PIO, ( (nibble << 12)) & LCD_PAR_DB);
  pio_clear(LCD_PAR_PIO, (~(nibble << 12)) & LCD_PAR_DB);

  lcd_enable();

  nibble = (byte & 0x0F) >> 0;

  pio_set  (LCD_PAR_PIO, ( (nibble << 12)) & LCD_PAR_DB);
  pio_clear(LCD_PAR_PIO, (~(nibble << 12)) & LCD_PAR_DB);

  lcd_enable();
}

static void
lcd_command(uint8_t cmd)
{
  pio_clear (LCD_PAR_PIO, LCD_PAR_RS);
  lcd_write_byte(cmd);

  delay_ms(3);
}

void lcd_loc(unsigned int col, unsigned int row)
{
  switch(row)
  {
  case 0: lcd_command(0x080 + 0x00 + col); break;
  case 1: lcd_command(0x080 + 0x40 + col); break;
  case 2: lcd_command(0x080 + 0x14 + col); break;
  case 3: lcd_command(0x080 + 0x54 + col); break;
  }
}


static constexpr char convert_null_char(const char c)
{
  return (c == 0) ? ' ' : c;
}

void lcd_write(const char* text)
{
  lcd_write(text, strlen(text));
}

void lcd_write(const char* text, size_t textlen)
{
  pio_set (LCD_PAR_PIO, LCD_PAR_RS);

  for( ;textlen > 0; --textlen)
    lcd_write_byte(convert_null_char(*(text++)));
}
