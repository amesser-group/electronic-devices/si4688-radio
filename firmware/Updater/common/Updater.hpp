/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the MusicBox project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef MUSICBOX_UPDATER_HPP
#define MUSICBOX_UPDATER_HPP

#include <cstdint>

#include "ecpp/Abi/SimpleBinFormat.hpp"

namespace MusicBox
{
  using ::std::uint_least8_t;
  using ::std::uint_least16_t;
  using ::std::uint_least32_t;

  /** Holds all information for updater to load update from filesystem */
  class UpdaterParameter
  {
  public:
    static constexpr unsigned int MaxClusters {128};

    uint_least32_t  RCA                   {0};
    uint_least16_t  NumSectors            {0};
    uint_least8_t   HighCapacity          {0};
    uint_least8_t   SectorsPerCluster     {0};
    uint_least32_t  BaseSector            {0};
    uint_least32_t  Clusters[MaxClusters] {0};

    bool load();
  };

  class UpdaterImg : public ecpp::Abi::SimpleBinaryFormat
  {
  public:
    typedef void (UpdateFuncType)(const UpdaterParameter & prm);

    constexpr UpdaterImg( UpdateFuncType *func) : ecpp::Abi::SimpleBinaryFormat(), main(func) {}

    UpdateFuncType *main;
  };

}
#endif
