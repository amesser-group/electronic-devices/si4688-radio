/*
 * program.hpp
 *
 *  Created on: 08.12.2017
 *      Author: andi
 */

#ifndef PROGRAM_HPP_
#define PROGRAM_HPP_

#include <cstdint>
using namespace std;

template<int size>
class ProgramDb;

class Program
{
private:
  uint_least32_t Frequency;
  uint_least32_t serviceid;
  uint_least16_t componentid;
  char           m_Label[16];

  template<int size>
  friend class ProgramDb;
public:
  constexpr uint_least32_t getFrequency() const {return Frequency;}
  constexpr const char* getLabel() const {return m_Label;}

  constexpr uint_least32_t getServiceId()   const {return serviceid;}
  constexpr uint_least16_t getComponentId() const {return componentid;}
};

template<int size>
class ProgramDb
{
private:
  Program Programs[size];
public:
  const Program & operator [] (unsigned int i)
  {
    return Programs[i];
  }

  void registerDabTransponder(uint_least32_t frequency);
  void registerDabComponent(uint_least32_t frequency, uint_least32_t serviceid, uint_least16_t componentid, const char *label);

  const Program * nextProgram(const Program* current);
};

template<int size>
void ProgramDb<size>::registerDabTransponder(uint_least32_t frequency)
{
  unsigned int i,j;

  j = size;

  for(i = 0; i < size; ++i)
  {
    if (Programs[i].Frequency == frequency)
      return;

    if (0 == Programs[i].Frequency)
      break;
  }

  for(j = i; j < size; ++j)
  {
    if (Programs[j].Frequency == frequency)
      return;
  }

  Programs[i].Frequency = frequency;
}

template<int size>
void ProgramDb<size>::registerDabComponent(uint_least32_t frequency, uint_least32_t serviceid, uint_least16_t componentid, const char *label)
{
  unsigned int i,j;

  j = size;

  for(i = 0; i < size; ++i)
  {
    if ( (Programs[i].Frequency   == frequency) &&
         (Programs[i].serviceid   == serviceid) &&
         (Programs[i].componentid == componentid))
      return;

    if (0 == Programs[i].Frequency)
      break;

    if ( (Programs[i].Frequency   == frequency) &&
         (Programs[i].serviceid   == 0) &&
         (Programs[i].componentid == 0))
      break;
  }

  for(j = i; j < size; ++j)
  {
    if ( (Programs[i].Frequency   == frequency) &&
         (Programs[i].serviceid   == serviceid) &&
         (Programs[i].componentid == componentid))
      return;
  }

  Programs[i].Frequency = frequency;
  Programs[i].serviceid   = serviceid;
  Programs[i].componentid = componentid;
  memcpy(Programs[i].m_Label, label, sizeof(Programs[i].m_Label));
}


template<int size>
const Program * ProgramDb<size>::nextProgram(const Program* current)
{
  const Program *it;

  if(nullptr == current)
    it = &(Programs[0]);
  else
    it = current + 1;

  while(it < &(Programs[size]))
  {
    if(it->Frequency != 0)
      return it;

    it++;
  }

  it = &(Programs[0]);

  while(it < current)
  {
    if(it->Frequency != 0)
      return it;

    it++;
  }

  return nullptr;
}





#endif /* FIRMWARE_APP_SRC_PROGRAM_HPP_ */
