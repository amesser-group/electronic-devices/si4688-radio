#ifndef MUSIBOX_WIDGETS_HPP_
#define MUSIBOX_WIDGETS_HPP_

#include "DrawContext.hpp"

#include "ecpp/Ui/Text/ListWidget.hpp"
#include "ecpp/Ui/Text/ActionItem.hpp"
#include "ecpp/Vector.hpp"

#include <array>
#include <vector>
#include <functional>

namespace MusicBox
{
  using namespace ::ecpp::Ui::Text;
  using ::ecpp::StaticVector;


  typedef ActionItem<Lcd4x20DrawContext> ActionItemType;
  typedef StaticVector<ActionItemType, uint_least8_t> StaticActionListModel;
  typedef ArrayListWidget<StaticActionListModel> ActionListWidget;

  class IdleWidget : public TextWidgetType
  {
  public:
    void draw(Lcd4x20DrawContext &ctx) override;
    void event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event) override;

  protected:
    ActionListWidget                                        QuickActions         {};
    ::std::reference_wrapper<const StaticActionListModel>   QuickActionsModel    {OffQuickActionsModel};

    static const StaticActionListModel DABQuickActionsModel;
    static const StaticActionListModel FMQuickActionsModel;
    static const StaticActionListModel RadioQuickActionsModel;
    static const StaticActionListModel OffQuickActionsModel;
  };

  class ListItem
  {
  public:
    virtual void draw(Lcd4x20DrawContext::Painter &ctx) const = 0;
    virtual void event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event) const;
  };

  class StaticListWidget : public TextWidgetType
  {
  public:
    constexpr StaticListWidget(const char* title, const ListItem* const *items, uint_least8_t len) : TextWidgetType(), title(title), items(items), len(len), pos(0) {}

    template<size_t COUNT>
    constexpr StaticListWidget(const char* title, const ListItem* const (&items)[COUNT]) : TextWidgetType(), title(title), items(items), len(COUNT), pos(0) {}

    template<size_t COUNT>
    constexpr StaticListWidget(const char* title, const ::std::array<const ListItem*, COUNT> & arr) : TextWidgetType(), title(title), items(arr.data()), len(COUNT), pos(0) {}

    void draw(Lcd4x20DrawContext &ctx) override;
    void event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event) override;
  private:
    const char* const      title;
    const ListItem* const* items;
    const uint_least8_t    len;
    uint_least8_t          pos;
  };


  class TitledListItem : public ListItem
  {
  public:
    constexpr TitledListItem(const char* title) : title(title) {};

    void draw(Lcd4x20DrawContext::Painter &ctx) const override
    {
      ctx.putText(this->title);
    }
  protected:
    const char * const title;
  };

  template<typename WIDGETTYPE>
  class SubWidgetItem : public TitledListItem
  {
  public:
    constexpr SubWidgetItem(const char* title) : TitledListItem(title) {};

    void event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event) const override
    {
      if(event.type == Lcd4x20Event::EventType::Clicked)
        ctx.pushWidget<WIDGETTYPE>(this->title);
      else
        TitledListItem::event(ctx, event);
    }
  };

  template<typename WIDGETTYPE = StaticListWidget>
  class SubListItem : public TitledListItem
  {
  public:
    template<size_t COUNT>
    constexpr SubListItem(const char* title, const ListItem* const (&items)[COUNT]) : TitledListItem(title), subItems{items}, numItems{COUNT} {};

    template<size_t COUNT>
    constexpr SubListItem(const char* title, const ::std::array<const ListItem*, COUNT> & arr)   : TitledListItem(title), subItems{arr.data()}, numItems{COUNT} {};

    void event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event) const override
    {
      if(event.type == Lcd4x20Event::EventType::Clicked)
        ctx.pushWidget<WIDGETTYPE>(title, subItems, numItems);
      else
        TitledListItem::event(ctx, event);
    }
  private:
    const ListItem* const* subItems;
    const unsigned int     numItems;
  };

  class ChannelListWidget : public TextWidgetType
  {
  public:
    ChannelListWidget(const char* title) : TextWidgetType(), title(title), selectedService()
    {
      ++selectedService;
    }

    void draw(Lcd4x20DrawContext &ctx) override;
    void event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event) override;

  protected:
    void stepService(Database::ServiceIterator& it, int steps) const;
    void stepService(Database::ServiceValueIterator& it, int steps) const ;

    virtual bool matchService(const Database::ServiceValueIterator& it) const;

  private:
    const char* const title;
    Database::ServiceIterator selectedService;
  };


  class FileSystemWidget : public TextWidgetType
  {
  public:
    FileSystemWidget();
    FileSystemWidget(const char* title) : FileSystemWidget() {}

    ~FileSystemWidget() override;

    void draw(Lcd4x20DrawContext &ctx) override;
    void event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event) override;
  };
};

#endif