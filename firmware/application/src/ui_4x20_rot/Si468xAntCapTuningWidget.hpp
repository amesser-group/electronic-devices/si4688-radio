
/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_SI468XANTCAPTUNINGWIDGET_HPP_
#define MUSICBOX_SI468XANTCAPTUNINGWIDGET_HPP_

#include "DrawContext.hpp"
#include "FileSystem.hpp"

namespace MusicBox
{
  class Si468xManualAntCapTuningWidget : public TextWidgetType
  {
  public:
    Si468xManualAntCapTuningWidget(const char* title) : TextWidgetType{} {};

    void draw(Lcd4x20DrawContext &ctx) override;
    void event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event) override;

  private:
  };

  class Si468xAutoAntCapTuningWidget : public Si468xManualAntCapTuningWidget
  {
  public:
    Si468xAutoAntCapTuningWidget(const char* title);
    virtual ~ Si468xAutoAntCapTuningWidget();

    void draw(Lcd4x20DrawContext &ctx) override;
  };
}

#endif