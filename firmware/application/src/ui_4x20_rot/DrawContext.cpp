#include "DrawContext.hpp"
#include "ecpp/Datatypes.hpp"

#include "target_Globals.hpp"
#include "target_Bsp.hpp"

#include "ui_4x20_rot/Widgets.hpp"
#include "ui_4x20_rot/Si468xFlashWidgets.hpp"


#include <cstring>
#include <cstdint>
#include <new>

using namespace target;
using namespace MusicBox;
using namespace ecpp;
using namespace std;

union WidgetStackItem
{
public:
  /* bug in gcc 7.1, 7.2:  The following fails although properly :
   * constexpr WidgetStackItem() : Default{} {} */
  constexpr WidgetStackItem() : Default() {}
  ~WidgetStackItem() {}

  TextWidgetType                 Default;
  IdleWidget                     Idle;
  StaticListWidget               StaticList;
  ChannelListWidget              ChannelList;
  FileSystemWidget               FileSystem;
  Si468xSelectFirmwareWidget     Si468xSelectFirmware;
  Si468xShowFlashProgressWidget  Si468xFlashProgress;
};

static WidgetStackItem WidgetStackArray[4];

void Lcd4x20DrawContext::start()
{
  if(WidgetStackDepth == -1)
  {
    TextWidgetType* w = pushWidget<IdleWidget>();
    invalidate(w);
  }

  PollTimer.start(Milliseconds(5), PollJob);
}

void Lcd4x20DrawContext::poll()
{
  /* Check if timed redraw schedule reached */
  if (RedrawTimeout > Milliseconds(0))
  {
    if ((System::getInstance().getTicks() - LastDrawTime) > RedrawTimeout)
      invalidate();
  }
}

void Lcd4x20DrawContext::poll(System::Job &job)
{
  Lcd4x20DrawContext & self = container_of(Lcd4x20DrawContext, PollJob, job);

  self.PollTimer.cont(Milliseconds(200), job);

  Bsp::getInstance().HandleUserInput();
  self.poll();
}

TextWidgetType*
Lcd4x20DrawContext::getCurrentWidget()
{
  if(WidgetStackDepth < 0)
    return nullptr;

  if(WidgetStackDepth >= ElementCount(WidgetStackArray))
    return nullptr;

  return &(WidgetStackArray[WidgetStackDepth].Default);
}

void
Lcd4x20DrawContext::popWidget()
{
  releaseWidget(getCurrentWidget());
}

TextWidgetType*
Lcd4x20DrawContext::allocateWidget()
{
  if((WidgetStackDepth + 1) >= ElementCount(WidgetStackArray))
    return nullptr;

  WidgetStackDepth++;

  return &(WidgetStackArray[WidgetStackDepth].Default);
}

void
Lcd4x20DrawContext::releaseWidget(TextWidgetType* w)
{
  if(WidgetStackDepth <= 0)
    return;

  if ( w != getCurrentWidget())
    return;

  /* call destructor */
  w->~TextWidgetType();

  WidgetStackDepth--;
  invalidate();
}

void
Lcd4x20DrawContext::drawScreen(System::Job &job)
{
  Lcd4x20DrawContext & self = container_of(Lcd4x20DrawContext, DrawJob, job);
  TextWidgetType *w;

  self.CreatePainter().fill(' ');
  self.RedrawTimeout = 0;

  w = self.getCurrentWidget();

  if(nullptr != w)
    w->draw(self);

  self.LastDrawTime = System::getTicks();

  Bsp::instance().display_.Update();
}

void
Lcd4x20DrawContext::invalidate(const TextWidgetType* w)
{
  enqueue(DrawJob);
}

void
Lcd4x20DrawContext::dispatchEvent(const Lcd4x20Event &ev)
{
  TextWidgetType *w = getCurrentWidget();

  if(nullptr == w)
    return;

  w->event(*this, ev);
}

void
Lcd4x20DrawContext::handleEventDefault(const Lcd4x20Event &event)
{
  switch(event.type)
  {
  case Lcd4x20Event::EventType::StatusChange:
    invalidate();
    break;
  case Lcd4x20Event::EventType::InteractionTimeout:
    while(WidgetStackDepth > 0 )
      popWidget();
    break;
  default:
    break;
  }
}

void
Lcd4x20DrawContext::handleInteractionTimeout()
{
  dispatchEvent(Lcd4x20Event(Lcd4x20Event::EventType::InteractionTimeout));
}

void
TextWidgetType::event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event)
{
  ctx.handleEventDefault(event);
}

