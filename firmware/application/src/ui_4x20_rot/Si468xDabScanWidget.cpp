#include "ui_4x20_rot/Si468xDabScanWidget.hpp"
#include "target_Globals.hpp"

using namespace MusicBox;

Si468xDabScanWidget::Si468xDabScanWidget(const char *title): TextWidgetType{}
{
  auto a = Globals::getInstance().SiHandler.getAction<Si468xDabChannelScanner>();
  auto p = a.aquire();

  if(nullptr != p)
    p->startScan();
}

Si468xDabScanWidget::~Si468xDabScanWidget()
{
  auto a = Globals::getInstance().SiHandler.getAction<Si468xDabChannelScanner>();
  auto p = a.get();

  if(nullptr != p)
    p->stopScan();

  a.release();
}

void
Si468xDabScanWidget::draw(Lcd4x20DrawContext &ctx)
{
  auto & s = Globals::getInstance().SiHandler;
  auto p = s.getAction<Si468xDabChannelScanner>().get();

  ctx.scheduleRedraw(Milliseconds(100));
  ctx.getRow(0).centerText("DAB Channel Scan");

  if (s.getOpMode() == Si468xOperationMode::DABRadio)
  {
    auto const &sc = Globals::getInstance().SiHandler;
    auto const & r = sc.getDABManager();
    auto rssi = ::ecpp::FixedPoint<int32_t, 1000>(r.getRssi());

    ctx.getRow(1).iprintf("Freq:   %3d.%3d MHz", s.getTunedFreq() / 1000, s.getTunedFreq() % 1000);
    ctx.getRow(2).iprintf("Rssi: %3d.%03d", rssi.get_raw() / 1000, rssi.get_raw() % 1000);
  }

  if(nullptr != p)
  { /* should be never the case */
    if( p->isScanning())
      ctx.getRow(3).centerText("Scanne..");
    else
      ctx.getRow(3).centerText("Fertig");
  }
}

void
Si468xDabScanWidget::event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event)
{
  switch(event.type)
  {
  case Lcd4x20Event::EventType::Up:
    break;
  case Lcd4x20Event::EventType::Down:
    break;
  case Lcd4x20Event::EventType::Clicked:
    ctx.popWidget();
    break;
  case Lcd4x20Event::EventType::InteractionTimeout:
    break; /* do not close this widget in case of application timeout */
  default:
    TextWidgetType::event(ctx, event);
    break;
  }
}
