
#include "Widgets.hpp"
#include "ui_4x20_rot/Si468xFlashWidgets.hpp"
#include "ui_4x20_rot/Si468xAntCapTuningWidget.hpp"
#include "ui_4x20_rot/Si468xDabScanWidget.hpp"
#include "target_Globals.hpp"
#include "ecpp/String.hpp"
#include "ecpp/StringOperations.hpp"
#include "updater/UpdaterRunner.hpp"

#include <string.h>
#include <experimental/array>
#include <array>


using namespace target;
using namespace MusicBox;
using namespace MusicBox::Database;
using namespace ecpp;
using namespace std;

/* forward declarations */
class MainMenuWidget;

void ListItem::event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event) const
{
  ctx.handleEventDefault(event);
}

class BackListItem : public ListItem
{
public:
  void draw(Lcd4x20DrawContext::Painter &ctx) const override
  {
    ctx.putText("Zurück");
  }

  void event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event) const override
  {
    if(event.type == Lcd4x20Event::EventType::Clicked)
      ctx.popWidget();
    else
      ListItem::event(ctx, event);
  }
};

static BackListItem Back;


enum IdleWidgetQuickAction
{
  MainMenu = 0,
  Poweroff = 1,
};

static const ActionItemType IdleWidgetRadioQuickActions[] =
{
  { u8"Hauptmenü", [](Lcd4x20DrawContext &ctx) { ctx.pushWidget<MainMenuWidget>(); }},
  { u8"Abschalten", [](Lcd4x20DrawContext &ctx) { Globals::getInstance().setMode(MusicBoxMode::Off); }},
};

static const ActionItemType IdleWidgetDABQuickActions[] =
{
  { u8"Hauptmenü", [](Lcd4x20DrawContext &ctx) { ctx.pushWidget<MainMenuWidget>(); }},
  { u8"Abschalten", [](Lcd4x20DrawContext &ctx) { Globals::getInstance().setMode(MusicBoxMode::Off); }},
};

static const ActionItemType IdleWidgetFMQuickActions[] =
{
  { "Suche vorwärts",  [](Lcd4x20DrawContext &ctx) { Globals::getInstance().SiHandler.seek(true); }},
  { "Suche rückwärts", [](Lcd4x20DrawContext &ctx) { Globals::getInstance().SiHandler.seek(false); }},
  { "Hauptmenüe",     [](Lcd4x20DrawContext &ctx) { ctx.pushWidget<MainMenuWidget>(); }},
  { "Abschalten", [](Lcd4x20DrawContext &ctx) { Globals::getInstance().setMode(MusicBoxMode::Off); }},
};



static const ActionItemType IdleWidgetOffQuickActions[] =
{
  { "Radio",      [](Lcd4x20DrawContext &ctx) { Globals::getInstance().setMode(MusicBoxMode::Radio); }},
};

const StaticActionListModel IdleWidget::DABQuickActionsModel   {IdleWidgetDABQuickActions};
const StaticActionListModel IdleWidget::FMQuickActionsModel    {IdleWidgetFMQuickActions};
const StaticActionListModel IdleWidget::RadioQuickActionsModel {IdleWidgetRadioQuickActions};
const StaticActionListModel IdleWidget::OffQuickActionsModel   {IdleWidgetOffQuickActions};

void
IdleWidget::draw(Lcd4x20DrawContext &ctx)
{  const auto & mgr = Globals::getInstance().SiHandler;
  // const auto & fm = mgr.getStatusManager();
  const auto &  s = mgr.getCurrentService();
  const auto mode = mgr.getMode();

  if(mode == Si468xOperationMode::FMRadio)
  {
    const auto & rds = mgr.getRDSState();
    const auto freq  = mgr.getRadioManager().getTunedFreq();

    ArrayString<ecpp::StringEncodings::Utf8, 16> rds_name = rds.program_service_name.decode();

    ctx.getRow(0).iprintf(u8"FM %3u.%02u MHz %2d dBµV", static_cast<unsigned>(freq/1000), static_cast<unsigned>(freq%1000/10),
                          static_cast<int>(mgr.getRadioManager().getRssi()));

    if ( s.Name.countCharacters() > 0)
    {
      ctx.getRow(1).centerText(s.Name);

      if (rds.isProgramServiceNameValid())
      {
        if (s.Name != rds_name.trim())
          ctx.getRow(2).centerText(rds_name);
      }
    }
    else if (rds.isProgramServiceNameValid())
    {
      ctx.getRow(1).centerText(rds_name);
    }
    else
    {
      ctx.getRow(1).centerText("<Unbekannt>");
    }

    QuickActionsModel = FMQuickActionsModel;
  }
  else if(mode == Si468xOperationMode::DABRadio)
  {
    const auto freq  = mgr.getRadioManager().getTunedFreq();

    ctx.getRow(0).iprintf("DAB     %2d dBµV", static_cast<int>(mgr.getRadioManager().getRssi()));

    if (s.Name.countCharacters() > 0)
      ctx.getRow(1).centerText(s.Name);
    else
      ctx.getRow(1).centerText("<Unbekannt>");

    ctx.getRow(2).iprintf("%3d.%03d MHz", freq/1000, freq%1000);

    QuickActionsModel = DABQuickActionsModel;
  }
  else
  {

    switch(Globals::getInstance().State.Mode)
    {
    case MusicBoxMode::Off:
      QuickActionsModel = OffQuickActionsModel;
      if (!ctx.isUiIdle())
        ctx.getRow(1).centerText("<Aus>");
      break;
    case MusicBoxMode::Radio:
      QuickActionsModel = RadioQuickActionsModel;
      ctx.getRow(1).centerText("<Kein Kanal>");
      break;
    }
  }

  if ( !ctx.isUiIdle())
  {
    auto r = ctx.getRow(3);
    QuickActions.draw(r, QuickActionsModel);
  }
#if 0
  {
    const auto & cd = Globals::getInstance().CardDriver;

    switch(cd.getCardState().CardState)
    {
    case SDCardState::NotPlugged:                                      break;
    case SDCardState::Initializing:   ctx.getRow(3).putText("SDInit"); break;
    case SDCardState::Ready:          ctx.getRow(3).putText("SDOK");   break;
    case SDCardState::Error:          ctx.getRow(3).putText("SDErr");  break;
    }
  }
#endif
}


void
StaticListWidget::draw(Lcd4x20DrawContext &ctx)
{
  int o,r;
  ctx.getRow(0).centerText(title);

  o = pos - 1;

  if ((o + 2) >= len)
    o = len - 3;

  if (o < 0)
    o = 0;

  for (r = 1; r < 4; ++r)
  {
    auto field = ctx.getRow(r).CreateFieldPainter(1,0);

    if( (o + r - 1) >= len)
      break;

    items[o + r - 1]->draw(field);

    if((o + r - 1) == this->pos)
      ctx.getRow(r)[{0,0}] = '>';
  }
}

void
StaticListWidget::event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event)
{
  switch(event.type)
  {
  case Lcd4x20Event::EventType::Down:
    if ( (this->pos + 1) < (this->len))
    {
      this->pos += 1;
      ctx.invalidate(this);
    }
    break;
  case Lcd4x20Event::EventType::Up:
    if ( this->pos > 0)
    {
      this->pos -= 1;
      ctx.invalidate(this);
    }
    break;
  default:
    items[pos]->event(ctx,event);
    break;
  }
}

void
ChannelListWidget::draw(Lcd4x20DrawContext &ctx)
{
  const auto & si = Globals::getInstance().SiHandler;
  auto & db = Globals::getInstance().Db;

  ServiceValueIterator s = selectedService;
  uint_fast8_t r;

  ctx.getRow(0).centerText(this->title);

  /* step one backward */
  stepService(s, -1);

  for (r=1; r < 4; ++r)
  {
    auto row = ctx.getRow(r);

    if ( s != db.endService() &&
         s != db.rendService())
    {
      if (si.isCurrentService(s))
      {
        row[{0,0}]  = '[';
        row[{19,0}] = ']';
      }
    }

    if (s == selectedService)
      row[{0,0}] = '>';

    auto field = row.CreateFieldPainter(1,0,18);

    if(s == db.rendService())
    {
      Back.draw(field);
    }
    else if (s == db.endService())
    {
      Back.draw(field);
      break;
    }
    else
    {
      field.putText(s->Name);
    }

    stepService(s, 1);
  }
}

void
ChannelListWidget::event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event)
{
  switch(event.type)
  {
    case Lcd4x20Event::EventType::Up:
      stepService(selectedService, -1);
      ctx.invalidate(this);
      break;
    case Lcd4x20Event::EventType::Down:
      stepService(selectedService, +1);
      ctx.invalidate(this);
      break;
    case Lcd4x20Event::EventType::Clicked:
      {
        auto & g = Globals::getInstance();

        if( (selectedService == g.Db.rendService()) ||
            (selectedService == g.Db.endService()))
            Back.event(ctx, event);
        else
          g.SiHandler.tuneChannel(selectedService);
        break;
      }
    case Lcd4x20Event::EventType::DatabaseChanged:
      ctx.invalidate(this);
      break;
    default:
      TextWidgetType::event(ctx, event);
      break;
  }
}

void
ChannelListWidget::stepService(ServiceIterator& it, int steps) const
{
  ServiceValueIterator it_value(it);

  stepService(it_value, steps);
  it = it_value;
}


void
ChannelListWidget::stepService(ServiceValueIterator& it, int steps) const
{
  auto & db = Globals::getInstance().Db;

  while(steps < 0) {
    --it;

    if (it == db.rendService())
      break;
    else if (matchService(it))
      steps++;
  }

  while(steps > 0) {
    ++it;

    if (it == db.endService())
      break;
    else if (matchService(it))
      steps--;
  }
}

bool
ChannelListWidget::matchService(const ServiceValueIterator& it) const
{
  return it->isDabService() ||
         it->isFMService();
}

class PresetListWidget : public ChannelListWidget
{
public:
  PresetListWidget(const char* title) : ChannelListWidget(title) {}
protected:

  bool matchService(const ServiceValueIterator& it) const override
  {
    return it->bPreset;
  }
};

class FMChannelsListWidget : public ChannelListWidget
{
public:
  FMChannelsListWidget(const char* title) : ChannelListWidget(title) {}
protected:
  bool matchService(const ServiceValueIterator& it) const override
  {
    return it->isFMService();
  }
};

class DABChannelsListWidget : public ChannelListWidget
{
public:
  DABChannelsListWidget(const char* title) : ChannelListWidget(title) {}
protected:
  bool matchService(const ServiceValueIterator& it) const override
  {
    return it->isDabService();
  }
};

class FMTuneWidget : public TextWidgetType
{
public:
  FMTuneWidget(const char* title);

  void draw(Lcd4x20DrawContext &ctx) override;
  void event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event) override;
};

FMTuneWidget::FMTuneWidget(const char* title) : TextWidgetType()
{
  auto & mgr = Globals::getInstance().SiHandler;
  mgr.activateMode(Si468xOperationMode::FMRadio);
}

void
FMTuneWidget::draw(Lcd4x20DrawContext &ctx)
{
  auto & g = Globals::getInstance();
  auto const & si  = g.SiHandler;

  {
    char buf[20];
    auto freq = si.getTunedFreq();

    snprintf(buf, sizeof(buf), "%3d.%02d MHz", freq/1000, freq%1000/10);
    ctx.getRow(1).centerText(buf);
  }

  if (TunerStateType::Tuned == si.getTunerState())
  {
    const auto & rds_state = si.getRDSState();

    if(rds_state.isProgramServiceNameValid())
    {
      ArrayString<ecpp::StringEncodings::Utf8, 16> rds_name = rds_state.program_service_name.decode();
      ctx.getRow(2).centerText(rds_name);
    }
    else
    {
      ctx.getRow(2).centerText("<Unbekannt>");
    }
  }
  else
  {
    ctx.getRow(2).centerText("<Kein Signal>");
  }
}

void
FMTuneWidget::event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event)
{
  switch(event.type)
  {
    case Lcd4x20Event::EventType::Up:
      Globals::getInstance().SiHandler.seek(false);
      ctx.invalidate(this);
      break;
    case Lcd4x20Event::EventType::Down:
      Globals::getInstance().SiHandler.seek(true);
      break;
    case Lcd4x20Event::EventType::Clicked:
      break;
    case Lcd4x20Event::EventType::DatabaseChanged:
    case Lcd4x20Event::EventType::StatusChange:
      ctx.invalidate(this);
      break;
    default:
      TextWidgetType::event(ctx, event);
      break;
  }
}


static const struct
{
  SubWidgetItem<FMTuneWidget>           Tune {"FM Tuning"};
  SubWidgetItem<FMChannelsListWidget>   Channels {"FM Kanäle"};
} FMMenuItems;

static const ListItem* const FMMenuItemsPtr[] = {
  &(FMMenuItems.Tune),
  &(FMMenuItems.Channels),
  &(Back),
};

static const struct
{
  SubWidgetItem<DABChannelsListWidget>   Channels {"DAB Kanäle"};
} DABMenuItems;


static const ListItem* const  DABMenuItemsPtr[] = {
  &(DABMenuItems.Channels),
  &(Back),
};


static const struct
{
  SubWidgetItem<Si468xSelectFirmwareWidget>       UpdateRadioFirmware {"Radio FW Update"};
  SubWidgetItem<Si468xManualAntCapTuningWidget>   ManualAntCapTuning  {"Radio AntCap Tuning"};
  SubWidgetItem<Si468xAutoAntCapTuningWidget>     AutoAntCapTuning    {"Radio AntCap Messung"};
  SubWidgetItem<Si468xDabScanWidget>              DabScan             {"DAB Kanalsuche"};
} SettingsMenuItems;


static const ListItem* const  SettingsMenuItemsPtr[] = {
  &(SettingsMenuItems.UpdateRadioFirmware),
  &(SettingsMenuItems.ManualAntCapTuning),
  &(SettingsMenuItems.AutoAntCapTuning),
  &(SettingsMenuItems.DabScan),
  &(Back),
};


class SDCardInfoWidget : public TextWidgetType
{
public:
  SDCardInfoWidget();
  SDCardInfoWidget(const char* title) : SDCardInfoWidget() {}

  ~SDCardInfoWidget();

  void draw(Lcd4x20DrawContext &ctx) override;
  void event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event) override;

private:
  uint_least8_t Selection = 0;
};

SDCardInfoWidget::SDCardInfoWidget() : TextWidgetType()
{
  auto & vmgr = Globals::getInstance().VolumeManager;
  vmgr.mountVolume();
}

SDCardInfoWidget::~SDCardInfoWidget()
{
  auto & vmgr = Globals::getInstance().VolumeManager;
  vmgr.unmountVolume();
}


void
SDCardInfoWidget::draw(Lcd4x20DrawContext &ctx)
{
  auto & cs = Globals::getInstance().CardDriver.getCardState();
  char buf[20];

  ctx.getRow(0).centerTrimmedText("SD Karte");

  snprintf(buf, sizeof(buf), "Größe: %5d MByte", cs.BlockCount / 2 / 1024);
  ctx.getRow(1).putText(buf);

  ctx.getRow(2).CreateFieldPainter(1, 0, 18).putText("Zurück");

  if (Globals::getInstance().VolumeManager.openFile("/updater.bin").getSize() > 0)
    ctx.getRow(3).CreateFieldPainter(1, 0, 18).putText("Fw-Update");
  else
    Selection = 0;

  if(Selection < 2)
  {
    ctx.getRow(2 + Selection)[0]  = '[';
    ctx.getRow(2 + Selection)[19] = ']';
  }

}

void
SDCardInfoWidget::event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event)
{
  switch(event.type)
  {
  case Lcd4x20Event::EventType::Up:
    if (Selection > 0)
      Selection--;
    break;
  case Lcd4x20Event::EventType::Down:
    if (Selection < 1)
      Selection++;
    break;
  case Lcd4x20Event::EventType::Clicked:
    if (Selection == 0)
      ctx.popWidget();
    else if (Selection == 1)
      UpdaterRunner().run();
    break;
  default:
    TextWidgetType::event(ctx, event);
    break;
  }
}

FileSystemWidget::FileSystemWidget() : TextWidgetType()
{
  auto & vmgr = Globals::getInstance().VolumeManager;
  vmgr.mountVolume();
}

FileSystemWidget::~FileSystemWidget()
{
  auto & vmgr = Globals::getInstance().VolumeManager;
  vmgr.unmountVolume();
}

void
FileSystemWidget::draw(Lcd4x20DrawContext &ctx)
{
  // auto & cs = Globals::getInstance().CardDriver.getCardState();
  auto & vmgr = Globals::getInstance().VolumeManager;

  ctx.getRow(0).centerTrimmedText("Dateien");

  {
    auto dir = vmgr.openDir("/");
    decltype(dir)::DirEntType ent;
    int r;

    for(r=1; (r < 4) && dir.read(ent); ++r)
    {
      ctx.getRow(r).putText(ent.fname);
    }
  }
}

void
FileSystemWidget::event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event)
{
  switch(event.type)
  {
  case Lcd4x20Event::EventType::Clicked:
    ctx.popWidget();
  default:
    TextWidgetType::event(ctx, event);
    break;
  }
}



static const struct
{
  SubWidgetItem<PresetListWidget>   Presets     {"Favoriten"};
  SubWidgetItem<ChannelListWidget>  Channels    {"Kanäle"};
  SubListItem<StaticListWidget>     FMRadio     {"FM Radio",  FMMenuItemsPtr};
  SubListItem<StaticListWidget>     DABRadio    {"DAB Radio", DABMenuItemsPtr};
  SubWidgetItem<SDCardInfoWidget>   SDCard      {"SD Karte"};
  SubWidgetItem<FileSystemWidget>   FileSystem  {"Dateien"};
  SubListItem<StaticListWidget>     Settings    {"Einstellungen",  SettingsMenuItemsPtr};
} MainMenuItems;

static const ListItem* const MainMenuItemsPtr[] = {
  &(MainMenuItems.Presets),
  &(MainMenuItems.Channels),
  &(MainMenuItems.FMRadio),
  &(MainMenuItems.DABRadio),
  &(MainMenuItems.SDCard),
  &(MainMenuItems.FileSystem),
  &(MainMenuItems.Settings),
};

class MainMenuWidget : public TextWidgetType
{
public:
  constexpr MainMenuWidget() : TextWidgetType(), pos(0) {}

  void draw(Lcd4x20DrawContext &ctx) override;
  void event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event) override;
private:
  uint_least8_t          incPos(uint_least8_t pos) const;
  uint_least8_t          decPos(uint_least8_t pos) const;
  bool                   isHidden(uint_least8_t pos) const;
  uint_least8_t          pos;
};

uint_least8_t
MainMenuWidget::incPos(uint_least8_t pos) const
{
  do {
    if ( (pos+1) >= ElementCount(MainMenuItemsPtr))
      break;

    pos += 1;

  } while(isHidden(pos));

  return pos;
}

uint_least8_t
MainMenuWidget::decPos(uint_least8_t pos) const
{
  do {
    if ( pos == 0 )
      break;

    pos -= 1;

  } while(isHidden(pos));

  return pos;
}

bool
MainMenuWidget::isHidden(uint_least8_t pos) const
{
  switch(pos)
  {
  case 4:
  case 5:
    return (Globals::getInstance().CardDriver.getCardState().CardState != SDCardState::Ready);
  default:
    return false;
  }
}


void
MainMenuWidget::draw(Lcd4x20DrawContext &ctx)
{
  int o,r;
  ctx.getRow(0).centerTrimmedText("Hauptmenü");

  o = decPos(pos);

  if(incPos(pos) + 1 >= ElementCount(MainMenuItemsPtr))
    o = decPos(o);

  for (r = 1; r < 4; ++r) {
    auto field = ctx.getRow(r).CreateFieldPainter(1, 0);
    unsigned next;


    MainMenuItemsPtr[o]->draw(field);

    if(o == this->pos)
      ctx.getRow(r)[0] = '>';

    next = incPos(o);
    if(o == next)
      break;

    o = next;
  }
}

void
MainMenuWidget::event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event)
{
  switch(event.type)
  {
  case Lcd4x20Event::EventType::Down:
    this->pos = incPos(this->pos);
    ctx.invalidate(this);
    break;
  case Lcd4x20Event::EventType::Up:
    this->pos = decPos(this->pos);
    ctx.invalidate(this);
    break;
  default:
    MainMenuItemsPtr[this->pos]->event(ctx,event);
    break;
  }
}


void
IdleWidget::event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event)
{
  switch(event.type)
  {
  case Lcd4x20Event::EventType::Clicked:
    if(ctx.isUiIdle())
    {
      if(MusicBoxMode::Off != Globals::getInstance().State.Mode )
        RadioQuickActionsModel[IdleWidgetQuickAction::MainMenu].doAction(ctx);
    }
    else
      QuickActionsModel.get()[QuickActions.getSelection()].doAction(ctx);
    break;
  case Lcd4x20Event::EventType::Up:
    if(ctx.isUiIdle())
      QuickActions.setSelection(QuickActionsModel.get().size() - 1, QuickActionsModel.get());
    else
      QuickActions.prevSelection(QuickActionsModel.get());
    break;
  case Lcd4x20Event::EventType::Down:
    if(ctx.isUiIdle())
      QuickActions.setSelection(0, QuickActionsModel.get());
    else
      QuickActions.nextSelection(QuickActionsModel.get());
    break;
  case Lcd4x20Event::EventType::DatabaseChanged:
  case Lcd4x20Event::EventType::ModeChange:
  case Lcd4x20Event::EventType::StatusChange:
    ctx.invalidate(this);
    break;
  case Lcd4x20Event::EventType::InteractionTimeout:
    ctx.invalidate(this);
    break;
  }

}