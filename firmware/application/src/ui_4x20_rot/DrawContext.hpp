
#ifndef MUSICBOX_DRAWCONTEXT_H_
#define MUSICBOX_DRAWCONTEXT_H_

#include "ecpp/Ui/Widget/Text/DrawContext.hpp"
#include "ecpp/Ui/Widget/Text/TextWidget.hpp"
#include "ecpp/Ui/Text/Painter.hpp"
#include "ecpp/Datatypes.hpp"
#include "target_System.hpp"
#include <cstring>
#include <helpers.hpp>
#include "target_Bsp.hpp"
#include "database.hpp"
#include "Si4688/Driver.hpp"

namespace MusicBox
{
  using namespace ::target;

  using ::ecpp::Units::Milliseconds;
  using ::ecpp::Ui::Text::TextPainter;

  class Lcd4x20DrawContext;

  class Lcd4x20Event
  {
  public:
    enum class EventType : uint8_t {
      Clicked,
      Up,
      Down,
      StatusChange,
      DatabaseChanged,
      ModeChange,
      InteractionTimeout,
    };

    constexpr Lcd4x20Event(const Database::ServiceIterator &it) : type{EventType::DatabaseChanged}, data{ .service = it}   {};
    constexpr Lcd4x20Event(Si468xOperationMode mode)            : type{EventType::ModeChange},      data{ .opmode  = mode } {};
    constexpr Lcd4x20Event(EventType ev) : type{ev}, data{} {};

    const EventType type;

    const union {
      Database::ServiceIterator      service;
      Si468xOperationMode            opmode;
    } data;
  };

  class TextWidgetType : public ecpp::TextWidget<Lcd4x20DrawContext, Lcd4x20Event>
  {
  public:
      constexpr TextWidgetType() {}
      virtual ~TextWidgetType() {}

      void draw(Lcd4x20DrawContext &ctx) override {}

      /** handle an event in the gui */
      void event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event) override;
  };

  class Lcd4x20DrawContext : public ecpp::DrawContext, protected JobAble
  {
  public:
    typedef decltype(Bsp::getInstance().display_)                 Display;
    typedef decltype(Bsp::getInstance().display_.CreatePainter()) Painter;
    typedef uint_least8_t IndexType;

    /* typedef ::ecpp::FieldContext<IndexType> FieldContext; */

    constexpr Lcd4x20DrawContext() : WidgetStackDepth(-1) {}

    Painter CreatePainter()
    {
      return Bsp::getInstance().display_.CreatePainter();
    }

    Painter getRow(uint8_t row)
    {
      return Painter(CreatePainter(), {0, row});
    }

    template<typename WIDGETTYPE, typename... ARGS>
    TextWidgetType* pushWidget(ARGS & ... args)
    {
      TextWidgetType* w = allocateWidget();
      WIDGETTYPE* p;

      if(nullptr == w)
        return nullptr;

      p = reinterpret_cast<WIDGETTYPE*>(w);
      new ( p ) WIDGETTYPE(args...);

      invalidate();
      return p;
    }

    void popWidget();


    template<typename WIDGETTYPE, typename... ARGS>
    TextWidgetType* replaceWidget(ARGS & ... args)
    {
      popWidget();
      return pushWidget<WIDGETTYPE, ARGS...>(args...);
    }


    void invalidate(const TextWidgetType* w);
    void invalidate() { invalidate(getCurrentWidget()); }

    void start();
    void poll();

    void handleChange(const Database::ServiceIterator &it) {dispatchEvent(Lcd4x20Event(it));}
    void handleChange(Si468xOperationMode mode)            {dispatchEvent(Lcd4x20Event(mode));}

    void handleStatusChange() { dispatchEvent(Lcd4x20Event(Lcd4x20Event::EventType::StatusChange));}
    void ResetInteractionTime() { InteractionTimeoutTimer.start(Milliseconds(10000), InteractionTimeoutJob);}

    void handleEventDefault(const Lcd4x20Event &ev);

    constexpr System::Tick getLastDrawTime() const { return LastDrawTime; }

    template<typename T>
    void scheduleRedraw(const T &timeout) { RedrawTimeout = timeout; }

    bool isUiIdle() const { return !InteractionTimeoutTimer.isRunning(); }
    void dispatchEvent(const Lcd4x20Event &ev);

  private:
    TextWidgetType* allocateWidget();
    void            releaseWidget(TextWidgetType* w);
    TextWidgetType* getCurrentWidget();

    void handleInteractionTimeout();

    static void drawScreen(System::Job& job);
    static void poll(System::Job& job);


    int_least8_t            WidgetStackDepth {-1};

    System::Job             InteractionTimeoutJob { [](System::Job &job) { container_of(Lcd4x20DrawContext, InteractionTimeoutJob, job).handleInteractionTimeout();} };
    System::Timer           InteractionTimeoutTimer   {};

    System::Tick                  LastDrawTime        {};
    Milliseconds<uint_least16_t>  RedrawTimeout       {0};

    System::Job             DrawJob {drawScreen};
    System::Job             PollJob {poll};

    System::Timer           PollTimer {};
  };

};

#endif
