/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ui_4x20_rot/Si468xAntCapTuningWidget.hpp"
#include "Si4688/AntCapMeasurer.hpp"
#include "target_Globals.hpp"

using namespace target;
using namespace MusicBox;

void
Si468xManualAntCapTuningWidget::draw(Lcd4x20DrawContext &ctx)
{
  auto const & s = Globals::getInstance().SiHandler;

  ctx.scheduleRedraw(Milliseconds(100));

  ctx.getRow(0).centerText("AntCap Optimierung");

  if ( (s.getOpMode() == Si468xOperationMode::DABRadio) ||
       (s.getOpMode() == Si468xOperationMode::FMRadio) )
  {
    auto const & r = s.getRadioManager();
    auto rssi = ::ecpp::FixedPoint<int32_t, 1000>(r.getRssi());

    ctx.getRow(1).iprintf("Freq:   %3d.%3d MHz", s.getTunedFreq() / 1000, s.getTunedFreq() % 1000);
    ctx.getRow(2).iprintf("AC: %3d Rssi: %3d.%03d", s.getAntCapValue(), rssi.get_raw() / 1000, rssi.get_raw() % 1000);
  }
}

void
Si468xManualAntCapTuningWidget::event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event)
{
  auto & s = Globals::getInstance().SiHandler;

  switch(event.type)
  {
  case Lcd4x20Event::EventType::Up:
    if ( (s.getOpMode() == Si468xOperationMode::DABRadio) ||
          (s.getOpMode() == Si468xOperationMode::FMRadio) )
      s.setAntCap(s.getAntCapValue() - 1);
    break;
  case Lcd4x20Event::EventType::Down:
    if ( (s.getOpMode() == Si468xOperationMode::DABRadio) ||
          (s.getOpMode() == Si468xOperationMode::FMRadio) )
      s.setAntCap(s.getAntCapValue() + 1);
    break;
  case Lcd4x20Event::EventType::Clicked:
    ctx.popWidget();
    break;
  case Lcd4x20Event::EventType::InteractionTimeout:
    break; /* do not close this widget in case of application timeout */
  default:
    TextWidgetType::event(ctx, event);
    break;
  }
}

Si468xAutoAntCapTuningWidget::Si468xAutoAntCapTuningWidget(const char* title) :
  Si468xManualAntCapTuningWidget(title)
{
  auto a = Globals::getInstance().SiHandler.getAction<Si468xAntCapMeasurer>();
  auto p = a.aquire();

  if(nullptr != p)
    p->startMeasurement();
}

Si468xAutoAntCapTuningWidget::~Si468xAutoAntCapTuningWidget()
{
  auto a = Globals::getInstance().SiHandler.getAction<Si468xAntCapMeasurer>();
  auto p = a.get();

  if(nullptr != p)
    p->stopMeasurement();

  a.release();
}

void
Si468xAutoAntCapTuningWidget::draw(Lcd4x20DrawContext &ctx)
{
  auto a = Globals::getInstance().SiHandler.getAction<Si468xAntCapMeasurer>();
  auto p = a.get();

  Si468xManualAntCapTuningWidget::draw(ctx);

  if(nullptr != p)
  { /* should be never the case */
    if( p->isMeasuring())
      ctx.getRow(3).centerText("Erstelle Messreihe..");
    else
      ctx.getRow(3).centerText("Fertig");
  }
}
