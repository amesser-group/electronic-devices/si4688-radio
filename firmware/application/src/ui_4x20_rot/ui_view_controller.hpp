/*
 * ui.hpp
 *
 *  Created on: 14.09.2017
 *      Author: andi
 */

#ifndef SRC_UI_4X20_ROT_CPP_UI_HPP_
#define SRC_UI_4X20_ROT_CPP_UI_HPP_

#include "ui_model.hpp"

class MenuState;

class Lcd4x20Rot
{
private:
  enum class StateType : uint_least8_t
  {
    STATE_INIT       = 0,
    STATE_IDLE       = 1,
    STATE_USER       = 2,
  };

  enum class OpModeType : uint_least8_t
  {
    MODE_INIT      = 0,
    MODE_PLAY_FM   = 1,
    MODE_PLAY_DAB  = 2,
  };

  Job            m_Job;
  Job            m_InputJob;
  Timer          m_PeriodicTimer;
  StateType      m_State  = StateType::STATE_INIT;
  OpModeType     m_OpMode = OpModeType::MODE_INIT;
  uint32_t       m_StateChangedTime;

  ActionFn       m_handleAction;
  BuildFn        m_buildScreen;
  ScrollFn       m_handleScroll;

  int            display_pos;
  int            selected_pos;
  int            m_EntryCount;
  unsigned int   m_SelectedEntryHandle;

  int8_t         m_DisplayBuffer[4][20];


  void formatSummary(char *buf, uint_fast8_t BufferLen);

  void pollStateChange();

  static void handlePeriodic_job(Job& job);
  static void refresh_job(Job& job);

  void clearScreen();
  void writeLine(int line, const char* text);
  void writeLineCentered(int line, const char* text);

  void flush();

  static const StaticMenuItem s_EndMarker;
public:

  void init();
  void refresh();

  void handleSi4688OpModeChange(Si4688DeviceManager::OpModeType mode);

  void scrollSaturated(int delta);
  constexpr unsigned int getSelectedEntryHandle() const { return m_SelectedEntryHandle;};

  void buildIdleScreen();
  void scrollIdleMenu(int delta);

  void show(const SubMenuItem &menu);

  friend class MenuState;
  friend class WidgetState;
  friend class WidgetEnter;
};


class WidgetEnter
{
public:
  WidgetEnter(Lcd4x20Rot &ui, BuildFn build, ScrollFn scroll, ActionFn action)
  {
    ui.m_buildScreen  = build;
    ui.m_handleScroll = scroll;
    ui.m_handleAction = action;

    ui.refresh();
  }
};


class WidgetState
{
public:
  static constexpr int display_size = 3;

protected:
  Lcd4x20Rot &m_ui;

public:
  template<class WIDGET>
  WidgetState(Lcd4x20Rot &ui, const WIDGET &title) : m_ui(ui)
  {
    ui.clearScreen();
    ui.writeLineCentered(0, title.getTitle(-1));
  }

  ~WidgetState()
  {
    m_ui.flush();
  }

  void putString(int pos, const char* s)
  {
    if(pos < 1)
      return;

    if(pos > display_size)
      return;

    m_ui.writeLine(pos, s);
  }

  void putStringCentered(int pos, const char* s)
  {
    if(pos < 1)
      return;

    if(pos > display_size)
      return;

    m_ui.writeLineCentered(pos, s);
  }
};


class MenuState : public WidgetState
{
public:
  int display_pos;
  int selected_pos;
  int put_pos;

  char m_LineBuffer[20];

  template<class I>
  MenuState(Lcd4x20Rot &ui, const I &title) : WidgetState(ui, title)
  {
    ui.show(title);

    display_pos  = ui.display_pos;
    selected_pos = ui.selected_pos;
    put_pos = 0;

  }

  ~MenuState()
  {
    m_ui.m_EntryCount = put_pos;

   if(put_pos > display_size)
      putItem(m_ui.s_EndMarker);

    memset(m_LineBuffer, ' ', sizeof(m_LineBuffer));

    while((put_pos - display_pos +1) <= display_size)
    {
      m_ui.writeLine((put_pos - display_pos +1 ), m_LineBuffer);
      put_pos++;
    }
  }

  template<class I>
  void putItem(const I &item)
  {
    if((put_pos >= display_pos) &&
       (put_pos < (display_pos + display_size)))
    {
      m_LineBuffer[0] = ' ';

      strncpy(&(m_LineBuffer[1]), item.getTitle(put_pos), 19);

      if(put_pos == selected_pos)
      {
        m_LineBuffer[0] = '>';

        m_ui.m_handleAction        = item.getAction(put_pos);
        m_ui.m_SelectedEntryHandle = item.getHandle();
      }

      m_ui.writeLine(put_pos - display_pos + 1, m_LineBuffer);
    }

    put_pos++;
  }
};



#endif /* SRC_UI_4X20_ROT_CPP_UI_HPP_ */
