/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ui_4x20_rot/Si468xFlashWidgets.hpp"
#include "target_Globals.hpp"

using namespace target;
using namespace MusicBox;

Si468xSelectFirmwareWidget::Si468xSelectFirmwareWidget(const char*)
  : TextWidgetType {}, Dir {}
{
  auto & v = Globals::getInstance().VolumeManager;

  v.mountVolume();

  Dir = ::std::move(ElmChanDirectory("/"));
  v.unmountVolume();

  findNextFirmwareFile();
}

void
Si468xSelectFirmwareWidget::draw(Lcd4x20DrawContext &ctx)
{
  ctx.getRow(0).centerText("Radio Update");

  if (File.getSize() > 0)
  {
    ctx.getRow(1).iprintf("Datei: %s", DirEnt.fname);
    ctx.getRow(2).centerText("Einspielen");
  }
  else
  {
    ctx.getRow(2).centerText("Zurück");
  }

}

void
Si468xSelectFirmwareWidget::event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event)
{
  switch(event.type)
  {
  case Lcd4x20Event::EventType::Down:
    findNextFirmwareFile();
    break;
  case Lcd4x20Event::EventType::Clicked:
    if (File.getSize() > 0)
    {
      auto & s = Globals::getInstance().SiHandler;

      if ( 0 == strncasecmp(DirEnt.fname, "dab", 3))
      {
        s.updateFirmware(Si468xOperationMode::DABRadio, File);
        ctx.replaceWidget<Si468xShowFlashProgressWidget>();
      }
      else if ( 0 == strncasecmp(DirEnt.fname, "fmhd", 4))
      {
        s.updateFirmware(Si468xOperationMode::FMRadio, File);
        ctx.replaceWidget<Si468xShowFlashProgressWidget>();
      }
    }
    else
    {
      ctx.popWidget();
    }
    break;
  default:
    TextWidgetType::event(ctx, event);
    break;
  }
}

void
Si468xSelectFirmwareWidget::findNextFirmwareFile()
{
  bool found;

  File.close();

  while( (found = Dir.read(DirEnt) ))
  {
    auto len = strnlen(DirEnt.fname, sizeof(DirEnt.fname));
    char buf[1 + 12 + 1];

    if ( (0 != strncasecmp(DirEnt.fname + len - 4, ".bin", 4)) &&
          (0 != strncasecmp(DirEnt.fname + len - 4, ".bif", 4)) )
      continue;

    if ( (0 != strncasecmp(DirEnt.fname, "dab", 3)) &&
          (0 != strncasecmp(DirEnt.fname, "fmhd", 4)) )
      continue;

    /* build pathname */
    strncpy(buf + 1, DirEnt.fname, 12);
    buf[0] = '/'; buf[13] = '\0';

    ElmChanFile f(buf);

    if(f.getSize() <= 0)
      continue;

    /* found firmware file canditate */
    File = ::std::move(f);
    break;
  }

  if(!found)
    Dir.rewind();
}

void
Si468xShowFlashProgressWidget::draw(Lcd4x20DrawContext &ctx)
{
  auto & s = Globals::getInstance().SiHandler;

  ctx.getRow(0).centerText("Schreibe FW");

  if (Si468xOperationMode::UpdateFlash == s.getMode())
  {
    auto p = s.getUpdateFlashManager().getProgress();

    if(static_cast<bool>(p))
    {
      ctx.getRow(2).centerText("Zurück");
      ctx.getRow(3).iprintf(" %6lu Byte", p.Denominator);
    }
    else
    {
      ctx.getRow(2).putProgress(p);
      ctx.getRow(3).iprintf(" %6lu/%lu", p.Nominator, p.Denominator);

      /* keep the widget until finished */
      ctx.ResetInteractionTime();
    }
  }
  else
  {
    ctx.getRow(2).centerText("Zurück");
  }
}

void
Si468xShowFlashProgressWidget::event(Lcd4x20DrawContext &ctx, const Lcd4x20Event &event)
{
  switch(event.type)
  {
  case Lcd4x20Event::EventType::Clicked:
    {
      auto & s = Globals::getInstance().SiHandler;

      if ( (Si468xOperationMode::UpdateFlash != s.getMode()) ||
           static_cast<bool>(s.getUpdateFlashManager().getProgress()))
        ctx.popWidget();
    }
    break;
  default:
    TextWidgetType::event(ctx, event);
    break;
  }
}