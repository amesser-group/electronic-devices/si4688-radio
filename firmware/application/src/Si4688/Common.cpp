/*
 *  Copyright 2016-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Si4688/Common.hpp"

#include "target_Globals.hpp"
#include "target_Bsp.hpp"

using namespace target;
using namespace MusicBox;

bool
Si4688Transaction::sendCommand(const void* pBuf, size_t BufferLen)
{
  auto & twi = Bsp::instance().GetTwiHandler(Si4688Driver::getDriver(*this));

  const uint8_t* pBytes = static_cast<const uint8_t*>(pBuf);

  if (BufferLen < 1)
    return false;

  if (m_State != StateType::IDLE)
    return false;

  m_State = StateType::W_TWI_WCNF;
  m_LastCommand = pBytes[0];

  twi.set_twi_addr(Bsp::instance().kI2CAddr_Si4688);
  twi.write_req(pBytes, pBytes + 1, BufferLen - 1);

  return true;
}

bool
Si4688Transaction::readResponse(void* pBuf, size_t BufferLen)
{

  if (m_State != StateType::IDLE)
    return false;

  return sendReadCommand(pBuf, BufferLen);
}

bool
Si4688Transaction::sendReadCommand(void* pBuf, size_t BufferLen)
{
  auto & twi = Bsp::instance().GetTwiHandler(Si4688Driver::getDriver(*this));

  uint8_t* pBytes = static_cast<uint8_t*>(pBuf);

  if (BufferLen < 4)
    return false;

  m_State   = StateType::W_TWI_RCNF;

  pBytes[0] = CMD_RD_REPLY;
  twi.set_twi_addr(Bsp::instance().kI2CAddr_Si4688);
  twi.read_req(pBytes, pBytes, BufferLen);

  return true;
}

bool Si4688Transaction::setProperty(uint_fast16_t Property, uint_fast16_t Value)
{
  if (m_State != StateType::IDLE)
    return false;

  setField16(0, CMD_SET_PROPERTY);
  setField16(2, Property);
  setField16(4, Value);

  return sendCommand(6);
}


void
Si4688Transaction::handleTwiFinished(bool success)
{
  auto & mgr = Si4688Driver::getDriver(*this);
  auto & twi = Bsp::instance().GetTwiHandler(mgr);


  if( (StateType::W_TWI_WCNF != m_State) &&
      (StateType::W_TWI_RCNF != m_State) )
    return;

  if(!success)
  {
    m_LastStatus = 0xFFFFFFFF;
    m_State = StateType::IDLE;

    mgr.getMgr().handleCommandFinished(m_LastCommand, m_LastStatus);
  }
  else if (StateType::W_TWI_WCNF == m_State)
  {
    /* write is now done, start polling the status */
    sendReadCommand(m_Buffer, 5);
    LastCommandSentTicks = System::getInstance().getTicks();
  }
  else
  {
    m_ReadLen = twi.getReadLen();

    if(m_ReadLen >= 4)
      m_LastStatus.update(twi.getTwiBuffer<uint8_t>());

    if((getField8(0) & MSK_STATUS0_CTS))
    {
      m_State = StateType::IDLE;
      mgr.getMgr().handleCommandFinished(m_LastCommand, m_LastStatus);
    }
    else if((System::getInstance().getTicks() - LastCommandSentTicks) > Milliseconds(1000))
    { /* looks like chip lookup */
      m_LastStatus = 0xFFFFFFFF;
      m_State = StateType::IDLE;

      mgr.getMgr().handleCommandFinished(m_LastCommand, m_LastStatus);
    }
    else
    { /* not yet ready, continue polling */
      sendReadCommand(m_Buffer, 5);
    }
  }
}

void
Si4688RadioManager::changeTunerState(TunerStateType state)
{
  if( (TunerStateType::Failed == state) &&
      (state == TunerState))
    return;

  TunerState = state;
  Globals::statusChanged();
}


void
Si4688RadioManager::tune(bool force)
{
  if (force)
    RequestedAction = Si468xAction::Retune;
  else
    RequestedAction = Si468xAction::Tune;

  Si4688Driver::getDriver(*this).pollAsync();
}

void
Si4688RadioManager::seekForward()
{
  RequestedAction = Si468xAction::SeekForward;
  Si4688Driver::getDriver(*this).pollAsync();
}

void
Si4688RadioManager::seekBackward()
{
  RequestedAction = Si468xAction::SeekBackWard;
  Si4688Driver::getDriver(*this).pollAsync();
}
