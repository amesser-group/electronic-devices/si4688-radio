/*
 *  Copyright 2016-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Si4688/Driver.hpp"
#include "target_Globals.hpp"

using namespace target;
using namespace MusicBox;
using namespace MusicBox::Database;

const uint16_t Si4688DABManager::s_BootupProperties[] =
{
  /* For DAB VHFSW should be closed since we're using the "Appendix C
   * Applicatin circuit for EMI mitigation. See AN649, AN650. Since
   * this is the default, we can leave it as is
   * PROP_DAB_TUNE_FE_CFG,                          0x0001, */
  PROP_TUNE_FE_VARM,        static_cast<uint16_t>(-1541),
  PROP_TUNE_FE_VARB,        static_cast<uint16_t>(  394),
  PROP_DAB_VALID_RSSI_TIME,                          63,
  PROP_DAB_VALID_SYNC_TIME,                           0,
  PROP_DAB_VALID_DETECT_TIME,                         0,
  PROP_DAB_ACF_ENABLE,                           0x0000,
  PROP_DAB_CTRL_DAB_MUTE_SIGNAL_LEVEL_THRESHOLD,      0,
  PROB_DAB_ANOUNCEMENTS,                         0x0000,
  0xFFFF
};

void Si4688DABManager::handleDABBoot()
{
  auto & cmd = Si4688Driver::getDriver(*this).tSiCmd;

  if(s_BootupProperties[m_Offset] != 0xFFFF)
  {
    m_Offset += 2;
    cmd.setProperty(s_BootupProperties[m_Offset-2], s_BootupProperties[m_Offset-1]);
  }
  else
  {
    m_State = StateType::STATE_W_SET_CHANNELS;
    setChannelsList();
  }
}

/** Configure out channel list into the Si468x
 */
void
Si4688DABManager::setChannelsList()
{
  auto const & channels = Globals::getInstance().Db.getDabChannelList();
  auto & cmd = Si4688Driver::getDriver(*this).tSiCmd;
  int_fast8_t i;

  cmd.setField8(0, CMD_DAB_SET_FREQ_LIST);
  cmd.setField16(2, 0);

  for(i = 0; i < 48; ++i)
  {
    if (channels[i].Freq == 0)
      break;

    cmd.setField32(i*4 + 4, channels[i].Freq);
  }

  cmd.setField8(1,  i);
  cmd.sendCommand(i*4 + 4);
}

void Si4688DABManager::handleAction()
{
  auto & drv = Si4688Driver::getDriver(*this);
  auto & cmd = drv.tSiCmd;

  if(m_State < StateType::STATE_READY)
    return;

  if(!cmd.isReady())
    return;

  if( (RequestedAction == Si468xAction::Retune) ||
      (RequestedAction == Si468xAction::Tune))
  {
    if( (m_State != StateType::STATE_TUNING) &&
        (drv.m_ServiceIterator->DabChannel.isValid()) )
    {
      if ( (drv.m_ServiceIterator->DabChannel != m_DabChannel) ||
           (RequestedAction                   == Si468xAction::Retune))
      {
        auto & db   = Globals::getInstance().Db;

        m_State      = StateType::STATE_TUNING;
        m_DabChannel = drv.m_ServiceIterator->DabChannel;

        AverageRssi.init(-128);
        TunedFrequency = db.getDabChannelList().getDabChannelValue(m_DabChannel).Freq;
        ServiceCount   = -1;

        cmd.setField16(0, CMD_DAB_TUNE_FREQ);
        cmd.setField16(2, db.getDabChannelList().indexOf(m_DabChannel));
        cmd.setField16(4, drv.AntCapVal);
        cmd.sendCommand(6);
      }
      else
      {
        if(m_State > StateType::STATE_TUNED)
          m_State = StateType::STATE_TUNED;

        cmd.setField16(0, CMD_DAB_DIGRAD_STATUS);
        cmd.sendCommand(2);
      }

      RequestedAction = Si468xAction::None;
    }
  }
  else
  {
    RequestedAction = Si468xAction::None;
  }
}

void
Si4688DABManager::sendCommandGetEventStatus()
{
  auto & cmd = Si4688Driver::getDriver(*this).tSiCmd;

  cmd.setField8(0, CMD_DAB_GET_EVENT_STATUS);
  cmd.setField8(1,                      0x1);
  cmd.sendCommand(2);
}

void Si4688DABManager::handleDABDigradStatusCommand()
{
  auto & drv = Si4688Driver::getDriver(*this);
  auto & cmd = drv.tSiCmd;

  if((4+18) > cmd.getReadLen())
  {
    cmd.readResponse(4+18);
    return;
  }

  if(m_State < StateType::STATE_TUNED)
    AverageRssi.init(Si468xRssiType{cmd.getSigned8(6),1});
#if 0
  else
    AverageRssi.sample(Si468xRssiType{cmd.getSigned8(6),1});
#endif

  TunedFrequency = cmd.getField32(12);
  FFTOffset      = cmd.getField8(17);

  // drv.AntCapVal = cmd.getField8(18);

  m_Debug = cmd.getField32(5);
  Globals::statusChanged();

  if(!cmd.getStatus().isSeeking())
  {
    bool valid = (cmd.getField8(5) & 0x01) != 0;

    if(m_State == StateType::STATE_TUNING)
    {
      if (valid)
      {
        m_State = StateType::STATE_TUNED;
        changeTunerState(TunerStateType::Tuned);
      }
      else
      {
        m_State = StateType::STATE_READY;
        changeTunerState(TunerStateType::Failed);

        RequestedAction = Si468xAction::Retune;
      }
    }

    /* When sucessfully tuned into the ensemble, start playing */
    if(m_State == StateType::STATE_TUNED)
    {
      if(drv.m_ServiceIterator->isDabService())
      {
        m_State = StateType::STATE_PLAY;

        cmd.setField32(0, CMD_START_DIGITAL_SERVICE);
        cmd.setField32(4, drv.m_ServiceIterator->ServiceId);
        cmd.setField32(8, drv.m_ServiceIterator->CId);
        cmd.sendCommand(12);
      }
    }

    /* if we still can send a command, either poll for dab
     * status or process with test rssi command */
    if(cmd.isReady())
    {
      if(getTunerState() == TunerStateType::Tuned)
        sendCommandGetEventStatus();
      else
        sendCommandTestGetRssi();
    }
  }
}

void Si4688DABManager::handleDABGetEventStatusCommand()
{
  auto & drv = Si4688Driver::getDriver(*this);
  auto & cmd = drv.tSiCmd;

  if((4+4) > cmd.getReadLen())
  {
    cmd.readResponse(4+4);
    return;
  }

  if(cmd.getField8(5) & 0x01)
  {
    cmd.setField16(0, CMD_GET_DIGITAL_SERVICE_LIST);
    cmd.sendCommand(2);
  }
  else
  {
    sendCommandTestGetRssi();
  }
}

void Si4688DABManager::handleTestGetRssi()
{
  auto & drv = Si4688Driver::getDriver(*this);
  auto & cmd = drv.tSiCmd;

  if(6 > cmd.getReadLen())
  {
    cmd.readResponse(6);
    return;
  }

  AverageRssi.sample(Si468xRssiType{cmd.getSigned16(4), 256});
}

void Si4688DABManager::handleDABSetFreqListStatusCommand()
{
  //auto & drv = Si4688Driver::getDriver(*this);
  //auto & cmd = drv.tSiCmd;
}

void Si4688DABManager::handleDABStartDigitalServiceCommand(bool success)
{
  if(success)
    Globals::getInstance().notifyServiceStarted();
  else
    m_State = StateType::STATE_TUNED;

  if(getTunerState() == TunerStateType::Tuned)
    sendCommandGetEventStatus();
  else
    sendCommandTestGetRssi();
}

void Si4688DABManager::handleGetDigitalServiceListCommand()
{
  auto & drv = Si4688Driver::getDriver(*this);
  auto & cmd = drv.tSiCmd;

  unsigned int size;
  unsigned int offset;
  unsigned int num_services;

  if(6 > cmd.getReadLen())
  {
    cmd.readResponse(6);
    return;
  }

  size = cmd.getField16(4);

  if( (8+size) > cmd.getBufferSize())
  {
    size = cmd.getBufferSize() - 8;
  }

  if(size == 0)
    return;

  if(((8 + size) > cmd.getReadLen()))
  {
    cmd.readResponse(8 + size);
    return;
  }

  size    = 6 + cmd.getField16(4);
  offset  = 4;

  ServiceCount = cmd.getField8(offset + 4);
  offset += 8;

  num_services = ServiceCount;

  while(num_services--)
  {
    const char* service_name;
    unsigned int num_component;
    uint_fast16_t service_id;
    uint_fast8_t flags;

    if((offset + 24) > size)
      return;

    service_id    = cmd.getField32(offset + 0);
    flags         = cmd.getField8(offset+4);
    num_component = cmd.getField8(offset+5) & 0x0F;
    service_name  = static_cast<const char*>(cmd.getArg(offset+8));
    offset += 24;

    while(num_component--)
    {
      auto & db = Globals::getInstance().Db;

      bool     primary_component;
      uint16_t component_id;

      if((offset + 4) > size)
        return;

      if(0 == (flags & 0x01))
      { /* program service */
        component_id      = cmd.getField16(offset);
        primary_component = (0 != (cmd.getField8(offset+2) & 0x2));

        if(primary_component)
        {
          /* init service value current service */
          ServiceValue v {};
          ServiceName  tmp;

          v.DabChannel = m_DabChannel;
          v.ServiceId  = service_id;
          v.CId        = component_id;

          tmp.assignRawValue(service_name, 16);
          v.Name = tmp.trim();

          auto h = db.updateService(v);

          /* reload currently listening service with most recent information */
          if(service_id == drv.m_ServiceIterator->ServiceId)
            drv.m_ServiceIterator = ServiceIterator(h);
        }
      }
      else
      { /* Data service */

      }

      offset += 4;
    }
  }



  sendCommandTestGetRssi();
}

void Si4688DABManager::handleTuning()
{
  auto & cmd = Si4688Driver::getDriver(*this).tSiCmd;

  cmd.setField16(0, CMD_DAB_DIGRAD_STATUS);
  cmd.sendCommand(2);
}

void Si4688DABManager::sendCommandTestGetRssi()
{
  auto & cmd = Si4688Driver::getDriver(*this).tSiCmd;

  cmd.setField16(0, CMD_TEST_GET_RSSI);
  cmd.sendCommand(2);
}

void Si4688DABManager::handleCommandFinished(uint_fast8_t command, const Si4688Status& status)
{
  auto & drv = Si4688Driver::getDriver(*this);
  bool success = !status.isErrorBitSet();

  if(success)
    drv.startTimeout(Milliseconds(200));

  switch(m_State)
  {
  case StateType::STATE_W_SET_PROPERTIES:
    if (success)
      handleDABBoot();
    else
      drv.activateMode(Si468xOperationMode::DABRadio, true);
    break;
  case StateType::STATE_W_SET_CHANNELS:
    if (success)
    {
      m_State = StateType::STATE_READY;
      drv.pollAsync();
    }
    break;
  case StateType::STATE_TUNING:
    if((command == CMD_DAB_DIGRAD_STATUS) && success)
      handleDABDigradStatusCommand();
    else
      handleTuning();
    break;
  case StateType::STATE_READY:
  case StateType::STATE_TUNED:
  case StateType::STATE_PLAY:
    if(command == CMD_START_DIGITAL_SERVICE)
    {
      handleDABStartDigitalServiceCommand(success);
    }
    else if (success)
    {
      if(command == CMD_DAB_DIGRAD_STATUS)
        handleDABDigradStatusCommand();
      else if(command == CMD_GET_DIGITAL_SERVICE_LIST)
        handleGetDigitalServiceListCommand();
      else if(command == CMD_DAB_GET_EVENT_STATUS)
        handleDABGetEventStatusCommand();
      else if(command == CMD_TEST_GET_RSSI)
        handleTestGetRssi();
    }
    break;
  case StateType::STATE_ERROR:
    break;
  }
}

void Si4688DABManager::timeout()
{
  auto & drv = Si4688Driver::getDriver(*this);

  drv.startTimeout(Milliseconds(250));
  poll();
}

void Si4688DABManager::poll()
{
  auto & drv = Si4688Driver::getDriver(*this);
  auto & cmd = drv.tSiCmd;

  if(!cmd.isReady())
    return;

  switch(m_State)
  {
  case StateType::STATE_W_SET_PROPERTIES:
    handleDABBoot();
    break;
  case StateType::STATE_READY:
    handleAction();
    break;
  case StateType::STATE_TUNED:
  case StateType::STATE_TUNING:
  case StateType::STATE_PLAY:
    handleAction();

    if(cmd.isReady())
    {
      cmd.setField16(0, CMD_DAB_DIGRAD_STATUS);
      cmd.sendCommand(2);
    }
    break;
  case StateType::STATE_ERROR:
    handleAction();
  }
}
