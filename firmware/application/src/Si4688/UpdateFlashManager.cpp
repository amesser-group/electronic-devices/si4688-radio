/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Si4688/Driver.hpp"
#include "target_Globals.hpp"

using namespace target;
using namespace MusicBox;

Si4688StartUpdateFlashManager::Si4688StartUpdateFlashManager(ElmChanFile & file, uint32_t offset) :
  Si4688FirmwareManager {Si468xOperationMode::UpdateFlash}, File {file}, FlashOffset {offset}
{
}

void Si4688StartUpdateFlashManager::handleCommandFinished(uint_fast8_t command, const Si4688Status& status)
{
  if (m_State < StateType::STATE_W_LOAD_PATCH)
  {
    Si4688FirmwareManager::handleCommandFinished(command, status);
  }
  else
  {
    auto & drv = Si4688Driver::getDriver(*this);
    bool success = !status.isErrorBitSet();

    if (0 == File.getSize())
    { /* nothing to flash, abort */
      drv.activateMode(Si468xOperationMode::Disabled);
    }
    else if(success)
    {
      ElmChanFile  f{File};
      drv.changeMgr<Si4688UpdateFlashManager>(f, FlashOffset);
    }
    else
    { /* error during flashing, abort */
      drv.activateMode(Si468xOperationMode::Disabled);
    }

    Globals::statusChanged();
  }
}

Si4688UpdateFlashManager::Si4688UpdateFlashManager(ElmChanFile & file, uint32_t offset) :
  Si4688ManagerBase {Si468xOperationMode::UpdateFlash}, File {file}, FlashOffset {offset}
{
}

void Si4688UpdateFlashManager::writeNextBlock()
{
  auto & drv = Si4688Driver::getDriver(*this);
  auto & cmd = drv.tSiCmd;

  if( (0 == (FlashOffset % 4096)) &&
      (SI4688_SUBCMD_FLASH_PASS_THROUGH::ERASE_SECTOR != LastSubCmd))
  { /* need to erase sector first */

    cmd.setField8(0, CMD_FLASH_PASS_THROUGH);
    cmd.setField8(1, SI4688_SUBCMD_FLASH_PASS_THROUGH::ERASE_SECTOR);
    cmd.setField8(2, 0xC0);
    cmd.setField8(3, 0xDE);
    cmd.setField32(4, FlashOffset);

    LastSubCmd = SI4688_SUBCMD_FLASH_PASS_THROUGH::ERASE_SECTOR;
    cmd.sendCommand(8);
  }
  else
  { /* check for next flash sector to write */
    uint8_t *buf = reinterpret_cast<uint8_t*>(cmd.setArg(16, 512));
    long count;

    if(buf)
      count = File.read(buf, 512);
    else
      count = -1;

    if(count <= 0)
    {
      drv.tuneChannel(drv.m_ServiceIterator);
    }
    else
    {
      if(count < 512)
      {
        memset(&(buf[count]), 0x00, 512 - count);
        count = 512;
      }

      cmd.setField8(0, CMD_FLASH_PASS_THROUGH);
      cmd.setField8(1, SI4688_SUBCMD_FLASH_PASS_THROUGH::WRITE_BLOCK);
      cmd.setField8(2, 0x0C);
      cmd.setField8(3, 0xED);
      cmd.setField32(4, 0);
      cmd.setField32(8, FlashOffset);
      cmd.setField32(12,count);

      LastSubCmd = SI4688_SUBCMD_FLASH_PASS_THROUGH::WRITE_BLOCK;
      cmd.sendCommand(16 + count);
    }

  }
}

void Si4688UpdateFlashManager::handleCommandFinished(uint_fast8_t command, const Si4688Status& status)
{
  bool success = !status.isErrorBitSet();

  if(success)
  {
    if (LastSubCmd != SI4688_SUBCMD_FLASH_PASS_THROUGH::ERASE_SECTOR)
      FlashOffset += 512;

    writeNextBlock();
  }
  else
  { /* error during flashing, abort */
    auto & drv = Si4688Driver::getDriver(*this);
    drv.activateMode(Si468xOperationMode::Disabled);
  }

  Globals::statusChanged();
}

void Si4688UpdateFlashManager::poll()
{
  auto & drv = Si4688Driver::getDriver(*this);
  auto & cmd = drv.tSiCmd;

  if (0 == File.getSize())
  { /* nothing to flash, abort */
    drv.activateMode(Si468xOperationMode::Disabled);
  }
  else if(cmd.isReady())
  {
    writeNextBlock();
  }
}

void Si4688UpdateFlashManager::timeout()
{
  auto & drv = Si4688Driver::getDriver(*this);
  drv.pollAsync();
}