/*
 *  Copyright 2016-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SI4688_FIRMWAREMANAGER_HPP_
#define SI4688_FIRMWAREMANAGER_HPP_

#include "Si4688/Common.hpp"

namespace MusicBox
{
  class Si4688FirmwareManager : public Si4688ManagerBase
  {
  public:
    enum class StateType : uint8_t
    {
      STATE_POWERON,
      STATE_W_RESET_ASSERTED,
      STATE_W_RESET_DEASSERTED,
      STATE_W_CMD_POWERUP,
      STATE_W_CMD_LOAD_PATCH,
      STATE_W_LOAD_PATCH,
      STATE_W_CMD_LOAD_FIRMWARE,
      STATE_W_LOAD_FIRMWARE,
      STATE_W_CMD_BOOT,
      STATE_W_SET_PROPERTIES,
      STATE_RUN,
      STATE_ERROR,
      STATE_W_RESET,
    };

    constexpr Si4688FirmwareManager (Si468xOperationMode target_mode) : Si4688ManagerBase {Si468xOperationMode::Booting}, TargetMode {target_mode} {}
    virtual  ~Si4688FirmwareManager() {}

    void handleCommandFinished(uint_fast8_t command, const Si4688Status& status) override;
    void poll() override;
    void timeout() override;

    static const uint8_t s_PowerUpCmd[16];

    const Si468xOperationMode TargetMode;

    static const uint8_t Rom00Patch016[5796];

  protected:
    StateType               m_State  { StateType::STATE_POWERON};

    uint_least16_t m_Offset {0};

    void handleTimeout();
    void bootChip();

    void resetDevice();

    void handleLoadPatch();
    void handleLoadFirmware();
  };
}
#endif