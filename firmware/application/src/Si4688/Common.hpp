/*
 *  Copyright 2016-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SI4688_MANAGERBASE_HPP_
#define SI4688_MANAGERBASE_HPP_

#include <cstdint>
#include <cstring>

#include "target_System.hpp"

#include "ecpp/Math/Fixedpoint.hpp"
#include "ecpp/Math/Statistics.hpp"
#include "Si4688/Registers.hpp"

#include "database.hpp"
#include "Database/DabChannels.hpp"
#include "helpers.hpp"

namespace MusicBox
{
  using namespace ::target;

  using ::std::memcpy;
  using ::std::uint_least8_t;
  using ::std::uint_least32_t;

  enum class Si468xOperationMode : uint_least8_t
  {
    Reset,
    Disabled,
    Booting,
    FMRadio,
    DABRadio,
    UpdateFlash,
  };

  enum class TunerStateType : uint_least8_t
  {
    Unknown,
    Tuned,
    Failed,
  };


  struct Si468xEvent
  {
    struct OpmodeChanged;
  };

  struct Si468xEvent::OpmodeChanged : public Si468xEvent
  {
    constexpr OpmodeChanged(Si468xOperationMode m) : Si468xEvent(), mode(m) {};

    Si468xOperationMode mode;
  };



  class Si4688Status
  {
  private:
    uint8_t m_Status[4] {};

  public:
    constexpr Si4688Status() {};
    constexpr Si4688Status(uint32_t init) : m_Status {static_cast<uint8_t>((init >> 0) & 0xFF),
                                                      static_cast<uint8_t>((init >> 8) & 0xFF),
                                                      static_cast<uint8_t>((init >> 16) & 0xFF),
                                                      static_cast<uint8_t>((init >> 24) & 0xFF)} {};

    constexpr uint32_t getStatus32() const
    {
      return (m_Status[0] << 0UL) | (m_Status[1] << 8UL) | (m_Status[2] << 16UL) | (m_Status[3] << 24UL);
    }

    uint8_t operator [] (int index) const {return m_Status[index];}

    void update(const uint8_t* pBuf) { memcpy(m_Status, pBuf, 4); }

    bool isClearToSend() const {return 0 != (m_Status[0] & MSK_STATUS0_CTS);}
    bool isErrorBitSet() const {return 0 != (m_Status[0] & MSK_STATUS0_ERR);}
    bool isSeeking()     const {return 0 == (m_Status[0] & MSK_STATUS0_STCINT);}

    static constexpr const Si4688Status & fromBuffer(const uint8_t* pBuf)
    {
      return *reinterpret_cast<const Si4688Status*>(pBuf);
    }
  };

  class Si4688Transaction
  {
  private:
    enum class StateType : uint8_t
    {
      IDLE,
      W_START,
      W_TWI_WCNF,
      W_TWI_RCNF,
    };

    StateType  m_State                { StateType::IDLE };

    Si4688Status m_LastStatus         {};
    System::Tick        LastCommandSentTicks;
    uint8_t             m_LastCommand {0};
    uint_least16_t      m_ReadLen     {0};
    uint8_t             m_Buffer[16 + 512] {};

    bool sendReadCommand(void* pBuf, size_t BufferLen);
  public:

    void setField8(unsigned int idx, uint8_t value)
    {
      m_Buffer[idx] = value;
    }

    uint8_t getField8(unsigned int idx) const
    {
      return m_Buffer[idx];
    }

    int8_t getSigned8(unsigned int idx) const
    {
      return static_cast<int8_t>(getField8(idx));
    }

    void setField16(unsigned int idx, uint16_t value)
    {
      m_Buffer[idx]   = value & 0xFF;
      m_Buffer[idx+1] = (value >> 8) & 0xFF;
    }

    uint16_t getField16(unsigned int idx) const
    {
      return  m_Buffer[idx] |
            (m_Buffer[idx+1] << 8);
    }

    int16_t getSigned16(unsigned int idx) const
    {
      return static_cast<int16_t>(getField16(idx));
    }

    void setField32(unsigned int idx, uint32_t value)
    {
      m_Buffer[idx]   = value & 0xFF;
      m_Buffer[idx+1] = (value >> 8) & 0xFF;
      m_Buffer[idx+2] = (value >> 16) & 0xFF;
      m_Buffer[idx+3] = (value >> 24) & 0xFF;
    }

    uint32_t getField32(unsigned int idx) const
    {
      return  m_Buffer[idx] |
            (m_Buffer[idx+1] << 8) |
            ((uint32_t)m_Buffer[idx+2] << 16) |
            ((uint32_t)m_Buffer[idx+3] << 24);
    }

    const void*  getArg(unsigned int idx) const
    {
      return &(m_Buffer[idx]);
    }

    void*        setArg(unsigned int idx, unsigned int BufferLen)
    {
      if ( (idx+BufferLen) > getBufferSize())
        return nullptr;

      return &m_Buffer[idx];
    }

    uint_fast8_t setArg(unsigned int idx, const void *pvBuf, uint_fast8_t BufferLen)
    {
      if((idx + BufferLen) > sizeof(m_Buffer))
        BufferLen = sizeof(m_Buffer) - idx;

      memcpy(&m_Buffer[idx], pvBuf, BufferLen);

      return BufferLen;
    }


    bool sendCommand(const void* pBuf, size_t BufferLen);

    bool sendCommand(size_t BufferLen)
    {
      if(BufferLen > sizeof(m_Buffer))
        return false;

      return sendCommand(m_Buffer, BufferLen);
    }

    bool readResponse(void* pBuf, size_t BufferLen);

    bool readResponse(size_t BufferLen = 4)
    {
      if(BufferLen > sizeof(m_Buffer))
        return false;

      return readResponse(m_Buffer, BufferLen);
    }

    bool setProperty(uint_fast16_t Property, uint_fast16_t Value);

    uint8_t        getLastCommand() const { return m_LastCommand;}
    uint_fast16_t  getReadLen()       const {return m_ReadLen;}

    const Si4688Status & getStatus() const
    {
      return m_LastStatus;
    }

    bool isReady()   const { return m_State == StateType::IDLE; }

    bool isPending() const { return m_State != StateType::IDLE; }

    void handleTwiFinished(bool success);

    unsigned int getBufferSize() const {return sizeof(m_Buffer);}
  };

  class Si468xActionPerformer : public JobAble
  {
  protected:
    constexpr Si468xActionPerformer () : JobAble() {}
  };

  class Si4688ManagerBase
  {
  public:
    constexpr Si4688ManagerBase(Si468xOperationMode mode) : OpMode { mode } {}
    virtual  ~Si4688ManagerBase() {};

    constexpr Si468xOperationMode getOpMode()     const { return OpMode; }

    virtual void handleCommandFinished(uint_fast8_t command, const Si4688Status& status) = 0;
    virtual void poll()    = 0;
    virtual void timeout() = 0;

    /* We have a 2MByte flash soldered
     * So we use the following partition table:
     *
     * 0x004000 :   16k  Romloader patch
     * 0x008000 :   32k  FM Firmware
     * 0x0a0000 :  640k  DAB Firmware
     */
    static constexpr uint32_t FlashAddrFMFirmware  = 0x08000;
    static constexpr uint32_t FlashAddrDABFirmware = 0xa0000;
  protected:
    const          Si468xOperationMode OpMode;
  };


  enum class Si468xAction : uint_least8_t
  {
    None = 0,
    SeekForward,
    SeekBackWard,
    Retune,
    Tune,
  };

  typedef ::ecpp::FixedPoint<uint_least32_t, 256> Si468xRssiType;

  class Si4688RadioManager : public Si4688ManagerBase
  {
  public:
    constexpr Si4688RadioManager(Si468xOperationMode mode) : Si4688ManagerBase{mode} {};

    constexpr TunerStateType      getTunerState() const { return TunerState; }
    constexpr uint_least32_t      getTunedFreq()  const { return TunedFrequency; }
    constexpr Si468xRssiType      getRssi()       const { return AverageRssi.getValue(); }

    void tune(bool force = false);
    void seekForward();
    void seekBackward();
  protected:

    void changeTunerState(TunerStateType state);

    TunerStateType                                     TunerState      { TunerStateType::Unknown };
    Si468xAction                                       RequestedAction { Si468xAction::Tune };
    uint_least32_t                                     TunedFrequency  {0};
    ::ecpp::Math::Statistics::PT1<Si468xRssiType, 16>  AverageRssi     {-128};
  };
};

#endif
