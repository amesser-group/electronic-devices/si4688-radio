/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_SI468XDABCHANNELSCANNER_H_
#define MUSICBOX_SI468XDABCHANNELSCANNER_H_

#include "Si4688/Common.hpp"
#include "target_System.hpp"

namespace MusicBox
{
  using namespace target;

  class Si468xDabChannelScanner : public Si468xActionPerformer
  {
  public:
    constexpr Si468xDabChannelScanner() : Si468xActionPerformer() {}

    void startScan();
    void stopScan();

    constexpr bool isScanning() const { return ChannelIdx >= 0; }
  private:
    static void timeout_job( System::Job & job);

    void timeout();
    void scanNextChannel();

    void prepareDatabase();
    void cleanupDatabase();

    System::Timer   Timer      {};
    System::Job     TimeoutJob { timeout_job };

    int_least8_t ChannelIdx {-1};
    int_least8_t Retry {0};
  };

}

#endif // MUSICBOX_SI468XDABCHANNELSCANNER_H_
