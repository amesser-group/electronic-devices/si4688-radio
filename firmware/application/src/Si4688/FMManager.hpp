/*
 *  Copyright 2016-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SI4688_FMRADIOMANAGER_HPP_
#define SI4688_FMRADIOMANAGER_HPP_

#include "Si4688/Common.hpp"
#include "rds.hpp"

namespace MusicBox
{
  class Si4688FMManager : public Si4688RadioManager
  {
  public:
    enum class StateType : uint8_t
    {
      STATE_CLOSED,
      STATE_BOOTED,
      STATE_W_SET_PROPERTIES,
      STATE_READY,
      STATE_SEEKING,
      STATE_RUN,
      STATE_ERROR,
    };

    constexpr Si4688FMManager() : Si4688RadioManager(Si468xOperationMode::FMRadio) {}
    virtual  ~Si4688FMManager() {}

    const RDSState & getRDSState () const { return RdsManager.getState(); }
  private:
    StateType                m_State  = StateType::STATE_BOOTED;

    uint_least16_t           m_Offset {0};

    static const uint16_t s_BootupProperties[];

    RDSManager  RdsManager;

    bool        sendRDSStatus_req();
    bool        sendRDSBlockCount_req();

    void        handleRSQStatusCommand();
    void        handleRDSStatusCommand();

    void        handleFMBoot();
    void        handleAction();
    void        handleSeek(uint_fast8_t command, const Si4688Status& status);
    void        handleTimeout();

    void        resetState();
    void        handleRDSFrame(const RDSFrame &Frame);

  public:
    void handleCommandFinished(uint_fast8_t command, const Si4688Status& status) override;
    void poll() override;
    void timeout() override;
  };
}

#endif