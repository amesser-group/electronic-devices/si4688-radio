/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Si468xDabChannelScanner.hpp"
#include "target_Globals.hpp"

using namespace target;
using namespace MusicBox;

using Database::ServiceIterator;
using Database::ServiceValueIterator;
using Database::ServiceValue;

using ::ecpp::Units::Milliseconds;

void
Si468xDabChannelScanner::startScan()
{
  /* dont write channels during scanning */
  Globals::getInstance().Storage.setSupressSaves(true);

  prepareDatabase();

  ChannelIdx = -1;
  scanNextChannel();
}

void
Si468xDabChannelScanner::stopScan()
{
  auto & s = Globals::getInstance().SiHandler;

  ChannelIdx = -1;
  Timer.stop();

  cleanupDatabase();
  s.activateMode(Si468xOperationMode::Disabled, true);

  Globals::getInstance().Storage.setSupressSaves(false);
}

void
Si468xDabChannelScanner::prepareDatabase()
{
  auto & db = Globals::getInstance().Db;
  ServiceValue val;

  /* clear dab information for all channels */
  /* clear dab information for all channels */
  for(auto it = db.beginService(); it != db.endService(); ++it)
  {
    db.clearDABProperties(it.getHandle());
  }
}

void
Si468xDabChannelScanner::cleanupDatabase()
{
  auto & db = Globals::getInstance().Db;

  ServiceValueIterator it = db.beginService();
  while(it != db.endService())
  {
    if(!(it->isDabService() || it->isFMService()))
    {
      auto h = it.getHandle();

      /* advance iterator before removing the service */
      ++it;

      db.removeService(h);
    }
    else
    {
      ++it;
    }
  }
}

void
Si468xDabChannelScanner::scanNextChannel()
{
  auto & s = Globals::getInstance().SiHandler;
  auto & l = Globals::getInstance().Db.getDabChannelList();

  ChannelIdx++;

  if((ChannelIdx >= 0) && ChannelIdx < l.MaxTransponders)
  {
    auto & chan = l[ChannelIdx];

    if(chan.Handle.isValid() && chan.Freq > 0)
    {
      Retry = 10;

      s.tuneChannel(chan);
      Timer.start(Milliseconds(1000), TimeoutJob);
    }
    else
    {
      stopScan();
    }
  }
  else
  { /* scan finished */
    stopScan();
  }
}

void
Si468xDabChannelScanner::timeout()
{
  const auto & s = Globals::getInstance().SiHandler;

  if(ChannelIdx < 0)
    return;

  if(s.getOpMode() == Si468xOperationMode::DABRadio)
  {
    const auto & dab = s.getDABManager();

    if( (dab.getTunerState() == TunerStateType::Tuned) &&
        (dab.getServiceCount() > 0))
    { /* channel tuned and ensemble info scanned */
      scanNextChannel();
    }
    else if(Retry > 0)
    {
      --Retry;
      Timer.start(Milliseconds(1000), TimeoutJob);
    }
    else
    {
      scanNextChannel();
    }
  }
  else if(Retry > 0)
  { /* dab mnode not yet booted */
    --Retry;
    Timer.start(Milliseconds(1000), TimeoutJob);
  }
  else
  {
    stopScan();
  }
}


void
Si468xDabChannelScanner::timeout_job(System::Job & job)
{
  container_of(Si468xDabChannelScanner, TimeoutJob, job).timeout();
}
