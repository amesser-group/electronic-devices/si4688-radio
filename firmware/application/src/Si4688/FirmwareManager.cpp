/*
 *  Copyright 2016-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Si4688/Driver.hpp"
#include "target_Bsp.hpp"

using namespace target;
using namespace MusicBox;

void
Si4688FirmwareManager::resetDevice()
{
  auto & drv = Si4688Driver::getDriver(*this);

  if(drv.tSiCmd.isReady())
  {
    if(Bsp::instance().SetSi4688Reset())
    {
      m_State = StateType::STATE_W_RESET_ASSERTED;
      drv.startTimeout(Milliseconds(100));
    }
    else
    {
      drv.startTimeout(Milliseconds(10));
    }
  }
  else
  {
    drv.startTimeout(Milliseconds(10));
  }
}

/* Using a CX3225SB crystal 19.2 Mhz CL=8pf*/
constexpr uint8_t  clk_mode  = 1;
constexpr uint8_t  tr_size   = 0x7;        /* for 19.2 Mhz crystal */
constexpr uint8_t  ibias     = 600 / 10 ;  /* 600 µA startup bias */
constexpr uint8_t  ibias_run = 100 / 10;   /* 200 µA runtime bias */
constexpr uint32_t freq      = 19200000;   /* crystal freq */
constexpr uint8_t  ctun      = static_cast<uint8_t>(2*(8. - 3.) / 0.381); /* tuning capacitance occording to manual */

constexpr uint8_t Si4688FirmwareManager::s_PowerUpCmd[16] =
{
  CMD_POWER_UP,
  0,
  (clk_mode << 4) | tr_size,
  0x7f & ibias,
  freq & 0xFF,
  (freq >> 8) & 0xFF,
  (freq >> 16) & 0xFF,
  (freq >> 24) & 0xFF,
  0x3f & ctun,
  0x10,
  0,
  0,
  0,
  0x7F & ibias_run,
  0,
  0
};

void Si4688FirmwareManager::bootChip()
{
  auto & cmd = Si4688Driver::getDriver(*this).tSiCmd;

  if((TargetMode != Si468xOperationMode::FMRadio) &&
     (TargetMode != Si468xOperationMode::DABRadio) &&
     (TargetMode != Si468xOperationMode::UpdateFlash))
    return;

  m_State = StateType::STATE_W_CMD_POWERUP;
  cmd.sendCommand(s_PowerUpCmd, sizeof(s_PowerUpCmd));
}

void
Si4688FirmwareManager::handleLoadPatch()
{
  auto & drv = Si4688Driver::getDriver(*this);
  auto & cmd = drv.tSiCmd;

  if(m_State == StateType::STATE_W_CMD_POWERUP)
  {
    m_State = StateType::STATE_W_CMD_LOAD_PATCH;
    m_Offset = 0;

    cmd.setField16(0, CMD_LOAD_INIT);
    cmd.sendCommand(2);
  }
  else if(m_State == StateType::STATE_W_CMD_LOAD_PATCH)
  {
    if(m_Offset < sizeof(Rom00Patch016))
    {
      uint_fast8_t DataLen;
      cmd.setField32(0, CMD_HOST_LOAD);

      DataLen = cmd.setArg(4, &(Rom00Patch016[m_Offset]), sizeof(Rom00Patch016) - m_Offset);
      /* Datalength must be 4 byte multiples */
      DataLen -= DataLen % 4;

      m_Offset += DataLen;
      cmd.sendCommand(4 + DataLen);
    }
    else
    {
      m_State = StateType::STATE_W_LOAD_PATCH;
      drv.startTimeout(Milliseconds(5));
    }
  }
}

void Si4688FirmwareManager::handleLoadFirmware()
{
  auto & drv = Si4688Driver::getDriver(*this);
  auto & cmd = drv.tSiCmd;

  if(m_State == StateType::STATE_W_LOAD_PATCH)
  {
    m_State = StateType::STATE_W_CMD_LOAD_FIRMWARE;
    m_Offset = 0;

    cmd.setField16(0, CMD_LOAD_INIT);
    cmd.sendCommand(2);
  }
  else if(m_State == StateType::STATE_W_CMD_LOAD_FIRMWARE)
  {
    m_State = StateType::STATE_W_LOAD_FIRMWARE;

    switch(TargetMode)
    {
    case Si468xOperationMode::Reset:
    case Si468xOperationMode::Disabled:
    case Si468xOperationMode::Booting:
      break;
    case Si468xOperationMode::FMRadio:
      cmd.setField32(4, FlashAddrFMFirmware);
      break;
    case Si468xOperationMode::DABRadio:
      cmd.setField32(4, FlashAddrDABFirmware);
      break;
    case Si468xOperationMode::UpdateFlash:
      break; /* handled independendly */
    }

    cmd.setField32(0, CMD_FLASH_PASS_THROUGH);
    cmd.setField32(8, 0);

    cmd.sendCommand(12);
  }
  else if(m_State == StateType::STATE_W_LOAD_FIRMWARE)
  {
    m_State = StateType::STATE_W_CMD_BOOT;

    cmd.setField16(0, CMD_BOOT);
    cmd.sendCommand(2);
  }
  else if(m_State == StateType::STATE_W_CMD_BOOT)
  {
    m_State  = StateType::STATE_W_SET_PROPERTIES;
    m_Offset = 0;
  }

  if(m_State == StateType::STATE_W_SET_PROPERTIES)
  {
    auto properties = Bsp::instance().si4688_board_properties(drv);

    if(0xFFFF != properties[m_Offset])
    {
      m_Offset += 2;
      cmd.setProperty(properties[m_Offset-2], properties[m_Offset-1]);
    }
    else
    {
      drv.changeMgr(TargetMode);
    }
  }
}

void Si4688FirmwareManager::handleCommandFinished(uint_fast8_t command, const Si4688Status& status)
{
  bool success = !status.isErrorBitSet();

  switch(m_State)
  {
  case StateType::STATE_W_CMD_POWERUP:
    if(success)
      handleLoadPatch();
    else
      resetDevice();
    break;
  case StateType::STATE_W_CMD_LOAD_PATCH:
    if(success)
      handleLoadPatch();
    else
      resetDevice();
      /* m_State = StateType::STATE_ERROR; */
    break;
  case StateType::STATE_W_CMD_LOAD_FIRMWARE:
  case StateType::STATE_W_LOAD_FIRMWARE:
  case StateType::STATE_W_CMD_BOOT:
  case StateType::STATE_W_SET_PROPERTIES:
    if(success)
      handleLoadFirmware();
    else
      m_State = StateType::STATE_ERROR;
    break;
  default:
    resetDevice();
    break;
  }
}

void
Si4688FirmwareManager::poll()
{
  switch(m_State)
  {
  case StateType::STATE_POWERON:
    resetDevice();
    break;
  default:
    break;
  }
}

void
Si4688FirmwareManager::timeout()
{
  auto & drv = Si4688Driver::getDriver(*this);

  if(!drv.tSiCmd.isReady())
  {
    drv.startTimeout(Milliseconds(5));
    return;
  }

  switch(m_State)
  {
  case StateType::STATE_POWERON:
    resetDevice();
    break;
  case StateType::STATE_W_RESET_ASSERTED:
    m_State = StateType::STATE_W_RESET_DEASSERTED;

    Bsp::instance().ClearSi4688Reset();
    drv.startTimeout(Milliseconds(100));
    break;
  case StateType::STATE_W_RESET_DEASSERTED:
    bootChip();
    break;
  case StateType::STATE_W_LOAD_PATCH:
    handleLoadFirmware();
    break;
  default:
    break;
  }
}

