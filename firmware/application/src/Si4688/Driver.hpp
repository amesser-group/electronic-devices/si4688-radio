/*
 *  Copyright 2016-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SI4688_DRIVER_HPP_
#define SI4688_DRIVER_HPP_

#include "Si4688/Common.hpp"
#include "Si4688/DABManager.hpp"
#include "Si4688/FirmwareManager.hpp"
#include "Si4688/FMManager.hpp"
#include "Si4688/UpdateFlashManager.hpp"
#include "Si4688/AntCapMeasurer.hpp"
#include "Si4688/Si468xDabChannelScanner.hpp"
#include "ecpp/Units/Time.hpp"

#include <variant>

namespace MusicBox
{
  using namespace target;

  typedef ::std::variant<::std::monostate,
                         Si468xAntCapMeasurer,
                         Si468xDabChannelScanner> Si468xActionsContainer;

  template<typename T>
  class Si468xActionWrapper
  {
  public:
    constexpr Si468xActionWrapper(Si468xActionsContainer &c) : Container{c} {}

    template<typename ...ARGS>
    T* aquire(ARGS... args);

    T* get();
    void release();

  private:
    Si468xActionsContainer & Container;
  };

  template<typename T>
  template<typename ...ARGS>
  T* Si468xActionWrapper<T>::aquire(ARGS ...args)
  {
    if (::std::holds_alternative<::std::monostate>(Container))
      return &Container.emplace<T>(args...);
    else
      return nullptr;
  }

  template<typename T>
  T* Si468xActionWrapper<T>::get()
  {
    return ::std::get_if<T>(&Container);
  }

  template<typename T>
  void Si468xActionWrapper<T>::release()
  {
    if (::std::holds_alternative<T>(Container))
      Container.emplace<::std::monostate>();
  }

  class Si4688Driver : protected JobAble
  {
  public:
    constexpr Si4688Driver() : JobAble() {}
    ~Si4688Driver();

    const     Si4688ManagerBase & getMgr()        const { return OpMgr.Firmware; }
    const     Si468xOperationMode getOpMode()     const { return getMgr().getOpMode(); }
    const     TunerStateType      getTunerState() const { return getRadioManager().getTunerState(); }
    const     uint_least32_t      getTunedFreq()  const { return getRadioManager().getTunedFreq(); }
    const     RDSState          & getRDSState()   const { return getFMManager().getRDSState(); }

    const Si4688RadioManager       & getRadioManager()        const  { return OpMgr.DAB;}
    const Si4688DABManager         & getDABManager()          const  { return OpMgr.DAB;}
    const Si4688FMManager          & getFMManager()           const  { return OpMgr.FM;}
    const Si4688UpdateFlashManager & getUpdateFlashManager()  const  { return OpMgr.UpdateFlash;}
  private:
    Si4688DABManager   & getDABManager()   { return OpMgr.DAB;}
    Si4688FMManager    & getFMManager()    { return OpMgr.FM;}

    void changedMgr();

    static void poll_job(System::Job &job);
    static void timeout_job(System::Job &job);

    System::Job   PollJob      {poll_job};
    System::Job   TimeoutJob   {timeout_job};
    System::Timer TimeoutTimer {};

    Database::ServiceValueIterator m_ServiceIterator;
    uint_least8_t                  AntCapVal     {0};


    union Blub {
      ~Blub();

      Si4688FirmwareManager          Firmware;
      Si4688DABManager               DAB;
      Si4688FMManager                FM;
      Si4688StartUpdateFlashManager  StartUpdateFlash;
      Si4688UpdateFlashManager       UpdateFlash;
    } OpMgr = { .Firmware = { Si468xOperationMode::Disabled } };

    Si468xActionsContainer           Actions;

    Si4688Transaction   tSiCmd;

    void enqueue(System::Job::HandlerType func)
    {
      PollJob.setHandler(func);
      JobAble::enqueue(PollJob);
    }

    void pollAsync()
    {
      JobAble::enqueue(PollJob);
    }

    template<typename T>
    void startTimeout(T timeout)
    {
      TimeoutTimer.start(timeout, TimeoutJob);
    }

    /** getters for casted container */
    Si4688ManagerBase       & getMgr()       { return OpMgr.Firmware; }

    template<typename T, typename ...ARGS>
    void changeMgr(ARGS ...args)
    {
      getMgr().~Si4688ManagerBase();
      new (&OpMgr) T(args...);
      changedMgr();
    }

    void changeMgr(Si468xOperationMode mode);

    void notifyStatus(uint_fast8_t last_command, const Si4688Status& status);
    void notifyOperationMode(Si468xOperationMode Mode);

    static Si4688Driver & getDriver(Si4688Transaction& ref)
    {
      return container_of(Si4688Driver, tSiCmd, ref);
    }

    static Si4688Driver & getDriver(Si4688ManagerBase& ref)
    {
      return container_of(Si4688Driver, OpMgr, ref);
    }

  public:
    void start() { pollAsync();}
    void seek(bool forward);

    void activateMode(Si468xOperationMode opmode, bool force = false);
    void updateFirmware(Si468xOperationMode opmode, ElmChanFile & file);

    Si468xOperationMode getMode()       const {return getMgr().getOpMode(); }

    constexpr const Database::ServiceValue    & getCurrentService()         const {return *m_ServiceIterator;}
    constexpr const Database::ServiceIterator & getCurrentServiceIterator() const {return m_ServiceIterator;}

    constexpr bool isCurrentService(const Database::ServiceIterator &it) const { return it == m_ServiceIterator;}

    void notifyTwiFinished(bool success);

    void tuneChannel(Database::ServiceIterator it,              Si468xOperationMode opmode = Si468xOperationMode::Booting);
    void tuneChannel(const Database::ServiceValueIterator & it, Si468xOperationMode opmode = Si468xOperationMode::Booting);

    void handleRDSFrame(const RDSFrame &Frame);

    uint8_t getAntCapValue() const {return AntCapVal; }
    void              setAntCap(uint8_t val);


    template<typename T>
    Si468xActionWrapper<T> getAction() { return Si468xActionWrapper<T>(Actions); }

  private:
    void tuneChannel(Si468xOperationMode opmode);

    friend class Si4688Transaction;
    friend class Si4688RadioManager;
    friend class Si4688FMManager;
    friend class Si4688DABManager;
    friend class Si4688FirmwareManager;
    friend class Si4688StartUpdateFlashManager;
    friend class Si4688UpdateFlashManager;
  };
}

#endif