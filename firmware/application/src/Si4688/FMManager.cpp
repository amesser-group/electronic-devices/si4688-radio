/*
 *  Copyright 2016-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Si4688/Driver.hpp"
#include "target_Globals.hpp"

using namespace target;
using namespace MusicBox;
using namespace MusicBox::Database;

const uint16_t Si4688FMManager::s_BootupProperties[] =
{
  /* For FM VHFSW should be open since we're using the "Appendix C
   * Application circuit for EMI mitigation. See AN649, AN650. Since
   * this is the default, we can leave it as is
   * PROP_FM_TUNE_FE_CFG,                          0x0000, */
  PROP_FM_RDS_CONFIG,                 0x00A1,
  0xFFFF
};


void Si4688FMManager::handleFMBoot()
{
  if(m_State == StateType::STATE_BOOTED)
  {
    m_Offset = 0;
    m_State = StateType::STATE_W_SET_PROPERTIES;
  }

  if(m_State == StateType::STATE_W_SET_PROPERTIES)
  {
    auto & cmd = Si4688Driver::getDriver(*this).tSiCmd;

    if(s_BootupProperties[m_Offset] != 0xFFFF)
    {
      m_Offset += 2;
      cmd.setProperty(s_BootupProperties[m_Offset-2], s_BootupProperties[m_Offset-1]);
    }
    else
    {
      m_State = StateType::STATE_READY;
    }
  }

  if(m_State == StateType::STATE_READY)
  {
    handleAction();
  }
}

void Si4688FMManager::handleAction()
{
  auto & drv = Si4688Driver::getDriver(*this);
  auto & cmd = drv.tSiCmd;

  if(m_State < StateType::STATE_READY)
  {
    return;
  }

  if(!cmd.isReady())
    return;


  if( (RequestedAction == Si468xAction::SeekForward) ||
      (RequestedAction == Si468xAction::SeekBackWard))
  {
    if(m_State != StateType::STATE_SEEKING)
    {
      m_State = StateType::STATE_SEEKING;

      cmd.setField16(0, CMD_FM_SEEK_START);

      if (RequestedAction == Si468xAction::SeekForward)
        cmd.setField16(2, MSK_WRAP | MSK_SEEKUP);
      else
        cmd.setField16(2, MSK_WRAP);

      cmd.setField16(4, drv.AntCapVal);
      cmd.sendCommand(6);

      RequestedAction = Si468xAction::None;
      resetState();
    }
  }
  else if( (RequestedAction == Si468xAction::Tune ) ||
           (RequestedAction == Si468xAction::Retune ))
  {
    if(m_State != StateType::STATE_SEEKING)
    {
      auto & v = Si4688Driver::getDriver(*this).getCurrentService();

      if(v.FMFreq != 0)
      {
        RequestedAction = Si468xAction::None;
        m_State = StateType::STATE_SEEKING;

        cmd.setField16(0, CMD_FM_TUNE_FREQ);
        cmd.setField16(2, v.FMFreq);
        cmd.setField16(4, drv.AntCapVal);
        cmd.sendCommand(6);

        resetState();
      }
    }
  }
}

void Si4688FMManager::handleRSQStatusCommand()
{
  auto & drv = Si4688Driver::getDriver(*this);
  auto & cmd = drv.tSiCmd;

  if(!cmd.isReady())
    return;

  if(cmd.getStatus().isErrorBitSet())
    return;

  if(cmd.getLastCommand() != CMD_FM_RSQ_STATUS)
    return;

  if((4+16) > cmd.getReadLen())
  {
    cmd.readResponse(4+16);
    return;
  }

  drv.startTimeout(Milliseconds(200));

  TunedFrequency = cmd.getField16(6) * 10UL;
  // drv.AntCapVal = cmd.getField8(12);

  if(m_State >= StateType::STATE_RUN)
    AverageRssi.sample(Si468xRssiType{cmd.getSigned8(9),1});
  else
    AverageRssi.init(Si468xRssiType{cmd.getSigned8(9),1});

  if(cmd.getStatus().isSeeking())
  { /* seeking */
    changeTunerState(TunerStateType::Unknown);
  }
  else
  {
    if(m_State == StateType::STATE_SEEKING)
      m_State = StateType::STATE_RUN;

    if (cmd.getField8(5) & 0x01)
    { /* tuned */

      if(getTunerState() != TunerStateType::Tuned)
      {
        changeTunerState(TunerStateType::Tuned);
        Globals::getInstance().notifyServiceStarted();
      }

      cmd.setField16(0, CMD_FM_RDS_STATUS);
      cmd.sendCommand(2);
    }
    else
    { /* no signal */
      changeTunerState(TunerStateType::Failed);
    }
  }
}

void Si4688FMManager::handleRDSStatusCommand()
{
  auto & cmd = Si4688Driver::getDriver(*this).tSiCmd;
  uint_fast8_t fifo_level;

  if(!cmd.isReady())
    return;

  if(cmd.getStatus().isErrorBitSet())
    return;

  if(cmd.getLastCommand() != CMD_FM_RDS_STATUS)
    return;

  if(20 > cmd.getReadLen())
  {
    cmd.readResponse(20);
    return;
  }

  fifo_level = cmd.getField8(10);

  if (fifo_level > 0)
  {
    RDSFrame tFrame = {0};

    uint_fast8_t BLE = cmd.getField8(11);

    if((BLE & 0xC0) != 0xC0)
      tFrame.ValidMask |= tFrame.BLOCKA_VALID;
    if((BLE & 0x30) != 0x30)
      tFrame.ValidMask |= tFrame.BLOCKB_VALID;
    if((BLE & 0x0C) != 0x0C)
      tFrame.ValidMask |= tFrame.BLOCKC_VALID;
    if((BLE & 0x03) != 0x03)
      tFrame.ValidMask |= tFrame.BLOCKD_VALID;

    tFrame.BlockA = cmd.getField16(12);
    tFrame.BlockB = cmd.getField16(14);
    tFrame.BlockC = cmd.getField16(16);
    tFrame.BlockD = cmd.getField16(18);

    handleRDSFrame(tFrame);
  }

  if (fifo_level > 1)
  {
    cmd.setField16(0, CMD_FM_RDS_STATUS);
    cmd.sendCommand(2);
  }
}


void Si4688FMManager::handleSeek(uint_fast8_t command, const Si4688Status& status)
{
  auto & cmd = Si4688Driver::getDriver(*this).tSiCmd;

  if(m_State != StateType::STATE_SEEKING)
    return;

  if(cmd.isReady())
  {
    cmd.setField16(0, CMD_FM_RSQ_STATUS);
    cmd.sendCommand(2);
  }
}


void
Si4688FMManager::handleRDSFrame(const RDSFrame &Frame)
{
  auto & g = Globals::getInstance();
  bool notify;

  notify = RdsManager.handleFrame(Frame);

  if(TunerStateType::Tuned == getTunerState())
  {
    auto & drv = Si4688Driver::getDriver(*this);
    const auto & state = RdsManager.getState();

    if (state.isProgramIdentifierValid())
    {
      ServiceValue    val = {};

      val.ServiceId   = state.program_identifier;
      val.FMFreq      = getTunedFreq() / 10;
      val.Name        = state.program_service_name.decode();

      if(g.Db.updateService(val) != drv.m_ServiceIterator.getHandle())
        g.Db.findService(val, drv.m_ServiceIterator);
    }
  }

  if(notify)
    g.statusChanged();
}

void
Si4688FMManager::resetState()
{
  auto & drv = Si4688Driver::getDriver(*this);
  auto & g = Globals::getInstance();

  RdsManager.invalidate();
  drv.m_ServiceIterator = g.Db.endService();
  g.statusChanged();
}


void Si4688FMManager::handleCommandFinished(uint_fast8_t command, const Si4688Status& status)
{
  bool success = !status.isErrorBitSet();

  switch(m_State)
  {
  case StateType::STATE_W_SET_PROPERTIES:
    if(success)
      handleFMBoot();
    else
      m_State = StateType::STATE_ERROR;
    break;
  case StateType::STATE_SEEKING:

    if(command == CMD_FM_RSQ_STATUS)
      handleRSQStatusCommand();

    handleSeek(command,status);
    break;
  case StateType::STATE_RUN:

    if(command == CMD_FM_RSQ_STATUS)
    {
      handleRSQStatusCommand();
    }
    else if(command == CMD_FM_RDS_STATUS)
    {
      handleRDSStatusCommand();
    }
    break;
  }
}

void Si4688FMManager::poll()
{
  handleFMBoot();
}

void Si4688FMManager::timeout()
{
  auto & drv = Si4688Driver::getDriver(*this);

  handleAction();

  if(m_State == StateType::STATE_RUN)
  {
    auto & cmd = drv.tSiCmd;

    if(cmd.isReady())
    {
      cmd.setField16(0, CMD_FM_RSQ_STATUS);
      cmd.sendCommand(2);
    }
    else
    {
      drv.startTimeout(Milliseconds(25));
    }
  }
  else if (m_State >= StateType::STATE_READY)
  {
    drv.startTimeout(Milliseconds(200));
  }

}