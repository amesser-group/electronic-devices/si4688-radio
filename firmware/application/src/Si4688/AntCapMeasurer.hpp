/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SI468X_ANTCAPMEASURER_HPP_
#define SI468X_ANTCAPMEASURER_HPP_

#include "target_System.hpp"
#include "database.hpp"
#include "Si4688/Common.hpp"
#include "FileSystem.hpp"

namespace MusicBox
{
  using namespace target;
  using ::ecpp::Units::Milliseconds;

  class Si468xAntCapMeasurer : public Si468xActionPerformer
  {
  public:
    constexpr Si468xAntCapMeasurer() : Si468xActionPerformer() {}

    void startMeasurement();
    void stopMeasurement();

    bool isMeasuring() const { return File.isOpen(); }

  private:
    static void timeout_job(System::Job & job);

    void nextService();
    void tuneNextService();
    void timeout();

    static constexpr Milliseconds<>      SettlePeriod{5000};

    Database::ServiceIterator    ServiceIt  { Database::ServiceIteratorREnd() };
    System::Timer                Timer      {};
    System::Job                  TimeoutJob { timeout_job };

    ElmChanFile                  File;
    uint_least8_t                AntCapVal {0};
    Si468xOperationMode          Mode      {Si468xOperationMode::Disabled};
  };
}

#endif