/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SI4688_UPDATEFLASHMANAGER_HPP_
#define SI4688_UPDATEFLASHMANAGER_HPP_

#include "ecpp/Units/Fraction.hpp"
#include "Si4688/Common.hpp"
#include "FileSystem.hpp"

namespace MusicBox
{
  class Si4688StartUpdateFlashManager : public Si4688FirmwareManager
  {
  public:
    Si4688StartUpdateFlashManager(ElmChanFile & file, uint32_t offset);

    virtual void handleCommandFinished(uint_fast8_t command, const Si4688Status& status) override;
  private:
    ElmChanFile                       File;
    uint_least32_t                    FlashOffset;
  };

  class Si4688UpdateFlashManager : public Si4688ManagerBase
  {
  public:
    typedef ::ecpp::Units::Fraction<uint32_t> ProgressType;

    Si4688UpdateFlashManager(ElmChanFile & file, uint32_t offset);

    virtual void handleCommandFinished(uint_fast8_t command, const Si4688Status& status) override;
    virtual void poll()    override;
    virtual void timeout() override;

    ProgressType getProgress() const { return {File.tell(), File.getSize()}; }

  private:
    void writeNextBlock();

    ElmChanFile                       File;
    uint_least32_t                    FlashOffset;
    SI4688_SUBCMD_FLASH_PASS_THROUGH  LastSubCmd  { SI4688_SUBCMD_FLASH_PASS_THROUGH::NONE };
  };
}

#endif