/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Si4688/AntCapMeasurer.hpp"
#include "target_Globals.hpp"
#include "target_Bsp.hpp"

using namespace target;
using namespace MusicBox;
using namespace MusicBox::Database;

void
Si468xAntCapMeasurer::startMeasurement()
{
  Globals & g = Globals::getInstance();

  g.VolumeManager.mountVolume();
  File = ElmChanFile("/antcap.dat", FA_WRITE | FA_CREATE_ALWAYS);
  g.VolumeManager.unmountVolume();

  if(File.isOpen())
  {
    ServiceIt = g.Db.beginService();
    AntCapVal = 1;

    if(ServiceIt.hasValue())
    {
      ServiceValueIterator it{ServiceIt};

      g.Storage.setSupressSaves(true);

      if(it->isDabService())
        Mode = Si468xOperationMode::DABRadio;
      else
        Mode = Si468xOperationMode::FMRadio;

      tuneNextService();
    }
    else
    {
      File.close();
    }
  }
  else
  {
    ServiceIt = g.Db.rendService();
  }
}

void
Si468xAntCapMeasurer::stopMeasurement()
{
  Globals & g = Globals::getInstance();

  File.close();

  g.Storage.setSupressSaves(false);
  g.SiHandler.setAntCap(0);
}


void
Si468xAntCapMeasurer::timeout_job(System::Job & job)
{
  container_of(Si468xAntCapMeasurer, TimeoutJob, job).timeout();
}

void
Si468xAntCapMeasurer::nextService()
{
  auto & db = Globals::getInstance().Db;
  ServiceValueIterator it, jt;

  /* check if current service has fm mode
   * and we did not yet scanned it */
  it = ServiceIt;
  if(Mode == Si468xOperationMode::DABRadio)
  {
    if(it.hasValue())
    {
      if(it->isFMService())
      {
        Mode = Si468xOperationMode::FMRadio;
        return;
      }
    }
  }

  while( it != db.endService())
  {
    ServiceValueIterator jt = it;
    ++it;

    if(it != db.endService())
    {
      /* check if we already had this dab channel scanned
       * = Same frequency */
      if (it->isDabService())
      {
        for(;jt != db.rendService(); --jt)
        {
          if(jt->DabChannel == it->DabChannel)
            break;
        }

        if(jt == db.rendService())
        {
          Mode = Si468xOperationMode::DABRadio;
          break;
        }
      }

      if (it->isFMService())
      {
        Mode = Si468xOperationMode::FMRadio;
        break;
      }
    }
  }

  ServiceIt = it;
}

void
Si468xAntCapMeasurer::tuneNextService()
{
  auto & s = Globals::getInstance().SiHandler;
  char linebuf[60] = {};
  int l;

  sniprintf(linebuf, sizeof(linebuf), "# Service %u %u\n",
            ServiceIt.getHandle(), (unsigned)Mode);

  l = strlen(linebuf);
  if(l != File.write(linebuf, l))
  {
    stopMeasurement();
    return;
  }

  s.tuneChannel(ServiceIt, Mode);
  s.setAntCap(AntCapVal);

  Timer.start(SettlePeriod, TimeoutJob);
}


void
Si468xAntCapMeasurer::timeout()
{
  auto & s = Globals::getInstance().SiHandler;

  if(!File.isOpen())
    return;

  /* save last value */
  if( (s.getOpMode() == Si468xOperationMode::DABRadio) ||
      (s.getOpMode() == Si468xOperationMode::FMRadio) )
  {
    auto const & r = s.getRadioManager();
    char linebuf[60] = {};
    int l;

    sniprintf(linebuf, sizeof(linebuf), "%lu;%u;%ld\n",
              r.getTunedFreq(), s.getAntCapValue(),
              ::ecpp::FixedPoint<int32_t, 1000>(r.getRssi()).get_raw());

    l = strlen(linebuf);
    if(l != File.write(linebuf, l))
    {
      stopMeasurement();
      return;
    }
  }

  if(AntCapVal < 128)
  {
    AntCapVal++;
    s.setAntCap(AntCapVal);

    Timer.start(SettlePeriod, TimeoutJob);
  }
  else
  {
    auto & db = Globals::getInstance().Db;
    const char sep[] = "\n\n";

    if(2 !=  File.write(sep, 2))
    {
      stopMeasurement();
    }
    else
    {
      AntCapVal = 1;
      nextService();

      if(ServiceIt == db.endService())
        stopMeasurement();
      else
        tuneNextService();
    }
  }
}





