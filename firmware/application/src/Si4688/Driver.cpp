/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Si4688/Driver.hpp"
#include "target_Bsp.hpp"
#include "target_Globals.hpp"

using namespace target;
using namespace MusicBox;

Si4688Driver::~Si4688Driver()
{

}

Si4688Driver::Blub::~Blub()
{

}

void
Si4688Driver::seek(bool forward)
{
  if( getMgr().getOpMode() != Si468xOperationMode::FMRadio)
    return;

  if(forward)
    OpMgr.FM.seekForward();
  else
    OpMgr.FM.seekBackward();
}

void
Si4688Driver::activateMode(Si468xOperationMode opmode, bool force)
{
  if( force || (getMgr().getOpMode() != opmode))
  {
    getMgr().~Si4688ManagerBase();
    new (&OpMgr.Firmware) Si4688FirmwareManager(opmode);

    pollAsync();
  }
}

void
Si4688Driver::updateFirmware(Si468xOperationMode opmode, ElmChanFile & file)
{
  switch(opmode)
  {
  case Si468xOperationMode::FMRadio:
    changeMgr<Si4688StartUpdateFlashManager>(file, Si4688ManagerBase::FlashAddrFMFirmware);
    break;
  case Si468xOperationMode::DABRadio:
    changeMgr<Si4688StartUpdateFlashManager>(file, Si4688ManagerBase::FlashAddrDABFirmware);
    break;
  }
}

void
Si4688Driver::notifyStatus(uint_fast8_t last_command, const Si4688Status& status)
{
  getMgr().handleCommandFinished(last_command, status);
}

void
Si4688Driver::changeMgr(Si468xOperationMode mode)
{
  switch(mode)
  {
  case Si468xOperationMode::DABRadio:
    changeMgr<Si4688DABManager>();
    break;
  case Si468xOperationMode::FMRadio:
    changeMgr<Si4688FMManager>();
    break;
  default:
    changeMgr<Si4688FirmwareManager>(Si468xOperationMode::Disabled);
    break;
  }
}

void
Si4688Driver::changedMgr()
{
  Globals::Notify(*this, Si468xEvent::OpmodeChanged(getMgr().getOpMode()));
  pollAsync();
}


void
Si4688Driver::notifyTwiFinished(bool success)
{
  tSiCmd.handleTwiFinished(success);
}


void
Si4688Driver::tuneChannel(Database::ServiceIterator it, Si468xOperationMode opmode)
{
  if( !it.hasValue())
    return;

  m_ServiceIterator = it;
  tuneChannel(opmode);
}

void
Si4688Driver::tuneChannel(const Database::ServiceValueIterator & it, Si468xOperationMode opmode)
{
  m_ServiceIterator = it;
  tuneChannel(opmode);
}

void
Si4688Driver::tuneChannel(Si468xOperationMode opmode)
{
  if(opmode == Si468xOperationMode::FMRadio)
  { /* fm forced */
    if(!m_ServiceIterator->isFMService())
      opmode = Si468xOperationMode::Disabled;
  }
  else if (opmode == Si468xOperationMode::DABRadio)
  { /* dab forced */
    if(!m_ServiceIterator->isDabService())
      opmode = Si468xOperationMode::Disabled;
  }
  else
  { /* choose best */
    opmode = Si468xOperationMode::Disabled;

    if(m_ServiceIterator->isFMService())
      opmode = Si468xOperationMode::FMRadio;
    else if (m_ServiceIterator->DabChannel.isValid())
      opmode = Si468xOperationMode::DABRadio;

    if(m_ServiceIterator->isDabService())
      opmode = Si468xOperationMode::DABRadio;
  }

  if(getMgr().getOpMode() != opmode)
    activateMode(opmode);
  else if (opmode == Si468xOperationMode::FMRadio)
    getFMManager().tune();
  else if (opmode == Si468xOperationMode::DABRadio)
    getDABManager().tune();
}

void
Si4688Driver::setAntCap(uint8_t value)
{
  if(value > 128)
    return;

  AntCapVal = value;

  if ( (getOpMode() == Si468xOperationMode::DABRadio) ||
       (getOpMode() == Si468xOperationMode::FMRadio))
    OpMgr.DAB.tune(true);
}

void
Si4688Driver::poll_job(System::Job &job)
{
  auto & self = container_of(Si4688Driver, PollJob, job);
  self.getMgr().poll();
}

void
Si4688Driver::timeout_job(System::Job &job)
{
  auto & self = container_of(Si4688Driver, TimeoutJob, job);
  self.getMgr().timeout();
}