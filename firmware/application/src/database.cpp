/*
 *  Copyright 2018-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "database.hpp"

#include <cstring>
#include "target_Globals.hpp"

namespace MusicBox::Database
{
  using namespace target;

  /* DAB                              FM


   * -------------------------------+--------------
   * Ensemble                         Channel
   * - DAB Service <-> Radio Sender   - FM Service
   *   - Component "Audio Stream"
   *   - Component "TMC Stream"
   * - DAB Service n...
   *
   *
   *   */

  Database::Database::ServiceHandleType    Database::Database::_handle_begin  = Database::Database::_handle_end;
  Database::Database::ServiceHandleType    Database::Database::_handle_rbegin = Database::Database::_handle_rend;
  Database::Database::ServiceContainer     Database::Database::_services[_max_services] = {{0}};


  bool ServiceValue::update(const ServiceValue & rhs)
  {
    bool updated = false;

    if( rhs.ServiceId == 0)
      return false;

    if( rhs.ServiceId != ServiceId )
    {
      updated = true;
      ServiceId = rhs.ServiceId;
    }

    if( rhs.isFMService() && (rhs.FMFreq != FMFreq))
    {
      updated = true;
      FMFreq = rhs.FMFreq;
    }

    if( rhs.isDabService() )
    {
      if((rhs.DabChannel != DabChannel) ||
         (rhs.CId        != CId) ||
         (rhs.Name       != Name))
        updated = true;

      DabChannel = rhs.DabChannel;
      CId        = rhs.CId;
      Name       = rhs.Name;
    }

    /* Update name from FM service only if this is
     * a non-dab service */
    if(!isDabService() && rhs.isFMService())
      Name = rhs.Name;

    return updated;
  }

  void Database::stepService(ServiceIterator &it, int steps)
  {
    /* backward stepping */
    while (steps < 0)
    {
      if(it._RecordHandle == _handle_rend)
        break;
      else if(it._RecordHandle == _handle_end)
        it._RecordHandle = _handle_rbegin;
      else
        it._RecordHandle = getServiceContainer(it._RecordHandle)._handlePrev;

      steps++;
    }

    /* forward stepping */
    while (steps > 0)
    {
      if(it._RecordHandle == _handle_end)
        break;
      else if(it._RecordHandle == _handle_rend)
        it._RecordHandle = _handle_begin;
      else
        it._RecordHandle = getServiceContainer(it._RecordHandle)._handleNext;

      steps--;
    }
  }

  ServiceIterator
  Database::beginService()
  {
    return getServiceIterator(_handle_begin);
  }

  ServiceIterator Database::getServiceIterator(ServiceHandleType handle)
  {
    ServiceIterator it = {0};
    it._RecordHandle = handle;
    return it;
  }

  Database::ServiceHandleType
  Database::findService(const ServiceValue &value, ServiceValueIterator &it)
  {
    for(it = beginService(); it != endService(); ++it)
    {
      if(it->ServiceId == value.ServiceId)
        break; /* exact match by service id */

      if((value.FMFreq != 0) &&
         (it->FMFreq == value.FMFreq))
        break; /* match by fm frequency */
    }

    return it.getHandle();
  }

  /** Update service information within the database.
   *
   * @return handle of updated service
   */
  Database::ServiceHandleType
  Database::updateService(const ServiceValue &value, ServiceHandleType handle)
  {
    if(!(value.isDabService() || value.isFMService()))
      return _handle_end;

    /* find matching service if not provided */
    if (handle == _handle_end)
    {
      ServiceValueIterator it;

      for(it = beginService(); it != endService(); ++it)
      {
        if(it->ServiceId == value.ServiceId)
          break; /* exact match by service id */

        if((value.FMFreq != 0) &&
           (it->FMFreq == value.FMFreq))
          break; /* match by fm frequency */
      }


      if(it != endService())
      { /* found a matching service */
        handle = it.getHandle();
      }
      else
      { /* found no matching service, locate first unused
         * service entry */
        for(handle = 1; handle < _handle_end; ++handle)
        {
          if(getServiceContainer(handle)._value.isUnused())
            break;
        }
      }
    }

    /* when existing or empty entry found update it if required
     * and link service if needed */
    if(handle != _handle_end)
    {
      auto & db_value = _services[handle - 1]._value;

      if (db_value.update(value))
      {
        linkService(handle);
        Globals::databaseChanged(getServiceIterator(handle));
      }
    }

    return handle;
  }

  void
  Database::clearDABProperties(ServiceHandleType handle)
  {
    if(isServiceHandle(handle))
    {
      ServiceContainer & c = getServiceContainer(handle);
      c._value.DabChannel = {};
      c._value.CId        =  0;
    }
  }

  Database::HandleIteratorType
  Database::linkService(ServiceHandleType handle)
  {
    if(!isServiceHandle(handle))
      return _handle_end;

    ServiceContainer & c = getServiceContainer(handle);

    /* check if already linked */
    if ( (c._handleNext != _handle_rend) ||
         (c._handlePrev != _handle_rend))
      return handle;

    /* insert at start of list */
    c._handleNext = _handle_begin;
    c._handlePrev = _handle_rend;

    /* insert into list */

    /* update successor */
    if(_handle_begin == _handle_end)
      _handle_rbegin = handle; /* set reverse begin */
    else
      getServiceContainer(_handle_begin)._handlePrev = handle; /* reverse-link current list-head*/

    /* update precedessor */
    /* set new list start point */
    _handle_begin = handle;

    return HandleIteratorType(handle);
  }

  void
  Database::unlinkService(ServiceHandleType handle)
  {
    if(!isServiceHandle(handle))
      return;

    ServiceContainer & c = getServiceContainer(handle);

    /* check if already unlinked */
    if ( (c._handleNext == _handle_rend) &&
         (c._handlePrev == _handle_rend))
      return;

    /* update precedessor */
    if(_handle_rend == c._handlePrev)
      _handle_begin = c._handleNext;
    else
      getServiceContainer(c._handlePrev)._handleNext = c._handleNext;

    /* update successor */
    if(_handle_end == c._handleNext)
      _handle_rbegin = c._handlePrev;
    else
      getServiceContainer(c._handleNext)._handlePrev = c._handlePrev;

    /* empty container (implicitly marks it as unused) */
    memset(&c, 0x00, sizeof(c));
  }

  void
  ServiceIterator::loadValue(ServiceValue &value) const
  {
    Database::getServiceValue(_RecordHandle, value);
  }

  void
  Database::getServiceValue(Database::ServiceHandleType handle, ServiceValue &value)
  {
    auto & db = Globals::getInstance().Db;

    if(handle > 0 && handle <= db._max_services)
      memcpy(&value, &(db.getServiceContainer(handle)._value), sizeof(value));
    else
      value = ServiceValue();
  }
}