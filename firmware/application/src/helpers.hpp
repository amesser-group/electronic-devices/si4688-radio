/*
 * helper.hpp
 *
 *  Created on: 27.04.2017
 *      Author: andi
 */

#ifndef FIRMWARE_APP_SRC_HELPERS_HPP_
#define FIRMWARE_APP_SRC_HELPERS_HPP_

#include <cstdint>
#include <cstdlib>

#define my_offset_of(Type,Field)  ((size_t)&(((Type*)0)->Field))

template<class C, class F>
C & container_of_(F &field, size_t offset)
{
  return *reinterpret_cast<C*>(reinterpret_cast<uint8_t*>(&field) - offset);
}

#define container_of(Type, Field, Reference) container_of_<Type>(Reference, my_offset_of(Type, Field))



#endif /* FIRMWARE_APP_SRC_HELPERS_HPP_ */
