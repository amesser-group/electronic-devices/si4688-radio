/*
 *  Copyright 2017-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MAX98089_HPP_
#define MAX98089_HPP_

#include "target_System.hpp"
#include "helpers.hpp"
#include "initializer_list"
#include "array"

using namespace target;

#define EQ_PARAM(val) ((val) >> 8) & 0xFF, (val) &0xFF

class Max98089Defs
{
public:
  enum : uint8_t
  {
    REG_BATTERY_VOLTAGE                          = 0x03,
    REG_MASTER_CLOCK                             = 0x10,
    REG_DAI1_CLOCK_MODE                          = 0x11,
    REG_DAI1_ANY_CLOCK_CONTROL1                  = 0x12,
    REG_DAI1_ANY_CLOCK_CONTROL2                  = 0x13,
    REG_DAI1_FORMAT                              = 0x14,
    REG_DAI1_CLOCK                               = 0x15,
    REG_DAI1_IO_CONFIGURATION                    = 0x16,
    REG_DAI1_TIME_DIVISION_MULTIPLEX             = 0x17,
    REG_DAI1_FILTERS                             = 0x18,
    REG_DAC_MIXER                                = 0x22,
    REG_LEFT_RECEIVER_AMPLIFIER_MIXER            = 0x28,
    REG_RIGHT_RECEIVER_AMPLIFIER_MIXER           = 0x29,
    REG_RECEIVER_AMPLIFIER_MIXER_CONTROL         = 0x2A,
    REG_LEFT_SPEAKER_OUTPUT_MIXER_REGISTER       = 0x2B,
    REG_RIGHT_SPEAKER_OUTPUT_MIXER_REGISTER      = 0x2C,
    REG_DAI1_PLAYBACK_LEVEL_CONTROL              = 0x2F,

    REG_LEFT_RECEIVER_AMPLIFIER_VOLUME_CONTROL   = 0x3B,
    REG_RIGHT_RECEIVER_AMPLIFIER_VOLUME_CONTROL  = 0x3C,
    REG_LEFT_SPEAKER_OUTPUT_LEVEL_REGISTER       = 0x3D,
    REG_RIGHT_SPEAKER_OUTPUT_LEVEL_REGISTER      = 0x3E,

    REG_LEVEL_CONTROL                            = 0x49,
    REG_OUTPUT_ENABLE                            = 0x4D,
    REG_POWER_MANAGEMENT_TOPLEVEL_BIAS_CONTROL   = 0x4E,
    REG_DAC_LOW_POWER_MODE2                      = 0x50,
    REG_SYSTEM_SHUTDOWN                          = 0x51,

    REG_EQ1_BAND1                                = 0x52,
  };
};

struct Max98089RegisterSequence
{
  const uint8_t *sequence_;
  uint_least8_t  len_;

  template<size_t N>
  constexpr Max98089RegisterSequence(const uint8_t (&arr)[N]) : sequence_(arr), len_(N) {}
};

class Max98089Cmd {};

class Max98089Manager;

class Max98089ManagerState
{
protected:
  virtual void handlePendingActions();
  virtual void handleTwiTransferDone();

  friend Max98089Manager;
};

class Max98089ManagerStateWriteRegs : public Max98089ManagerState
{
protected:
  void writeRegister(uint8_t reg, uint8_t val);
private:
  uint_least8_t data[2] {};
};

class Max98089ManagerStateSetup : public Max98089ManagerStateWriteRegs
{
protected:
  virtual void handlePendingActions()  override;
  virtual void handleTwiTransferDone() override;

  uint_least8_t seq_index_  {0};
  uint_least8_t seq_offset_ {0};
};

class Max98089ManagerStateRun : public Max98089ManagerState
{
public:
  Max98089ManagerStateRun(bool shutdown);

protected:
  virtual void handlePendingActions() override;
  friend class Max98089Manager;
};

class Max98089ManagerStateSingleTwiTransfer : public Max98089ManagerState
{
protected:
  virtual void handleTwiTransferDone() override;
};

class Max98089ManagerStateSetVol : public Max98089ManagerStateSingleTwiTransfer
{

protected:
  virtual void handlePendingActions()  override;
  uint8_t      buffer[5];
};

#if 0

class Max98089ManagerStateWriteDataContainer : public Max98089ManagerState
{
public:
  constexpr Max98089ManagerStateWriteDataContainer(const uint8_t *data, uint16_t length) : Max98089ManagerState(), data {data}, length {length} {}
protected:
  const uint8_t * const data;
  const uint_least16_t  length;
};

class Max98089ManagerStateWriteDataStart : public Max98089ManagerStateWriteDataContainer
{
public:
  constexpr Max98089ManagerStateWriteDataStart(const uint8_t *data, uint16_t length) : Max98089ManagerStateWriteDataContainer(data,length) {}
protected:
  virtual void handlePendingActions()  override;
  virtual void handleTwiTransferDone() override;
};

class Max98089ManagerStateWriteData : public Max98089ManagerStateWriteDataContainer
{
public:
  constexpr Max98089ManagerStateWriteData(const Max98089ManagerStateWriteDataStart &init) : Max98089ManagerStateWriteDataContainer(init) {}

protected:
  virtual void handlePendingActions()  override;
  virtual void handleTwiTransferDone() override;

  uint8_t               buffer[2] {};
  uint_least16_t        offset {0};
};

#endif

class Max98089ManagerStateActivate : public Max98089ManagerState
{
protected:
  virtual void handlePendingActions()  override;
  virtual void handleTwiTransferDone() override;
};

class Max98089ManagerStateEnterShutdown : public Max98089ManagerState
{
protected:
  virtual void handlePendingActions()  override;
  virtual void handleTwiTransferDone() override;
};

class Max98089Manager : protected JobAble
{
private:


  static Max98089Manager & getManager(Max98089Cmd& ref)
  {
    return container_of(Max98089Manager, tMaxCmd, ref);
  }

public:

  void initDevice();

  void          setVolumeLevel(uint_fast8_t level);
  int_fast16_t  getVolume_cB() const;
  void          changeVolumeLevel(int_fast16_t change);
  uint_fast8_t  getReqVolumeLevel() const {return m_VolLevelReq;}
  uint_fast8_t  getCurVolumeLevel() const {return m_VolLevelCur;}

  void           setShutdown(bool shutdown);
  bool           isShutdown() const;

  void notifyTwiFinished(bool success);
  void handlePending();

  friend class Max98089Cmd;
protected:

  template<typename STATE, typename... ARGS>
  void changeState(ARGS ... args) {
    new (& State) STATE(args...);
    handlePending();
  }

  union {
    Max98089ManagerState                  Default;
    Max98089ManagerStateWriteRegs         WriteRegs;
    Max98089ManagerStateSetup             Setup;
    Max98089ManagerStateRun               Run;
    Max98089ManagerStateSingleTwiTransfer SingleTwiTransfer;
    Max98089ManagerStateSetVol            SetVol;

#if 0
    Max98089ManagerStateWriteDataStart    WriteDataStart;
    Max98089ManagerStateWriteData         WriteData;
#endif

    Max98089ManagerStateActivate          Activate;
    Max98089ManagerStateEnterShutdown     EnterShutdown;
  } State { .Default = {} };

  Max98089ManagerState & getState() {
    return *reinterpret_cast<Max98089ManagerState*>(&State);
  }

  const Max98089ManagerState & getConstState() const {
    return *reinterpret_cast<const Max98089ManagerState*>(&State);
  }

  Max98089Cmd   tMaxCmd;

  int_least8_t   m_VolLevelCur = 0;
  int_least8_t   m_VolLevelReq = 0;
  uint_least8_t  ShutdownReq   {true};
  uint_least8_t  ShutdownCur   {false};

  friend Max98089ManagerState;
  friend Max98089ManagerStateWriteRegs;
  friend Max98089ManagerStateSetup;
  friend Max98089ManagerStateRun;
  friend Max98089ManagerStateSingleTwiTransfer;
  friend Max98089ManagerStateSetVol;
#if 0
  friend Max98089ManagerStateWriteDataStart;
  friend Max98089ManagerStateWriteData;
#endif

  friend Max98089ManagerStateActivate;
  friend Max98089ManagerStateEnterShutdown;

private:

  static void handlePendingActions_job(System::Job &Job);
  System::Job PendingActionsJob {handlePendingActions_job};

  static void handleTwiTransferDone_job(System::Job &Job);
  System::Job TwiTransferDoneJob {handleTwiTransferDone_job};

  void        writeData(void* buf, size_t buflen);

  uint8_t     TwiPending {0};
};





#endif /* FIRMWARE_APP_SRC_MAX98089_HPP_ */
