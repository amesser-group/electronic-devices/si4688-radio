/*
 *  Copyright 2016-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "max98089.hpp"
#include "ecpp/Datatypes.hpp"
#include "target_Globals.hpp"

using namespace ecpp;
using namespace target;

/* must be ram buffers for twi DMA to work */
static uint8_t Max98089ShutdownData[]   = { Max98089Defs::REG_SYSTEM_SHUTDOWN, 0x00};
static uint8_t Max98089UnShutdownData[] = { Max98089Defs::REG_SYSTEM_SHUTDOWN, 0x80};

void
Max98089ManagerState::handlePendingActions()
{
  Max98089Manager & m = container_of(Max98089Manager, State.Run, *this);
  m.changeState<Max98089ManagerStateSetup>();
}

void
Max98089ManagerState::handleTwiTransferDone()
{
}

/** The ATMSAM4s DMA has no access to flash. Thus we must wrap all writes
 *  In a ram buffer */
void
Max98089ManagerStateWriteRegs::writeRegister(uint8_t reg, uint8_t val)
{
  Max98089Manager & m = container_of(Max98089Manager, State.WriteRegs, *this);

  data[0] = reg;
  data[1] = val;

  m.writeData(data, 2);
}

void
Max98089ManagerStateSetup::handlePendingActions()
{
  Max98089Manager & m = container_of(Max98089Manager, State.Setup, *this);
  auto const & sequences = Bsp::instance().max98089_init_registers();

  while( seq_index_ < ElementCount(sequences))
  {
    auto const & s = sequences[seq_index_];

    if((seq_offset_ + 1) < s.len_)
    {
      writeRegister(s.sequence_[0] + seq_offset_, s.sequence_[seq_offset_+1]);
      seq_offset_++;
      break;
    }
    else
    {
      seq_offset_ = 0;
      seq_index_++;
    }
  }

  if( seq_index_ >= ElementCount(sequences))
  { /* parameterization finished */
    if(m.ShutdownReq)
      m.changeState<Max98089ManagerStateEnterShutdown>();
    else
      m.changeState<Max98089ManagerStateActivate>();
  }
}

void
Max98089ManagerStateSetup::handleTwiTransferDone()
{
  handlePendingActions();
}


void
Max98089ManagerStateActivate::handlePendingActions()
{
  Max98089Manager & m = container_of(Max98089Manager, State.Activate, *this);
   m.writeData(Max98089UnShutdownData, ElementCount(Max98089UnShutdownData));
};

void
Max98089ManagerStateActivate::handleTwiTransferDone()
{
  Max98089Manager & m = container_of(Max98089Manager, State.Activate, *this);

  m.changeState<Max98089ManagerStateRun>(false);
  Globals::statusChanged();
}

void
Max98089ManagerStateEnterShutdown::handlePendingActions()
{
  Max98089Manager & m = container_of(Max98089Manager, State.EnterShutdown, *this);
  m.writeData(Max98089ShutdownData, ElementCount(Max98089ShutdownData));
};

void
Max98089ManagerStateEnterShutdown::handleTwiTransferDone()
{
  Max98089Manager & m = container_of(Max98089Manager, State.EnterShutdown, *this);
  m.changeState<Max98089ManagerStateRun>(true);
  Globals::statusChanged();
}



Max98089ManagerStateRun::Max98089ManagerStateRun(bool shutdown) :
  Max98089ManagerState()
{
  Max98089Manager & m = container_of(Max98089Manager, State.Run, *this);
  m.ShutdownCur = shutdown;
}

void
Max98089ManagerStateRun::handlePendingActions()
{
  Max98089Manager & m = container_of(Max98089Manager, State.Run, *this);

  if(m.ShutdownReq)
  {
    if(m.ShutdownCur != m.ShutdownReq)
      m.changeState<Max98089ManagerStateEnterShutdown>();
  }
  else if( (m.m_VolLevelReq != m.m_VolLevelCur) ||
           (m.ShutdownCur != m.ShutdownReq) )
  { /* Enable max98089 always through setting volume first */
    m.changeState<Max98089ManagerStateSetVol>();
  }
}

void
Max98089ManagerStateSingleTwiTransfer::handleTwiTransferDone()
{
  Max98089Manager & m = container_of(Max98089Manager, State.SingleTwiTransfer, *this);

  if(m.ShutdownReq)
    m.changeState<Max98089ManagerStateEnterShutdown>();
  else
    m.changeState<Max98089ManagerStateActivate>();
}

void
Max98089ManagerStateSetVol::handlePendingActions()
{
  Max98089Manager & m = container_of(Max98089Manager, State.SetVol, *this);
  uint_least8_t level;

  level = m.m_VolLevelReq;
  m.m_VolLevelCur = level;

  /* start register */
  buffer[0] = Max98089Defs::REG_LEFT_RECEIVER_AMPLIFIER_VOLUME_CONTROL;
  buffer[1] = level;
  buffer[2] = level;
  buffer[3] = level;
  buffer[4] = level;

  m.writeData(buffer, 5);
}

#if 0
void
Max98089ManagerStateWriteDataStart::handlePendingActions()
{
  Max98089Manager & m = container_of(Max98089Manager, State.WriteDataStart, *this);
  m.writeData(Max98089ShutdownData, ElementCount(Max98089ShutdownData));
};

void
Max98089ManagerStateWriteDataStart::handleTwiTransferDone()
{
  Max98089Manager & m = container_of(Max98089Manager, State.WriteDataStart, *this);
  m.changeState<Max98089ManagerStateWriteData>(*this);
}

void
Max98089ManagerStateWriteData::handlePendingActions()
{
  Max98089Manager & m = container_of(Max98089Manager, State.WriteData, *this);

  if ((offset + 1U) < length)
  {
    buffer[0] = data[0] + offset;
    buffer[1] = data[offset + 1];

    m.writeData(buffer, 2);
    offset++;
  }
  else
  {
    if(m.ShutdownReq)
      m.changeState<Max98089ManagerStateEnterShutdown>();
    else
      m.changeState<Max98089ManagerStateActivate>();
  }
};

void
Max98089ManagerStateWriteData::handleTwiTransferDone()
{
  handlePendingActions();
};
#endif

void
Max98089Manager::handleTwiTransferDone_job(System::Job &Job)
{
  Max98089Manager & m = container_of(Max98089Manager, TwiTransferDoneJob, Job);

  m.TwiPending = false;
  m.getState().handleTwiTransferDone();
}


void
Max98089Manager::handlePendingActions_job(System::Job &Job)
{
  Max98089Manager &m = container_of(Max98089Manager, PendingActionsJob, Job);

  /* As long as current state is waiting for a twi transfer,
   * do not invoke pending actions */
  if(m.TwiPending)
    return;

  m.getState().handlePendingActions();
}



void
Max98089Manager::handlePending()
{
  enqueue(PendingActionsJob);
}

void
Max98089Manager::initDevice()
{
  enqueue(PendingActionsJob);
}

bool
Max98089Manager::isShutdown() const
{
  return ShutdownCur;
}

void
Max98089Manager::setVolumeLevel(uint_fast8_t level)
{
  if(level > 0x1F)
    level = 0x1F;

  if(level != static_cast<uint_fast8_t>(m_VolLevelReq))
  {
    m_VolLevelReq = level;
    handlePending();
  }
}

void Max98089Manager::changeVolumeLevel(int_fast16_t change)
{
  int_fast8_t level;

  level = m_VolLevelReq;

  if (change > 0)
  {
    if((0x1F - level) >= change)
      level += change;
    else
      level = 0x1F;
  }
  else if (change < 0)
  {
    if(level >= (-change))
      level += change;
    else
      level = 0;
  }

  setVolumeLevel(level);
}

void
Max98089Manager::setShutdown(bool shutdown)
{
  ShutdownReq = shutdown;
  handlePending();
}


static const int_least16_t s_LevelTocB [] =
{
  [0x00] = (int)(-62 * 10.),
  [0x01] = (int)(-58 * 10.),
  [0x02] = (int)(-54 * 10.),
  [0x03] = (int)(-50 * 10.),
  [0x04] = (int)(-46 * 10.),
  [0x05] = (int)(-42 * 10.),
  [0x06] = (int)(-38 * 10.),
  [0x07] = (int)(-35 * 10.),
  [0x08] = (int)(-32 * 10.),
  [0x09] = (int)(-29 * 10.),
  [0x0A] = (int)(-26 * 10.),
  [0x0B] = (int)(-23 * 10.),
  [0x0C] = (int)(-20 * 10.),
  [0x0D] = (int)(-17 * 10.),
  [0x0E] = (int)(-14 * 10.),
  [0x0F] = (int)(-12 * 10.),
  [0x10] = (int)(-10 * 10.),
  [0x11] = (int)(-8 * 10.),
  [0x12] = (int)(-6 * 10.),
  [0x13] = (int)(-4 * 10.),
  [0x14] = (int)(-2 * 10.),
  [0x15] = (int)(0 * 10.),
  [0x16] = (int)(+1 * 10.),
  [0x17] = (int)(+2 * 10.),
  [0x18] = (int)(+3 * 10.),
  [0x19] = (int)(+4 * 10.),
  [0x1A] = (int)(+5 * 10.),
  [0x1B] = (int)(+6 * 10.),
  [0x1C] = (int)(+6.5 * 10.),
  [0x1D] = (int)(+7 * 10.),
  [0x1E] = (int)(+7.5 * 10.),
  [0x1F] = (int)(+8 * 10.),
};

int_fast16_t
Max98089Manager::getVolume_cB() const
{
  return s_LevelTocB[m_VolLevelCur & 0x1F];
}

void
Max98089Manager::writeData(void* buf, size_t buflen)
{
  auto & twi = Bsp::instance().GetTwiHandler(*this);
  uint8_t *reg = static_cast<uint8_t*>(buf);

  TwiPending = true;

  twi.set_twi_addr(Bsp::kI2CAddr_MAX98089);
  twi.write_req(reg, reg + 1, buflen - 1);
}


void Max98089Manager::notifyTwiFinished(bool success)
{
  enqueue(TwiTransferDoneJob);
}
