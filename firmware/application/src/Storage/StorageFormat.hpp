/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MUSICBOX_STORAGE_BINFORMAT_HPP_
#define MUSICBOX_STORAGE_BINFORMAT_HPP_

#include <cstdint>
#include "Database/DabChannels.hpp"
#include "database.hpp"

namespace MusicBox::Storage
{
  class BinaryStorage
  {
  protected:

    enum class BlockTypes : uint8_t
    {
      Descriptor   = 1,
      Transponders = 2,
      Services     = 3,
    };

    /** Header structure common to all blocks */
    struct BlockHeader
    {
      /** Version of Database */
      uint8_t        Version;
      /** Type of Block */
      BlockTypes     Type;
      /** Length of Blockdata in Bytes */
      uint16_t       Length;
    };

    struct Block
    {
      BlockHeader   Header;

      void encodeHeader(int Version, BlockTypes Type, void *end_ptr)
      {
        uintptr_t start = reinterpret_cast<uintptr_t>(this + 1);
        uintptr_t end   = reinterpret_cast<uintptr_t>(end_ptr);

        Header.Version = Version;
        Header.Type    = Type;
        Header.Length  = end-start;
      }
    };

    struct StorageInfo
    {
      uint8_t        TranspondersBlockOffset;
      /* start offset of first block associated with service information */
      uint8_t        ServicesBlockOffset;
    };

    struct DescriptorBlock : public Block
    {
      void encode(const StorageInfo & info, const ::MusicBox::Database::ServiceIterator current_service);
      bool decode(StorageInfo & info) const;
      bool decode(::MusicBox::Database::ServiceIterator & current_service) const;

      /* start offset of first block associated with transponder */
      uint8_t        TranspondersBlockOffset;
      /* start offset of first block associated with service information */
      uint8_t        ServicesBlockOffset;
      /* handle of currently played service */
      uint8_t        CurrentServiceHandle;
    };

    struct TransponderBlock : public Block
    {
      int encode(int start, const ::MusicBox::Database::DabChannelList & list);
      int decode(int start,       ::MusicBox::Database::DabChannelList & list) const;

      /** Array to store information about 40 transponders */
      uint32_t       Values[40];
    };

    struct ServiceBlock : public Block
    {
      int encode(int start, const ::MusicBox::Database::Database & db);
      int decode(int start,       ::MusicBox::Database::Database & db) const;

      enum Flags :uint8_t
      {
        Preset  = 0x1,
      };

      struct ValueType
      {
        uint16_t     ServiceId;
        uint16_t     CId; /** Si46xx specific component id */
        uint16_t     FMFreq10kHz;
        uint8_t      DabChannelHandle;
        int8_t       Name[16];
        uint8_t      Flags;
      };

      ValueType      Values[(512 - sizeof(Block)) / sizeof(ValueType)];
    };

    union Blocks
    {
      operator uint8_t*()             { return _Padding; }
      operator const uint8_t*() const { return _Padding; }

      Block             Unknown;
      DescriptorBlock   Descriptor;
      TransponderBlock  Transponder;
      ServiceBlock      Service;

      /* ensure this struct has size of 512 byte */
      uint8_t           _Padding[512];
    };
  };

};
#endif