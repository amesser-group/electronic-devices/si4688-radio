/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "StorageFormat.hpp"
#include "ecpp/Datatypes.hpp"

#include <cstring>
#include <cstddef>

using ::std::memcpy;
using namespace ::MusicBox::Storage;
using namespace ::MusicBox::Database;

using ::ecpp::ElementCount;

void
BinaryStorage::DescriptorBlock::encode(const BinaryStorage::StorageInfo &info, ::MusicBox::Database::ServiceIterator current_service)
{
  memset(this, 0xFF, 512);

  TranspondersBlockOffset = info.TranspondersBlockOffset;
  ServicesBlockOffset     = info.ServicesBlockOffset;
  CurrentServiceHandle    = current_service.getHandle();

  encodeHeader(1, BlockTypes::Descriptor, this + 1);
}


bool
BinaryStorage::DescriptorBlock::decode(BinaryStorage::StorageInfo &info) const
{
  if(Header.Type != BlockTypes::Descriptor)
    return false;

  if( (sizeof(Header) + Header.Length) < offsetof(BinaryStorage::DescriptorBlock, CurrentServiceHandle))
    return false;

  info.TranspondersBlockOffset = TranspondersBlockOffset;
  info.ServicesBlockOffset     = ServicesBlockOffset;

  return true;
}

bool
BinaryStorage::DescriptorBlock::decode(::MusicBox::Database::ServiceIterator & current_service) const
{
  if(Header.Type != BlockTypes::Descriptor)
    return false;

  if( (sizeof(Header) + Header.Length) < sizeof(*this))
    return false;

  current_service = CurrentServiceHandle;
  return true;

}


int
BinaryStorage::TransponderBlock::encode(int start, const ::MusicBox::Database::DabChannelList & list)
{
  int i;

  memset(this, 0xFF, 512);

  if(start >= list.MaxTransponders)
    return 0;

  for(i = 0; (i < ElementCount(Values)) && ((i + start) < list.MaxTransponders); ++i)
  {
    Values[i] = (list[i + start].Freq << 8)
              | (list[i + start].Handle.getRaw());
  }

  encodeHeader(1, BlockTypes::Transponders, &(Values[i]));

  return i;
}

int
BinaryStorage::TransponderBlock::decode(int start,     ::MusicBox::Database::DabChannelList & list) const
{
  int i, imax;

  if(start >= list.MaxTransponders)
    return 0;

  if(Header.Version != 1)
    return -1;

  if(Header.Type != BlockTypes::Transponders)
    return -1;

  imax = Header.Length / sizeof(Values[0]);

  if(imax > ElementCount(Values))
    imax = ElementCount(Values);

  for(i = 0; (i < imax) && ((i + start) < list.MaxTransponders); ++i)
  {
    list[i + start].Handle.setRaw(Values[i] & 0xFF);
    list[i + start].Freq = Values[i] >> 8;
  }

  return i;
}

int
BinaryStorage::ServiceBlock::encode(int start, const ::MusicBox::Database::Database & db)
{
  int i;

  memset(this, 0xFF, 512);

  if(start >= db._max_services)
    return 0;

  for(i = 0; (i < ElementCount(Values)) && ((i + start) < db._max_services); ++i)
  {
    auto s = db.getService(i + start + 1);

    Values[i].ServiceId        = s.ServiceId;
    Values[i].CId              = s.CId;
    Values[i].FMFreq10kHz      = s.FMFreq;
    Values[i].DabChannelHandle = s.DabChannel.getRaw();
    memcpy(Values[i].Name, s.Name.data(), sizeof(Values[i].Name));
    Values[i].Flags            = s.bPreset ? Flags::Preset : 0;
  }

  encodeHeader(1, BlockTypes::Services, &(Values[i]));

  return i;
}

int
BinaryStorage::ServiceBlock::decode(int start,       ::MusicBox::Database::Database & db) const
{
  int i, imax;

  if(start >= db._max_services)
    return 0;

  if(Header.Version != 1)
    return -1;

  if(Header.Type != BlockTypes::Services)
    return -1;

  imax = Header.Length / sizeof(Values[0]);

  if(imax > ElementCount(Values))
    imax = ElementCount(Values);

  for(i = 0; (i < imax) && ((i + start) < db._max_services); ++i)
  {
    ::MusicBox::Database::ServiceValue s{};

    s.ServiceId = Values[i].ServiceId;
    s.CId       = Values[i].CId;
    s.FMFreq    = Values[i].FMFreq10kHz;
    s.DabChannel.setRaw(Values[i].DabChannelHandle);
    memcpy(s.Name.data(), Values[i].Name, sizeof(s.Name));

    if (Values[i].Flags & Flags::Preset)
      s.bPreset = true;

    db.updateService(s, i + start + 1);
  }
  return i;
}
