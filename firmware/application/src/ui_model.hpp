
#ifndef UI_MODEL_HPP_
#define UI_MODEL_HPP_

#include <functional>

#define CALL_MEMBER_FN(object,ptrToMember)  ((object).*(ptrToMember))

class UserInterface;

typedef void (UserInterface::*ActionFn)();
typedef void (UserInterface::*BuildFn)();
typedef void (UserInterface::*ScrollFn)(int delta);

#if 0
class Widget
{

};

class MenuItem : public Widget
{
public:
  constexpr unsigned int getHandle() const {return 0;}
};

class StaticMenuItem : public MenuItem
{
public:
  const ActionFn m_ActionFn;
  const char *   m_Title;

  constexpr StaticMenuItem(const char* t, ActionFn a) : MenuItem(), m_ActionFn(a), m_Title(t) {}

  constexpr const char* getTitle(int pos)  const { return m_Title;}
  constexpr ActionFn    getAction(int pos) const { return m_ActionFn;}
  constexpr ScrollFn    getScroll(int pos) const { return nullptr;}
};

class SubMenuItem : public StaticMenuItem
{
public:
  const ScrollFn   m_ScrollFn;

  constexpr SubMenuItem(const char* t, ActionFn a, ScrollFn s) : StaticMenuItem(t,a), m_ScrollFn(s) {}
  constexpr ActionFn getBuild(int pos)  const { return m_ActionFn;}
  constexpr ScrollFn getScroll(int pos) const { return m_ScrollFn;}
};

/** Menuitem to enter wa widget */
class WidgetMenuItem : public StaticMenuItem
{
public:
  constexpr WidgetMenuItem(const char* t, ActionFn a) : StaticMenuItem(t,a) {}
};


class DynamicMenuItem : public MenuItem
{
public:
  const char* getTitle(int pos) const;
};

class UserInterface
{
public:
  void buildMainMenu();
  void scrollMainMenu(int delta);

  void buildPresetMenu();
  void scrollPresetMenu(int delta);

  void buildChannelsMenu();
  void scrollChannelsMenu(int delta);

  void buildFMMenu();
  void scrollFMMenu(int delta);

  void buildFMChannelsMenu();
  void scrollFMChannelsMenu(int delta);

  void enterFMTune();
  void buildFMTune();
  void scrollFMTune(int delta);
  void actionFMTune();

  void buildDABMenu();
  void scrollDABMenu(int delta);

  void buildDABChannelsMenu();
  void scrollDABChannelsMenu(int delta);

  void buildSettingsMenu();
  void scrollSettingsMenu(int delta);

  void selectChannel();

  void buildIdleScreen();
  void scrollIdleScreen(int delta);

  static const SubMenuItem    s_MainMenu;
  static const SubMenuItem    s_PresetMenu;
  static const SubMenuItem    s_ChannelsMenu;

  static const SubMenuItem    s_FMMenu;
  static const SubMenuItem    s_FMChannelsMenu;
  static const WidgetMenuItem s_FMTune;

  static const SubMenuItem s_DABMenu;
  static const SubMenuItem s_DABChannelsMenu;
  static const SubMenuItem s_SettingsMenu;
};

#endif


#endif
