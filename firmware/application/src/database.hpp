/*
 *  Copyright 2018-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MUSICBOX_DATABASE_HPP_
#define MUSICBOX_DATABASE_HPP_

#include "ecpp/ArrayString.hpp"
#include "ecpp/StringEncodings/Unicode.hpp"

#include "Database/DabChannels.hpp"
#include <cstdint>
#include <iterator>
#include <array>

namespace MusicBox::Database
{
  class Database;
  class StorageManager;

  typedef ::ecpp::ArrayString<::ecpp::StringEncodings::Utf8, 16> ServiceName;

  class ServiceValue
  {
  public:
    constexpr ServiceValue() {}
    constexpr ServiceValue(const DabChannelHandle &chan) : DabChannel{chan} {}

    typedef uint_least16_t FMFrequency_10kHz;

    constexpr bool isDabService() const { return DabChannel.isValid() && (ServiceId != 0);}
    constexpr bool isFMService() const  { return (FMFreq > 0);}
    constexpr bool isUnused()    const  { return (0 == ServiceId); }

    bool update(const ServiceValue & rhs);

    /* const ServiceValue & operator= (const ServiceValue & rhs); */

    bool operator== (const ServiceValue & rhs) const;
    bool operator!= (const ServiceValue & rhs) const {return !operator==(rhs);}

    uint_least16_t     ServiceId {0};

    FMFrequency_10kHz  FMFreq    {0};

    DabChannelHandle   DabChannel {};

    /** Si46xx specific component id */
    uint16_t CId                 {0};

    ServiceName     Name         {};

    uint_least8_t  bPreset       {0};
  };

  class ServiceIterator;
  class ServiceValueIterator;
  class ServiceIteratorEnd {};
  class ServiceIteratorREnd {};

  class Database
  {
  public:
    typedef ServiceIterator ConstHandleIteratorType;
    typedef ServiceIterator HandleIteratorType;

    typedef ServiceValueIterator ConstValueIteratorType;
    typedef ServiceValueIterator ValueIteratorType;

    typedef uint_fast8_t ServiceHandleType;

    static constexpr ServiceHandleType     _max_services     = 64;

  private:
    static constexpr ServiceHandleType _handle_rend  = 0;
    static constexpr ServiceHandleType _handle_end   = _max_services + 1;

    /** Moves a service the given amount forward or backward */
    static void stepService(ServiceIterator &it, int steps);

    static void getNextService(ServiceIterator &it) { stepService(it, +1); }
    static void getPrevService(ServiceIterator &it) { stepService(it, -1); };

    class ServiceContainer
    {
    public:
      ServiceHandleType    _handleNext;
      ServiceHandleType    _handlePrev;
      ServiceValue _value;
    };

    /** True when handle is a valid service handle */
    static constexpr bool               isServiceHandle(ServiceHandleType handle) {return (handle >= 1) && (handle <= _max_services);}

    static constexpr ServiceContainer & getServiceContainer(ServiceHandleType handle) {return _services[handle - 1];}

    static ServiceHandleType       _handle_begin;
    static ServiceHandleType       _handle_rbegin;

    static ServiceContainer     _services[_max_services];

    /* link a service in service list */
    HandleIteratorType linkService(ServiceHandleType handle);
    void               unlinkService(ServiceHandleType handle);
  public:
    constexpr Database() {};

    void init();

    ServiceIterator              beginService();

    static constexpr ServiceIteratorEnd    endService()  {return {};}
    static constexpr ServiceIteratorREnd   rendService() {return {};}

    ServiceIterator       getServiceIterator(ServiceHandleType handle);

    void             getTransponder(uint_fast16_t channel, DabChannelValue &arg) const;

    DabChannelList                   & getDabChannelList() { return ChannelList; }

    ServiceHandleType findService(const ServiceValue &value, ServiceValueIterator &it);
    ServiceHandleType updateService(const ServiceValue &value, ServiceHandleType handle = _handle_end);
    void              clearDABProperties(ServiceHandleType handle);
    void              removeService(ServiceHandleType handle) { unlinkService(handle); }


    static void getServiceValue(ServiceHandleType handle, ServiceValue &value);

    ServiceValue getService(ServiceHandleType handle) const
    {
      ServiceValue value;
      getServiceValue(handle,value);
      return value;
    }

  private:
    DabChannelList      ChannelList;

    friend class ServiceIterator;
    friend class ServiceValueIterator;
    friend class StorageManager;

  };

  class ServiceIterator : public std::iterator<std::input_iterator_tag, const Database::ServiceHandleType>
  {
  public:
    constexpr ServiceIterator(Database::ServiceHandleType handle = 0) : _RecordHandle(handle)    {}
    constexpr ServiceIterator(const ServiceIterator & init)     : _RecordHandle(init._RecordHandle)  {}
    constexpr ServiceIterator(const ServiceIteratorEnd & init)  : _RecordHandle(Database::_handle_end)  {}
    constexpr ServiceIterator(const ServiceIteratorREnd & init) : _RecordHandle(Database::_handle_rend)  {}

    const ServiceIterator & operator= (const ServiceIteratorEnd& end)  { _RecordHandle = Database::_handle_end; return *this; }

    constexpr bool operator==(const ServiceIterator& other) const { return _RecordHandle == other._RecordHandle;};
    constexpr bool operator!=(const ServiceIterator& other) const { return _RecordHandle != other._RecordHandle;};

    const iterator& operator++()    { Database::stepService(*this, 1); return *this;}
    const iterator& operator--()    { Database::stepService(*this, -1); return *this;}

    bool operator==(const ServiceIteratorEnd& end) const { return _RecordHandle >= Database::_handle_end;};
    bool operator!=(const ServiceIteratorEnd& end) const { return _RecordHandle <  Database::_handle_end;};

    bool operator==(const ServiceIteratorREnd& end) const { return _RecordHandle <= Database::_handle_rend;};
    bool operator!=(const ServiceIteratorREnd& end) const { return _RecordHandle  > Database::_handle_rend;};


    bool hasValue() const { return (_RecordHandle < Database::_handle_end) && (_RecordHandle > Database::_handle_rend);  }

    constexpr Database::ServiceHandleType getHandle() const {return _RecordHandle;}

    void loadValue(ServiceValue &value) const;

    friend class Database;
  protected:
    Database::ServiceHandleType _RecordHandle;
  };

  class ServiceValueIterator : public ServiceIterator
  {
  public:
    constexpr ServiceValueIterator() : ServiceIterator{ServiceIteratorEnd()} {load();}
    constexpr ServiceValueIterator(Database::ServiceHandleType handle, const ServiceValue & value) : ServiceIterator{handle}, _value{value} {}
    constexpr ServiceValueIterator(const ::MusicBox::Database::DabChannelValue &chan) : ServiceIterator{0}, _value {chan.Handle} {}

    ServiceValueIterator(Database::ServiceHandleType handle) : ServiceIterator{handle} {load();}
    ServiceValueIterator(const ServiceIterator& it)      : ServiceIterator(it), _value{}          { load();}
    ServiceValueIterator(const ServiceValueIterator& it) : ServiceIterator(it), _value{it._value} {}

    constexpr const ServiceValue & operator*() const {return _value;}
    constexpr const ServiceValue*  operator->() const {return &_value;}

    const ServiceValueIterator & operator= (const ServiceValueIterator& it) { ServiceIterator::operator=(it); _value = it._value; return *this; }

    const ServiceValueIterator & operator= (const ServiceIterator& it) { ServiceIterator::operator=(it); return load(); }
    const ServiceValueIterator & operator= (const ServiceIteratorEnd& end)  { ServiceIterator::operator=(end); return load(); }

    const ServiceValueIterator & operator++() { ServiceIterator::operator++(); return load();}
    const ServiceValueIterator & operator--() { ServiceIterator::operator--(); return load();}

  private:
    /**  load the value from database */
    const ServiceValueIterator & load()
    {
      loadValue(_value);
      return *this;
    }

    ServiceValue         _value;
  };
}

#endif
