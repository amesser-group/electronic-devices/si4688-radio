##################################
Using Radio shield with Teensy 3.2
##################################

************
Comissioning
************

There are some steps required to make the board ready for 
use with a teensy:

- you need to connect the IOREF voltage with the Teensy 3.3V 
  supply: Use some solder to connect both pads of JP101 on the
  backside of the shield. (alternative: connect the 3.3V and IOREF 
  pins of Arduino connector with a wire) **Warning: Do not plug
  the shield on an 5V Arduino afterwards, since this will
  short the 3.3V and 5V voltages**
- If you want to use the I2S interface with the Teensy 3.2, solder 
  a 4x0603 10 Ohm resistor array to RN104
- If you want to use the SD-Card interface you have to solder 
  another 4x0603 10 Ohm resistor array to RN101, oriented
  on the leftmost four pads for Teensy 3.2.
  **Warning: revision 1 shield unfortunately  uses a wrong (5x0603) 
  footprint which does not align properly with a 4x0603 array. Try 
  to solder some small wires or two 2x0402  resistor arrays**

.. figure:: shield-r1_teensy3.2.jpg
   
   Preparing for Teensy 3.2

***********
Demo Sketch
***********

A demo sketch is provided in `TeensyRadioConsole <../../../TeensyRadioConsole>`_. This
sketch is controlled via USB Serial interface and accepts
several commands, some with arguments to control the shield.

In addition the sketch implements an USB Sound-Card which
sends all PC sounds to the RadioShields through the Teensy's
I2S interface.

Teensyduino should be configured for 48 MHz and USB with
Serial + Midi + Audio. The sketch is controlled via the 
serial monitor, "CR" line-ending. To get a list of
the commands, type "help"

In order to make a first try with radio the following
command sequence can be used:

1. Boot the Si4688 in FM mode: ``si_fm``
2. Activate the Max98089: ``max_activate``
3. Set volume on speaker: ``vol_spk 10`` or on 
   headphone/lineout ``vol_hp 5`` or even both.
   Range is 0-31. **WARNING: vol_spk >= 25 is fairly
   loud, so start with small value and increase 
   step by step**
4. Let Si4688 seek for a channel: ``seek``. Repeat command
   until a channel is found. (First invocation will just set the
   start value)

**********
Interfaces
**********

I2C
===

The default implementation of the Wire library might not
be suffcient to properly control the SI4688 through
I2C bus. An working example I2C driver implementation is
provided in 

- `TwoWire.h <../../../demo/TeensyRadioConsole/TwoWire.h>`_
- `TeensyTwi.cpp <../../../demo/TeensyRadioConsole/TeensyTwi.cpp>`_

I2S
===

The I2S Output/input provided with Teensy Audio library should not be 
used. The Audio library enables a MCLK pin which is not used
by the Radio shield but conflicts with the SD-Card interface of the
Radio Shield. A custom implementation of I2S output can
be created from the original Teensy Audio library, copying/renaming
the ``output_i2s.cpp`` and ``output_i2s.h`` files into your sketch,
renaming the classes and removing the line which enables the MCLK
clock on Pin 11. An example is provided in

- `RadioShieldI2S.h   <../../../demo/TeensyRadioConsole/RadioShieldI2S.h>`_ 
- `RadioShieldI2S.cpp <../../../demo/TeensyRadioConsole/RadioShieldI2S.cpp>`_ (Relevant change at end of file)


