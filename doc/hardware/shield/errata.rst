***************
Hardware Errata
***************

Revision 1
==========

- A pull down resistor for I2C_EN signal of Si4688 sub-circuit is missing. As 
  a result the gates of Q301 are floating and might unexpectedly 
  raise the RESET signal through resistor divider RN301/R302 above threshold
  such that Si4688 leaves standby mode.
  This can be solved by connecting a 10k Resistor between GND and Pin 4 of 
  PCAL6408. (Most esily done using C105 GND pad)
- Unfortunately the footprint of RN101 does not fit well with a 4x0603 resistor
  array. As a workaround, either solder four thin wires as needed or try to
  use two 2x0402 Convex resistor arrays

