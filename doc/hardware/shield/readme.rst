#######################
Arduino / Teensy Shield
#######################

This folder contains the documentation of the Arduino/Teensy Shield

- `General documentation <shield.rst>`_
- `Teensy 3.2 related info <teensy3.2.rst>`_
- `Errata <errata.rst>`_
- `Schematic <shield-r1_schematic.pdf>`_

