#######################
Arduino / Teensy Shield
#######################

************
Comissioning
************

Arduino Uno / Due / Leonardo
============================

You need to solder one 8-pin and one 10-pin 
header or similar sized stacking connectors to the left and
the right board edge. The just plug the shield
onto your arduino with the antenna connector located near
the USB port

Teensy
======

There are some steps required to make the board ready for 
use with a teensy:

- you need to connect the IOREF voltage with the Teensy 3.3V 
  supply: Use some solder to connect both pads of JP101 on the
  backside of the shield. (alternative: connect the 3.3V and IOREF 
  pins of Arduino connector with a wire) **Warning: Do not plug
  the shield on an 5V Arduino afterwards, since this will
  short the 3.3V and 5V voltages**
- If you want to use the I2S interface with the teensy, solder 
  a 4x0603 10 Ohm resistor array either to RN103 for Teensy 4.0 
  or to RN104 for Teensy 3.2
- If you want to use the SD-Card interface you have to solder 
  another 4x0603 10 Ohm resistor array to RN101, either oriented
  on the leftmost four pads for Teensy 3.2 or onto the rightmost
  four pads for Teensy 4.0 **Warning: revision 1 shield unfortunately
  uses a wrong (5x0603) footprint which does not align properly with
  a 4x0603 array. Try to solder some small wires or two 2x0402 
  resistor arrays**

.. figure:: shield-r1_teensy3.2.jpg

   Preparing for Teensy 3.2


*********************
Reference Description
*********************

I2C Bus interface
=================

I2C is the main interface to manage and control the shield. The
shield comes with integrated voltage level translation and can run
at any I/O Voltage (IOREF pin) between 1.8 and 5.5 V.

The following I2C bus addresses are used:

=======  =========  ========  ==============================================
Address  Reference  Device    Description
=======  =========  ========  ==============================================
0x10     U201       MAX98089  Audio Codec with integrated Class D amplifier
0x20     U102       PCAL6408  IO Expander to control various board functions
0x50     U103       24CW1280  128 kBit EEPROM for application use
0x64     U301       SI4688    DAB+/FM radio receiver chip.
=======  =========  ========  ==============================================

SI4688 usage
============

I2C Interface
-------------

Since the SI4688 pulls the I2C lines to GND during RESET, the
SI4688's I2C interface is disconnected on the shield
by default. In order to communicate with it, it must be connected 
to I2C bus by setting the P0 of U102 to High level:

- before setting P0 of U102 to High level, make sure to de-assert reset 
  signal of SI4688 by setting P1 of U102 to high level and delay some 
  milliseconds. 
- when asserting reset signal of SI4688 by setting P1 of U102
  to low level, make sure to disconnect I2C beforehand or at the same 
  time by setting P0 of U102 to low level

.. Warning::
   Do not set P0 to High level while SI4688 is in reset state. Otherwise
   the I2C bus will be blocked forever.

Firmware flash organization
---------------------------

The SI4688 receiver chip U301 is connected to a 2MByte flash 
memory U302 which contains the firmware for SI4688. The 
following partitioning scheme is used for this flash
memory:

========  ============  =====================
Offset    Maximum Size  Description
========  ============  =====================
0x000000    16k         Spare
0x004000    16k         Romloader patch
0x008000   608k         FM Firmware
0x0a0000  1408k         DAB Firmware
========  ============  =====================



