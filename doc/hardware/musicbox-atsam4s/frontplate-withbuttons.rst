Frontplate with OLED & tact buttons
===================================

This frontplate uses an EA OLEDM204-LGA 4x20 character display
and some tact buttons. It has no volume control. The volume is 
supposed to be controlled by volume control poti at power amp
input.

OLED Power Supply
-----------------

While the frontplate will be supplied 3.3V from base board, the OLED
display needs 12V VCC with a maximum of about 15mA current in case all
dots are on.

MC34063 variant
^^^^^^^^^^^^^^^

Using a 34063 based boost converter design following parameters:

- fsw   = 100kHz
- Lmin  = 220µH
- Ipeak = 0.12A

