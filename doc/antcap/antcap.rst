Antenna Cap Curve Measurement
=============================

As of the Si468x documentation, the Si468x can adapt 
internal capacities to the antenna using a simple linear
model. For that the optimum antcap value as function of
frequency needs to be determined and fitted. The application
here has the ability to perform the measurements automatically
and store the results into a csv file. The complete optimization
process is described in AN1028. However AN649,650 should be 
taken into account because the proper settings of VHFSW depend
on the actuall frontend circuit

Measurements
------------
- `antcap-rod-antenna1.dat`: 
   
   The first run with rod antenna, unfortunately not completely 
   expanded.

- `antcap-rod-antenna2.dat`: 

   49cm rod antenna
   
- `antcap-rod-antenna3.dat`: 
   
   49 cm rod but wrong settings for VHFSW switch. (I though it
   was wrong in 2nd measurment but it wasent.

Evaluation
----------

Since I'm using the "Appendix C - Application Circuit for EMI Mitigation"
circuit for my front end, it seems the varactor tuning is only needed for
DAB mode. According to manual, varactor has no influence in FM Mode.
Actually, in `antcap-rod-antenna2.dat` one observes the highest RSSI for lowest
AntCap values, so antcap should be set to "0". (This is the default setting)

For DAB Mode, we have to extract the AntCap value for highest RSSI and
Fit a linear function antcap = a * f[Mhz] + b. Extraction is done
manually. Fit using linear regression yields::

  m =    -1.541
  b =   394

Thus we have::

  DAB_TUNE_FE_VARM = -1541
  DAB_TUNE_FE_VARB =   394



Result of


